# Search API for job ads - getting started

*[If you want to get started with writing code for calling our APIs, go here: <https://gitlab.com/arbetsformedlingen/job-ads/getting-started-code-examples>]*

The aim of this text is to walk you through what you're seeing in the [Swagger-GUI](https://jobsearch.api.jobtechdev.se)
to give you a bit of orientation on what can be done with the Job Search API. If you are just looking for a way to fetch
all ads please use our [Stream API](https://jobtechdev.se/sv/komponenter/jobstream).

## Table of Contents

* [Endpoints](#Endpoints)
* [Results](#Results)
* [Errors](#Errors)
* [Use cases](#Use-cases)

## Short introduction

The endpoints for the ads search API are:

* [search](#Ad-search) - returning ads matching a search phrase.
* [complete](#Typeahead) - returning common words matching a search phrase. Useful for autocomplete.
* [ad](#Ad) - returning the ad matching an id.
* [logo](#Logo) - returns the logo for an ad.

The easiest way to try out the API is to go to the [Swagger-GUI](https://jobsearch.api.jobtechdev.se/). But first you
need a key to authenticate yourself.

## Endpoints

Below we only show the URLs. If you prefer the curl command, you type it like:

curl "{URL}" -H "accept: application/json"

### Ad search

/search?q={search text}

The search endpoint in the first section will return job ads that are currently open for
applications. The API is meant for searching, we want to offer you the possibility to just build your own customized GUI
on top of our free text query field "q" in /search like this ...

<https://jobsearch.api.jobtechdev.se/search?q=Flen>

This means you don't need to worry about how to build an advanced logic to help the users finding the most relevant ads
for, let's say, Flen. The search engine will do this for you. If you want to narrow down the search result in other ways
than the free query offers, you can use the available search filters. Some of the filters need id-keys as input for
searching structured data. The ids can be found in the [Taxonomy API](https://jobtechdev.se/docs/apis/taxonomy/). These
ids will help you get sharper hits for structured data. We will always work on improving the hits for free queries
hoping you'll have less and less use for filtering.

### Typeahead

/complete?q={typed string}

If you want to help your end users with term suggestions you can use the typeahead function, which will return common
terms found in the job ads. This should work great with an auto complete feature in your search box. If you request ...

<https://jobsearch.api.jobtechdev.se/complete?q=stor>

... you'll get storkök, storhushåll, storesupport, and storage as they are the most common terms starting with "stor*"
in ads.

If you have a trailing space in your request

<https://jobsearch.api.jobtechdev.se/complete?q=storage%20s>

... you'll get sverige, stockholms län, stockholm, svenska, and script since they are the most common terms beginning
with "s" for ads that contain the word "storage".

N.B. The number you get in "occurrences" is an approximate value, it will differ from the actual search result
(due to differences in the query to Opensearch). Use it as a guideline, not as an exact result.

### Ad

/ad/{id}

This endpoint is used for fetching specific job ads with all available metadata, by their ad ID number. The ID number
can be found by doing a search query.

<https://jobsearch.api.jobtechdev.se/ad/8430129>

### Logo

/ad/{id}/logo

This endpoint returns the logo for a given ad's id number.

<https://jobsearch.api.jobtechdev.se/ad/8430129/logo>

If no logo exists, a 1x1 pixel size white image is returned.

### Code examples

Code examples for accessing the API can be found in the 'getting-started-code-examples' repository on GitHub:
<https://github.com/JobtechSwe/getting-started-code-examples>

### Jobtech-Taxonomy

If you need help finding the official names for occupations, skills, or geographic locations you will find them in
our [Taxonomy API](https://jobtechdev.se/en/products/jobtech-taxonomy).

## Results

The results of your queries will be in [JSON](https://en.wikipedia.org/wiki/JSON) format. We won't attempt to explain
this attribute by attribute in this document. Instead, we've decided to try to include this in the data model which you
can find in our [Swagger-GUI](https://jobsearch.api.jobtechdev.se).

Successful queries will have a response code of 200 and give you a result set that consists of:

1. Some metadata about your search such as the number of hits and the time it took to execute the query and
2. The ads that matched your search.

## Errors

Unsuccessful queries will have a response code of:

| HTTP Status code | Reason | Explanation |
| ------------- | ------------- | -------------|
| 400 | Bad Request | Something wrong in the query |
| 404 | Missing ad | The ad you requested is not available |
| 429 | Rate limit exceeded | You have sent too many requests in a given amount of time |
| 500 | Internal Server Error | Something wrong on the server side |

## Use cases

To help you find your way forward, here are some examples of use cases:

* [Getting ads that are further down than 100 in the result set](#Using-offset-and-limit)
* [Searching using Wildcard](#Searching-using-Wildcard)
* [Phrase search](#Phrase-search)
* [Searching for a particular job title](#Searching-for-a-particular-job-title)
* [Searching only within a specific field of work](#Searching-only-within-a-specific-field-of-work)
* [Filtering employers using organisation number](#Filtering-employers-using-organisation-number)
* [Finding jobs near you](#Finding-jobs-near-you)
* [Negative search](#Negative-search)
* [Finding Swedish speaking jobs abroad](#Finding-Swedish-speaking-jobs-abroad)
* [Using the abroad filter](#Using-the-abroad-filter)
* [Using the remote filter](#Using-the-remote-filter)
* [Customise the result set](#Customise-the-result-set)
* [Getting all the jobs since date and time](#Getting-all-the-jobs-since-date-and-time)
* [Simple freetext search](#Simple-freetext-search)

#### Using offset and limit

There is a default number of ads in the result set that's set to 10. This can be increased up to a maximum of 100. From
there on ads that have been given a higher number will have to be fetched using the offset parameter. So in this case, I
want to fetch ads 100-200 for a freetext search for python.

Request URL

<https://jobsearch.api.jobtechdev.se/search?offset=100&limit=100>

#### Searching using Wildcard

For some terms, the easiest way to find everything you want is through a wildcard search. An example from a user
requesting this kind of search was for museum jobs where both searches for "museum" and the various job titles starting
with "musei" would be relevant hits which the information structure currently doesn't merge very well with. 
Included from version 1.8.0

Request URL

<https://jobsearch.api.jobtechdev.se/search?q=muse*>

#### Phrase search

To search in the ad text for a phrase, use the q parameter and surround the phrase with double quotes "this phrase".

Request URL

<https://jobsearch.api.jobtechdev.se/search?q="search%20for%20this%20phrase">

#### Searching for a particular job title

The easiest way to get the ads that contain a specific word like a job title is to use a free text query (q) with the _
search_ endpoint. This will give you ads with the specified word in either headline, ad description, or place of work.

Request URL

<https://jobsearch.api.jobtechdev.se/search?q=souschef>

If you want to be certain that the ad is for a "souschef" - and not just mentions a "souschef" - you can use the
occupation ID in the field "occupation". If the ad has been registered by the recruiter with the occupation field set
to "souschef", the ad will show up in this search. To do this query you use both
the [Taxonomy API](https://jobtechdev.se/docs/apis/taxonomy/) and the _search_ endpoint. First of all, you need to find
the occupation ID for "souschef" in the [Taxonomy API](https://jobtechdev.se/docs/apis/taxonomy/) for the term in the
right category (occupation-name).

**NB! the old endpoint (~~jobsearch.api.jobtechdev.se/taxonomy/~~) has been removed. Use
our [Taxonomy API](https://jobtechdev.se/docs/apis/taxonomy/) instead**

Now you can use the conceptId (iugg_Qq9_QHH) in _search_ to fetch the ads registered with the term "souschef" in the
occupation-name field:

Request URL

<https://jobsearch.api.jobtechdev.se/search?occupation-name=iugg_Qq9_QHH>

This will give a smaller number of results but with a higher accuracy that it is indeed a "souschef" being sought,
but this result will likely miss some ads since the occupation-name field is not always filled in by the employer. You
will find that a larger set is more useful since several sorting factors work to show the most relevant hits first. We
are constantly working to improve the API when it comes to unstructured data.

### Searching only within a specific field of work

First, use the [Taxonomy API](https://jobtechdev.se/en/products/jobtech-taxonomy) to get the Id Data/IT (occupation field). Then
do a freetext search for "IT" to specify the search to the occupation field.

In the response, you will find the conceptId (apaJ_2ja_LuF) for the term Data/IT. Use this together with the "search" endpoint to
define what you want. So now I want to combine this with my favorite programming language without all similar jobs
cluttering my search.

Request URL

<https://jobsearch.api.jobtechdev.se/search?occupation-field=apaJ_2ja_LuF&q=python>

Similarly, you can use the [Taxonomy API](https://jobtechdev.se/en/products/jobtech-taxonomy) to find the conceptId for
the parameters *occupation-group* and *occupation-collection*.

*occupation-collection* can be used in combination with *occupation-name*, *occupation-field*, and *occupation-group* and
the search will show ads that are in all.

### Using the remote filter

This filter looks for well-known phrases in the description that are used to describe that the position will mean remote work. 
It can be both partly or full-time. The feature means the ad is tagged with remote = true if one of the following phrases appear in the ad:  
"arbeta på distans"  
"arbete på distans"  
"jobba på distans"  
"arbeta hemifrån"  
"arbetar hemifrån"  
"jobba hemifrån"  
"jobb hemifrån"   
"remote work"  
"jobba tryggt hemifrån"  
“work remote”  
“jobba remote”  
“arbeta remote”*    
There is of course no gaurantee that this method is 100% accurate, but it allows for a slightly better experience for
users looking for remote jobs.

Request URL

<https://jobsearch.api.jobtechdev.se/search?remote=true>

### Filtering employers using organisation number

If you want to list all jobs at a specific employer you can use the Swedish organisation number from
Bolagsverket. For example, it is possible to take Arbetsförmedlingen's number 2021002114 and simply use it as a
filter.

Request URL

<https://jobsearch.api.jobtechdev.se/search?employer=2021002114>

The filter does a prefix search by default, much like a wildcard search without you having to specify an asterisk.
So a good example of the usefulness is the advantage that all government employers in Sweden have an organisation number
that starts with 2. So you can make a request for Java jobs in the public sector like this:

Request URL

<https://jobsearch.api.jobtechdev.se/search?employer=2&q=java>

### Finding jobs near you

You can filter on geographic terms that you retrieved from the Taxonomy API in the same way you can with occupation titles
and occupation fields. (Concept_id does not work everywhere yet, but you can use numeric ids that are official and
unlikely to change, as skills and occupations sometimes do). If you want to search for jobs in Norway, you can find
the conceptId for "Norway" in the [Taxonomy API](https://jobtechdev.se/en/products/jobtech-taxonomy).

And add the parameter conceptId (QJgN_Zge_BzJ) in the country field.

Request URL

<https://jobsearch.api.jobtechdev.se/search?country=QJgN_Zge_BzJ>

If you do a search with two geographic filters, the most local one will be shown first. As in this case, you search for
teachers and used the municipality code for Haparanda (tfRE_hXa_eq7) and the region code for Norrbotten County (9hXe_F4g_eTG). The jobs
that are in Haparanda will be the ones shown first in the list.

<https://jobsearch.api.jobtechdev.se/search?municipality=tfRE_hXa_eq7&region=9hXe_F4g_eTG&q=l%C3%A4rare>

You can also use latitude, longitude coordinates, and radius in kilometers if you want.

Request URL

<https://jobsearch.api.jobtechdev.se/search?position=59.3,17.6&position.radius=10>

### Negative search

So, this is very simple if you use the q field. For example, you want to find Unix jobs.

Request URL

<https://jobsearch.api.jobtechdev.se/search?q=unix>

But you find that you get several hits for Linux jobs, which you don't want at all in this example. All you need to do is
put a minus sign in front of the word you want to exclude and search for "unix -linux".

Request URL

<https://jobsearch.api.jobtechdev.se/search?q=unix%20-linux>

### Finding Swedish speaking jobs abroad

Sometimes a filter can be too broad and then it is easier to use a negative search to remove specific
results that you don't want. In this case, we show you how you can filter out all jobs in Sweden. Instead of
adding a minus sign in the q field "-sweden" you can use the country code and country field in the search. So first
find the country code for Sweden in the [Taxonomy API](https://jobtechdev.se/en/products/jobtech-taxonomy).

In response, you get the conceptId i46j_HmG_v64 for "Sweden" and the conceptId zSLA_vw2_FXN for "Swedish".

Request URL to get Swedish-speaking jobs outside of Sweden.

<https://jobsearch.api.jobtechdev.se/search?language=zSLA_vw2_FXN&country=-i46j_HmG_v64>

### Using the abroad filter

The abroad filter is created so it's possible to have jobs from other countries appear in a search where you're also
filtering for specific parts of Sweden. Since jobs in other countries don't have structured data for region or
municipality, they will always be filtered out if any parameters like that are used in the search since it's impossible to
include them by using existing fields other than country. This can make it hard to construct logical filters in a GUI
saying something like "Jobs outside of Sweden". By setting the filter to true, you will allow your search terms to find
ads with other country codes than Sweden but no region or municipality code present. In the example, we will look for jobs
within the Stockholm region but also including jobs abroad.

Request URL

    https://jobsearch.api.jobtechdev.se/search?region=CifL_Rzy_Mku&abroad=true

### Using the remote filter

This filter looks for well-known phrases in the description that are used to describe that the position will mean remote
work. It can be both partly or full-time. The feature means the ad is tagged with remote = true if one of the following
phrases appear in the ad:
`"arbeta på distans", "arbete på distans", "jobba på distans", "arbeta hemifrån", "arbetar hemifrån", "jobba hemifrån", "jobb hemifrån", "remote work", "jobba tryggt hemifrån", "delvis på distans"`
There is of course no guarantee that this method is 100% accurate, but it allows for a slightly better experience for
users looking for remote jobs.

Request URL

    https://jobsearch.api.jobtechdev.se/search?remote=true

### Customise the result set

There are several reasons why you might want fewer fields in your result set. In this case, the idea is a
search that shows the jobs on a map depending on what the user is searching for. What is needed is the GPS coordinates for the marker
on the map and id, employer, and the headline of the ad so more information can be fetched when the user clicks on
the ad marker. You probably also want to know the total number of ads. In Swagger GUI it is possible to use
X-fields to define which fields to include in the result.

total{value}, hits{id, headline, workplace_address{coordinates}, employer{name}}

This creates an extra header that is shown in the curl example in Swagger. So, this example will look like this:

curl "https://jobsearch.api.jobtechdev.se/search?q=skogsarbetare" -H "accept: application/json" -H "X-Fields: total{value}, hits{id, headline, workplace_address{coordinates}, employer{name}}"

### Getting all the jobs since date and time

A very common use case is to fetch ALL ADS. We don't want you to use the Job Search API for this. It
takes a lot of bandwidth, CPU, and development time and it is not even guaranteed that you will get all the ads. If you want to fetch
all ads we recommend that you use the [Stream API](https://jobstream.api.jobtechdev.se).

### Simple freetext search

To disable the smart search features, set the header `x-feature-disable-smart-freetext` to `true`. The result will be
that the q field will work as a simple text search in the ad's headline and description fields.