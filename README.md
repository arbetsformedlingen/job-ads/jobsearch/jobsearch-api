# JobSearch API

JobSearch API is a search engine API for job advertisements, designed to be used by job boards like Platsbanken. It provides comprehensive search capabilities, including free text and structured searches based on criteria such as professions and geography.

## Product Details

The JobSearch API allows you to search through Arbetsförmedlingen's job advertisements in various ways, enabling both free text and structured searches based on criteria like professions or geography.

### What Problem Does the Product Solve?

The JobSearch API simplifies the creation of custom search services in apps or web pages, providing easy-to-use JSON-formatted search results. This makes it easier for developers to integrate comprehensive job search functionalities into their applications.

By leveraging JobSearch, you can offer users robust and flexible search capabilities, enhancing their ability to find relevant job opportunities efficiently.

## Table of Contents

- [Requirements](#requirements)
- [Environment Variables](#environment-variables)
  - [Application Configuration](#application-configuration)
  - [Flask Configuration](#flask-configuration)
- [Running the Service](#running-the-service)
- [Tests](#tests)
  - [Unit Tests](#run-unit-tests)
  - [Integration Tests](#run-integration-tests)
  - [API Tests](#run-api-tests)
  - [Run All Tests](#run-all-tests)
  - [Smoke Tests](#smoke-tests)
  - [Test Coverage](#test-coverage)

## Requirements

- Python 3.9.4 or later
- Access to a host or server running Opensearch version 2.6.0 or later
- Imported job ads in the Opensearch index

## Environment Variables

The application is entirely configured using environment variables. Default values are provided where applicable.

### Application Configuration

| Variable Name | Default Value | Description                                           |
|---------------|---------------|-------------------------------------------------------|
| DB_HOST       | 127.0.0.1     | Specifies which Opensearch host to use for searching. |
| DB_PORT       | 9200          | Port number to use for Opensearch                     |
| ADS_ALIAS     | current_ads   | Specifies which index to search ads from              |
| DB_USER       |               | Sets username for Opensearch (no default value)       |
| DB_PWD        |               | Sets password for Opensearch (no default value)       |
| DB_TAX_INDEX  | taxonomy      | Specifies which index contains taxonomy information.  |

### Flask Configuration

| Variable Name | Default Value | Description                            |
|---------------|---------------|----------------------------------------|
| FLASK_ENV     |               | Set to "development" for development.  |

## Running the Service

To start the application, set the appropriate environment variables as described above. Then run the following commands:

```sh
pip install -r requirements.txt
export FLASK_ENV=development
flask run
```

Go to [http://127.0.0.1:5000](http://127.0.0.1:5000) to access the Swagger API.

## Tests

### Run Unit Tests

Run unit tests using the following command:

```sh
pytest tests/unit_tests
```

### Run Integration Tests

For integration tests, an actual application instance is started. Specify environment variables for Opensearch to run these tests properly:

```sh
pytest tests/integration_tests
```

### Run API Tests

For API tests, the application must be started. These tests depend on a specific set of ads in the Opensearch index, which can be created using [jobsearch-ad-mock-api](https://gitlab.com/arbetsformedlingen/job-ads/development-tools/jobsearch-ad-mock-api) and imported with [jobsearch-importers](https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-importers).

```sh
pytest tests/api_tests
```

### Run All Tests

To run all tests, including unit, integration, and API tests:

```sh
pytest tests/
```

### Smoke Tests

Smoke tests are a subset of API tests to test broadly but with fewer test cases, saving time. If smoke tests fail, there's no point in running the entire test suite.

```sh
pytest -m "smoke" tests/api_tests
```

### Test Coverage

To measure test coverage:

```sh
pytest --cov=. tests/
```
