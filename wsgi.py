import logging

from flask import Flask
from flask_cors import CORS
from jobtech.common.customlogging import configure_logging

# Import all Resources that are to be made visible for the app
# PyCharm marks them as unused imports, but they are needed
# noinspection PyUnresolvedReferences
import search.rest.endpoint.jobsearch  # noqa: F401
from common import appconf
from search.rest.model.apis import search_api

app = Flask(__name__, static_url_path="")
CORS(app)
# Set up logging with the package names that should be included in the log output
configure_logging([__name__.split(".")[0], "jobtech", "common", "search"])
log = logging.getLogger(__name__)
log.info(logging.getLevelName(log.getEffectiveLevel()) + " log level activated")
log.info(f"Starting: {__name__}")

if __name__ == "__main__":
    # Used only when starting this script directly, i.e. for debugging
    appconf.initialize_app(app, search_api)
    app.run(debug=False)
else:
    # Main entrypoint
    appconf.initialize_app(app, search_api)
