import pytest

from wsgi import app


@pytest.fixture()
def client():
    app.config.update(
        {
            "TESTING": True,
        }
    )

    client = app.test_client()

    # Always add accept header to client
    client.environ_base["HTTP_ACCEPT"] = "application/json"

    return client
