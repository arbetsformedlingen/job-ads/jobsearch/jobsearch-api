import os
from pathlib import Path

import pytest
import requests

from search.common_search import logo
from tests.test_resources.test_settings import TEST_USE_STATIC_DATA


@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on a fixed set of ads")
def test_fetch_logo_url_by_search(client):
    """
    Search for 100 hits
    Find the first ad with a logo url in those hits
    Verify that the logo url is reachable
    """
    response = client.get(f"/search", json={"limit": "100"})

    hits = response.json["hits"]
    assert len(hits) > 0

    found_logo_url = get_correct_logo_url_for_any_ad(hits)
    assert found_logo_url

    cert = Path(__file__).parent / ".." / ".." / ".." / "cert" / "af_jobtech_bundle.crt"
    result = requests.head(found_logo_url, proxies={}, verify=cert)
    assert result.status_code == 200


@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on a fixed set of ads")
def test_fetch_ad_logo_by_id(client):
    """
    Get logo for a specific ad
    Verify that the logo url is reachable
    """
    ad_id = "23699999"
    response = client.get(f"/ad/{ad_id}/logo")

    assert response.status_code == 200
    assert type(response.data) == bytes


def get_correct_logo_url_for_any_ad(list_of_ads):
    assert len(list_of_ads) > 0
    for ad in list_of_ads:
        try:
            logo_url = logo.get_correct_logo_url(ad)
            if logo_url:
                return logo_url
        except requests.exceptions.HTTPError:
            pass
    return False  # if no logo_url is found in list_of_ads


@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on a fixed set of ads")
def test_fetch_org_logo_url_by_ad_id():
    # Arrange
    query = {"limit": "100"}

    # Act
    response = requests.get("http://localhost:5000/search", params=query)

    # Assert
    hits = response.json()["hits"]
    assert len(hits) > 0
    assert get_correct_logo_url_for_any_ad(hits)


@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on a fixed set of ads")
def test_fetch_ad_logo_by_id():
    """
    Search for 100 hits
    Find the first logo url in those hits
    GET that logo url
    """
    # Arrange
    query = {"limit": "100"}

    # Act
    response = requests.get("http://localhost:5000/search", params=query)

    # Assert
    hits = response.json()["hits"]
    assert len(hits) > 0
    found_logo_url = get_correct_logo_url_for_any_ad(hits)
    assert found_logo_url

    result = requests.get(found_logo_url)
    result.raise_for_status()
    assert result is not None
    assert len(result.content) > 1  # image


if __name__ == "__main__":
    pytest.main([os.path.realpath(__file__), "-svv", "-ra", "-m integration"])
