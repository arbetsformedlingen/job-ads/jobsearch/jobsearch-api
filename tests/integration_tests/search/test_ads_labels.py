import pytest
from search.common_search.ads_labels import AdsLabels
from tests.test_resources.test_settings import TEST_USE_STATIC_DATA

@pytest.fixture(scope="module")
def ads_labels():
    ads_labels = AdsLabels()
    return ads_labels

@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on a fixed set of ads")
def test_load_unique_labels_from_ads(ads_labels):
    labels = ads_labels.get_extracted_ads_labels()
    assert len(labels) > 0

    assert 'nystartsjobb' in labels
    assert 'rekryteringsutbildning' in labels
    assert 'dummy_label' in labels
    assert 'räksmörgås' in labels
    assert 'weird_case_label' in labels
