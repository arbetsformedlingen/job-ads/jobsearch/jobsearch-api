import logging
import pytest
import re
from common import taxonomy as t
from search.common_search.valuestore import get_stats_for

log = logging.getLogger(__name__)

tax_stat = [[t.OCCUPATION], [t.GROUP], [t.FIELD], [t.SKILL]]
tax_other = [[t.MUNICIPALITY], [t.REGION]]
tax_noexist = [['  ', 'blabla', '']]

# TODO: Fix test or remove.
@pytest.mark.skip(reason="The value 'B - Begränsad auktorisation' in the taxonomy makes this test fail")
@pytest.mark.parametrize("taxonomy_type", tax_stat + tax_other + tax_noexist)
def test_get_stats_for_taxonomy_type(taxonomy_type):
    if taxonomy_type not in tax_stat:
        try:
            get_stats_for(taxonomy_type)
        except KeyError as e:
            print('KeyError exception. Reason: taxonomy type %s' % str(e))
            assert "'" + taxonomy_type + "'" == str(e)
        except Exception as ex:
            pytest.fail('ERROR: This is not a KeyError exception: %s (%s)' %
                        (str(ex), taxonomy_type), pytrace=False)
    else:  # taxonomy_type is in 5 mentioned in get_stats_for()
        stats_for_tax_type = get_stats_for(taxonomy_type)
        for k, v in stats_for_tax_type.items():
            assert is_str_of_int(k), f"Test failed for taxonomy_type: {taxonomy_type}, key: {k}, value: {v}"  # check k is string of int
            assert isinstance(v, int)  # check v is int


@pytest.mark.parametrize("taxonomy_type", (tax_noexist))
def test_get_stats_for_taxonomy_type_neg(taxonomy_type):
    assert get_stats_for(taxonomy_type) == {}


@pytest.mark.parametrize("taxonomy_type", tax_other)
def test_get_stats_for_taxonomy_type_other(taxonomy_type):
    assert get_stats_for(taxonomy_type) != {}


@pytest.mark.parametrize("v", ['a', 'abc', '-1'])
def test_is_char_as_str(v):
    with pytest.raises(AssertionError):
        assert is_int(v)


@pytest.mark.parametrize("v", ['1', '0', '10000'])
def test_is_int_as_str(v):
    assert is_int(v)


@pytest.mark.parametrize("v", [0, 1, 1000])
def test_is_int_as_int(v):
    with pytest.raises(TypeError):
        assert is_int(v)


def is_int(value):
    return re.match(r'[0-9]+$', value) is not None


def is_str_of_int(str_to_check):
    return re.match(r'^[\w\d_-]*$', str_to_check) is not None  # check if it is a string of int
