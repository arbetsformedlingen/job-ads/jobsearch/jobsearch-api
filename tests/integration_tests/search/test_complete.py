import json
import logging
import time

import jmespath
import pytest

from common import constants
from search.common_search.complete import _check_search_word_type
from search.common_search.complete import suggest
from search.common_search.querybuilder import QueryBuilderType
from search.rest.endpoint.jobsearch import Complete
from search.rest.endpoint.querybuilder_container import QueryBuilderContainer

log = logging.getLogger(__name__)

@pytest.fixture(scope="module")
def querybuilder_jobsearch():
    querybuilder_container = QueryBuilderContainer()
    querybuilder_container.launch()
    return querybuilder_container.get(QueryBuilderType.JOBSEARCH_COMPLETE)


@pytest.mark.parametrize("search_text, expected_result", [
    (None, None),
    ('', None),
    ('stockholm', 'location'),
    ('stockholms län', 'location'),
    ('stockholms län ', 'location'),
    (' stockholm', 'location'),
    ('+stockholm', 'location'),
    ('-stockholm', 'location'),
    ('"stockholm"', 'location'),
    ('"stockholm', 'location'),
    ('städare ', 'occupation'),
    ('städare &', 'occupation'),
    ('rekryteringsutbildning', 'skill'),
    ('java ', 'skill'),
])
def test_check_search_word_type(querybuilder_jobsearch, caplog, search_text, expected_result):
    """
    Tests for search word type
    """
    result = _check_search_word_type(search_text, querybuilder_jobsearch)
    log_levels = [tup[1] for tup in caplog.record_tuples]
    assert logging.ERROR not in log_levels, "Logging error from opensearch call"
    assert logging.WARNING not in log_levels, "Logging warning from opensearch call"
    assert result == expected_result



@pytest.mark.parametrize("search_text, expected_result", [
    ('java pytho', 'java python'),
])
def test_complete_for_enriched_terms(querybuilder_jobsearch, search_text, expected_result):
    start_time = int(time.time() * 1000)
    args = _create_args_according_to_endpoint(search_text)
    result = suggest(args, querybuilder_jobsearch)
    marshalled_result = Complete.marshal_results(result, 100, start_time)

    assert jmespath.search('typeahead[0].value', marshalled_result) == expected_result


LABEL_NYSTARTSJOBB = 'NYSTARTSJOBB'.lower()
LABEL_REKRYTERINGSUTBILDNING = 'REKRYTERINGSUTBILDNING'.lower()
LABEL_DUMMY = 'DUMMY_LABEL'.lower()
LABEL_SWEDISH_CHARS = 'RÄKSMÖRGÅS'.lower()
LABEL_ANYCASE = 'wEirD_CaSE_labEL'.lower()

@pytest.mark.parametrize("search_text, expected_result, contextual", [
    ('nystartsjob', f'{LABEL_NYSTARTSJOBB}', True),
    ('java nystartsjob', f'java {LABEL_NYSTARTSJOBB}', True),
    ('java nysta', f'java {LABEL_NYSTARTSJOBB}', True),
    ('java nystarts', f'java {LABEL_NYSTARTSJOBB}', True),
    ('java rekryterings', f'java {LABEL_REKRYTERINGSUTBILDNING}', True),
    ('java dummy_la', f'java {LABEL_DUMMY}', True),
    ('java räksm', f'java {LABEL_SWEDISH_CHARS}', True),
    ('java weird_', f'java {LABEL_ANYCASE}', True),
    ('nystartsjob', f'{LABEL_NYSTARTSJOBB}', False),
    ('java nystartsjob', f'java {LABEL_NYSTARTSJOBB}', False),
    ('java nysta', f'java {LABEL_NYSTARTSJOBB}', False),
    ('java nystarts', f'java {LABEL_NYSTARTSJOBB}', False),
    ('java rekryterings', f'java {LABEL_REKRYTERINGSUTBILDNING}', False),
    ('java dummy_la', f'java {LABEL_DUMMY}', False),
    ('java räksm', f'java {LABEL_SWEDISH_CHARS}', False),
    ('java weird_', f'java {LABEL_ANYCASE}', False),
])
def test_complete_for_labels_contextual_true_and_false(querybuilder_jobsearch, search_text, expected_result, contextual):
    start_time = int(time.time() * 1000)
    args = _create_args_according_to_endpoint(search_text)
    args[constants.CONTEXTUAL_TYPEAHEAD] = contextual
    result = suggest(args, querybuilder_jobsearch)

    assert result
    assert 'aggs' in result

    marshalled_result = Complete.marshal_results(result, 100, start_time)
    log.debug(f"\n\nmarshalled_result: {json.dumps(marshalled_result, indent=4)}")

    typeahead = jmespath.search('typeahead', marshalled_result)

    expected_value_found = False

    for typeahead_item in typeahead:
        if typeahead_item['value'] == expected_result:
            expected_value_found = True
            break

    assert expected_value_found, f"Expected value {expected_result} not found in typeahead result: {typeahead}"


@pytest.mark.parametrize("search_text, contextual", [
    ('sjuksköterska stockholms län ' , True),
    ('lärare stockholms län ' , True),
    ('lärare stockholms län' , True),
    ('lärare stockholms län gotlands län ' , True),
    ('lärare stockholms län lärare ' , True),
    ('sjuksköterska stockholms län ' , False),
    ('lärare stockholms län ' , False),
    ('lärare stockholms län' , False),
    ('lärare stockholms län gotlands län ' , False),
    ('lärare stockholms län lärare ' , False),
])
def test_complete_avoid_exact_input_in_result(querybuilder_jobsearch, search_text, contextual):
    start_time = int(time.time() * 1000)
    args = _create_args_according_to_endpoint(search_text)
    args[constants.CONTEXTUAL_TYPEAHEAD] = contextual
    result = suggest(args, querybuilder_jobsearch)

    assert result
    assert 'aggs' in result

    marshalled_result = Complete.marshal_results(result, 100, start_time)
    log.debug(f"\n\nmarshalled_result: {json.dumps(marshalled_result, indent=4)}")

    typeahead = jmespath.search('typeahead', marshalled_result)

    typeahead_values = [typeahead_item['value'] for typeahead_item in typeahead]

    assert search_text not in typeahead_values, f"Unexpected value {search_text} was found in typeahead result: {typeahead_values}"


@pytest.mark.parametrize("search_text, contextual", [
    ('lärare gotlands län ' , False),
])
def test_complete_keep_location_parts_in_result(querybuilder_jobsearch, search_text, contextual):
    start_time = int(time.time() * 1000)
    args = _create_args_according_to_endpoint(search_text)
    args[constants.CONTEXTUAL_TYPEAHEAD] = contextual
    result = suggest(args, querybuilder_jobsearch)

    assert result
    assert 'aggs' in result

    marshalled_result = Complete.marshal_results(result, 100, start_time)
    log.info(f"\n\nmarshalled_result: {json.dumps(marshalled_result, indent=4)}")

    typeahead = jmespath.search('typeahead', marshalled_result)

    typeahead_values = [typeahead_item['value'] for typeahead_item in typeahead]

    assert search_text not in typeahead_values, f"Unexpected value {search_text} was found in typeahead result: {typeahead_values}"



def _create_args_according_to_endpoint(input_terms):
    args = {}
    args[constants.FREETEXT_QUERY] = input_terms
    word_list, space_last = Complete.typeahead_query_analyzer(args.get(constants.FREETEXT_QUERY))
    prefix_words = word_list if space_last else word_list[:-1]
    args[constants.TYPEAHEAD_QUERY] = ' '.join(word_list) + (' ' if space_last else '')  # used for aggs queries
    args[constants.FREETEXT_QUERY] = ' '.join(prefix_words)  # used for subset selections (if contextual==True)
    return args