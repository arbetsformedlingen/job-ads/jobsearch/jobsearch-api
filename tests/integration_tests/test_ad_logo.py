import pytest


def test_logo(client):
    """
    Test that the 'logo' endpoint returns a logo
    """
    # Act
    response = client.get("/ad/24681267/logo")

    # Assert
    assert response.status_code == 200

    assert response.headers["Content-Type"] == "image/png"
    assert response.headers["Content-Disposition"] == "inline; filename=logo.png"
