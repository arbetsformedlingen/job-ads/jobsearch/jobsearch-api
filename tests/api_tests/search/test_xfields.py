import pytest  # noqa: F401
from jsonschema import validate


def test_search_with_x_fields_header_returns_response(client):
    # Arrange
    headers = {"X-fields": "total"}

    # Act
    response = client.get("/search", headers=headers)

    # Assert
    assert response.status_code == 200

    validate(
        response.json,
        {
            "$schema": "https://json-schema.org/draft/2020-12/schema",
            "type": "object",
            "properties": {
                "total": {
                    "type": "object",
                    "properties": {
                        "value": {"type": "integer"},
                    },
                    "additionalProperties": False,
                },
                "additionalProperties": False,
            },
            "additionalProperties": False,
        },
    )  # Throws ValidationError if fails.


def test_search_with_empty_x_fields_header_returns_response(client):
    # Arrange
    headers = {"X-fields": ""}

    # Act
    response = client.get("/search", headers=headers)

    # Assert
    assert response.status_code == 200
    assert response.json["total"]["value"] > 0


def test_search_with_x_fields_header_with_filter_for_hits_id_and_headline(client):
    # Arrange
    headers = {"X-fields": "hits{id, headline}"}

    # Act
    response = client.get("/search", headers=headers)

    # Assert
    assert response.status_code == 200

    validate(
        response.json,
        {
            "$schema": "https://json-schema.org/draft/2020-12/schema",
            "type": "object",
            "properties": {
                "hits": {
                    "type": "array",
                    "items": {
                        "type": "object",
                        "properties": {
                            "id": {"type": "string"},
                            "headline": {"type": "string"},
                        },
                        "additionalProperties": False,
                    },
                },
                "additionalProperties": False,
            },
            "additionalProperties": False,
        },
    )  # Throws ValidationError if fails.
