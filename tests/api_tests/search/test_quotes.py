import pytest


@pytest.mark.smoke
@pytest.mark.parametrize(
    "q, expected_count",
    [
        pytest.param("gymnasielärare", 62, marks=pytest.mark.smoke),  # no quotes
        ['"gymnasielärare"', 18],  # double quotes
        ["'gymnasielärare'", 62],  # single quotes
        ['gymnasielärare"', 62],  # double quote at end
        ["gymnasielärare'", 62],  # single quote at end
        ['"gymnasielärare', 18],  # double quote at start
        ["'gymnasielärare", 62],  # single quote at start
        ["gymnasielärare lärare", 255],  # no quotes, additional word
        ['"gymnasielärare" "lärare"', 357],  # double quotes, each words
        ['"gymnasielärare lärare"', 0],  # double quotes, both words
        ['gymnasielärare""', 13],  # two double quotes at end
        ['""gymnasielärare', 13],  # two double quotes at start
        ["gymnasielärare''", 62],  # two single quotes at end
        ["''gymnasielärare", 62],  # two single quotes at start
    ],
    # Provide nice names for the test cases
    ids=[
        "no quotes",
        "double quotes",
        "single quotes",
        "double quote at end",
        "single quote at end",
        "double quote at start",
        "single quote at start",
        "no quotes, additional word",
        "double quotes, each words",
        "double quotes, both words",
        "two double quotes at end",
        "two double quotes at start",
        "two single quotes at end",
        "two single quotes at start",
    ],
)
def test_different_quote_styles(q, expected_count, client):
    # Arrange
    query = {"q": q, "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count


@pytest.mark.parametrize(
    "q, expected_count",
    [
        pytest.param('"c++', 62, marks=pytest.mark.smoke),
        ['"c++"', 62],
        pytest.param('"c+', 41, marks=pytest.mark.smoke),
        ['"c( ', 40],
    ],
)
def test_search_with_cplusplus_in_quotes(q, expected_count, client):
    """
    Test for a bug where some quotes caused an 'internal server error' response
    """
    # Arrange
    query = {"q": q, "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count


@pytest.mark.parametrize(
    "q, expected_count",
    [
        ["python stockholm", 34],
        ['"python stockholm"', 0],
        ['"python" "stockholm"', 957],
        pytest.param('"python" stockholm', 843, marks=pytest.mark.smoke),
        ['python "stockholm"', 97],
        ['"python job in stockholm"', 0],
        ['"work from home" python stockholm', 34],
    ],
)
def test_query_with_quotes(q, expected_count, client):
    # Arrange
    query = {"q": q, "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count
