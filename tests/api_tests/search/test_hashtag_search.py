import pytest


@pytest.mark.parametrize(
    "q, expected_count",
    [
        ["#corona", 0],
        pytest.param("#jobbjustnu'", 308, marks=pytest.mark.smoke),
        ["#metoo'", 1],
        ["#wedo'", 1],
    ],
)
def test_search_with_hashtag(q, expected_count, client):
    """
    Check that it is possible to search with hashtags.
    """

    # Arrange
    query = {"q": q}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count
