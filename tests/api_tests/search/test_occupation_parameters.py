import pytest


@pytest.mark.parametrize("occupation_name", ["r4G8_gBH_Pug", "HdcS_g6a_jPk", "f8q7_Rwv_TP4"])
def test_occupation_name_returns_data(client, occupation_name):
    """
    Test that the 'occupation-name' parameter returns data
    """
    # Arrange
    query = {"occupation-name": occupation_name, "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] > 0


def test_occupation_name_returns_all_ads_with_empty_string(client):
    """
    Test that the 'occupation-name' parameter returns all ads when empty string
    """
    # Arrange
    query = {"occupation-name": "", "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] > 1000  # should return all ads


def test_occupation_name_returns_data_with_multiple_values(client):
    """
    Test that the 'occupation-name' parameter returns data with multiple values
    """
    # Arrange
    occupation_names = ["r4G8_gBH_Pug", "HdcS_g6a_jPk", "f8q7_Rwv_TP4"]
    query = {"occupation-name": occupation_names, "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] > 0


@pytest.mark.parametrize("occupation_group", ["Z8ci_bBE_tmx", "oQUQ_D11_HPx", "hmaC_cfi_UKg"])
def test_occupation_group_returns_data(occupation_group, client):
    """
    Test that the 'occupation-group' parameter returns data
    """
    # Arrange
    query = {"occupation-group": occupation_group, "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] > 0


@pytest.mark.parametrize("collection_id", ["UdVa_jRr_9DE", "ja7J_P8X_YC9", "XouD_cCj_HZN"])
def test_occupation_collection_returns_data(collection_id, client):
    """
    Test that the 'occupation-collection' parameter returns data
    """
    # Arrange
    query = {"occupation-collection": collection_id, "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] > 0


def test_occupation_collection_returns_all_ads_with_empty_string(client):
    """
    Test that the 'occupation-collection' parameter returns all ads when empty string
    """
    # Arrange
    query = {"occupation-collection": "", "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] > 1000  # should return all ads


def test_occupation_collection_returns_data_with_multiple_values(client):
    """
    Test that the 'occupation-collection' parameter returns data with multiple values
    """
    # Arrange
    collection_ids = ["UdVa_jRr_9DE", "ja7J_P8X_YC9", "XouD_cCj_HZN"]
    query = {"occupation-collection": collection_ids, "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] > 0


def test_collection_and_freetext(client):
    """
    Fetch collections from Taxonomy and test them one by one
    Do a search with 'occupation-collection' and verify that number of ads is > 0
    Do a search with 'q' parameter and verify that number of ads is > 0
    Do a search with 'occupation-collection' AND 'q' params and verify that no ads are in the result
    collection "no education needed" and query 'systemutvecklare +tandläkare' cancels each other out
    """
    # Arrange
    collection_id = "UdVa_jRr_9DE"  # 'Yrkessamling, yrken utan krav på utbildning'

    occupation_collection_query = {"occupation-collection": collection_id, "limit": 0}
    freetext_query = {"q": "systemutvecklare +tandläkare", "limit": 0}
    combined_query = {**occupation_collection_query, **freetext_query, "limit": 0}

    # Act
    response_occupation_collection = client.get("/search", query_string=occupation_collection_query)
    response_freetext = client.get("/search", query_string=freetext_query)
    response_combined = client.get("/search", query_string=combined_query)

    # Assert
    number_of_ads_collection = response_occupation_collection.json["total"]["value"]
    number_of_ads_q = response_freetext.json["total"]["value"]
    number_of_ads_combo = response_combined.json["total"]["value"]

    assert number_of_ads_collection > 0
    assert number_of_ads_q > 0
    assert number_of_ads_combo == 0  # current behavior
    assert (
        number_of_ads_collection > number_of_ads_q
    )  # assumption that a collection should return more hits than a query


def test_plus_minus(client):
    """
    Do a query with two occupation collections and save the number of hits in the results
    Do a new search with minus in front of one of the params sand save the number of hits
    Check that the first search have more hits than the second search (the one with minus)
    """
    # Arrange
    query_a = {"occupation-collection": ["UdVa_jRr_9DE", "ja7J_P8X_YC9"], "limit": 0}
    query_b = {"occupation-collection": ["UdVa_jRr_9DE", "-ja7J_P8X_YC9"], "limit": 0}

    # Act
    response_a = client.get("/search", query_string=query_a)
    response_b = client.get("/search", query_string=query_b)

    # Assert
    # Check that both queries return more than 0 hits
    assert response_a.json["total"]["value"] > 0
    assert response_b.json["total"]["value"] > 0

    # Check that the first query returns more hits than the second query
    assert response_a.json["total"]["value"] > response_b.json["total"]["value"]


def test_collection_positive_negative(client):
    """
    Queries:
    1. Get the total number of ads
    2. Include the occupation collection "utan krav på erfarenhet"
    3. Exclude that collection (prefix with minus)
    Compare results
    """
    # Arrange
    query_plus = {"occupation-collection": "UdVa_jRr_9DE", "limit": 0}
    query_minus = {"occupation-collection": "-UdVa_jRr_9DE", "limit": 0}

    # Act
    response_total = client.get("/search", query_string={})
    response_plus = client.get("/search", query_string=query_plus)
    response_minus = client.get("/search", query_string=query_minus)

    total = response_total.json["total"]["value"]
    number_inclusive = response_plus.json["total"]["value"]
    number_exclusive = response_minus.json["total"]["value"]

    assert total - number_exclusive == number_inclusive


@pytest.mark.parametrize("occupation_field", ["RPTn_bxG_ExZ", "E7hm_BLq_fqZ", "MVqp_eS8_kDZ"])
def test_occupation_field_returns_data(client, occupation_field):
    """
    Test that the 'occupation-field' parameter returns data
    """
    # Arrange
    query = {"occupation-field": occupation_field, "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] > 0


def test_occupation_field_returns_all_ads_with_empty_string(client):
    """
    Test that the 'occupation-field' parameter returns all ads when empty string
    """
    # Arrange
    query = {"occupation-name": "", "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] > 1000  # should return all ads


def test_occupation_field_returns_data_with_multiple_values(client):
    """
    Test that the 'occupation-field' parameter returns data with multiple values
    """
    # Arrange
    occupation_fields = ["RPTn_bxG_ExZ", "E7hm_BLq_fqZ", "MVqp_eS8_kDZ"]
    query = {"occupation-field": occupation_fields, "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] > 0
