import pytest


@pytest.mark.parametrize(
    "q, bool_method, expected_count",
    [
        ["", "and", 5029],
        ["Sirius crew", "and", 0],
        ["Sirius crew", "or", 4],
        ["Sirius crew", None, 4],
        pytest.param("lagstiftning anställning ", "and", 17, marks=pytest.mark.smoke),
        ["lagstiftning anställning ", "or", 1066],
        ["lagstiftning anställning ", None, 1066],
        ["TechBuddy uppdrag", "and", 1],
        ["TechBuddy uppdrag", "or", 1403],
        ["TechBuddy uppdrag", None, 1403],
    ],
)
def test_freetext_bool_method(q, bool_method, expected_count, client):
    """
    Test with 'or' & 'and' values for X_FEATURE_FREETEXT_BOOL_METHOD header flag
    Default value is 'OR' (used in test cases with None as param)
    Searches with 'or' returns more hits
    """
    # Arrange
    query = {"q": q, "limit": 0}

    # use default setting for X_FEATURE_FREETEXT_BOOL_METHOD == 'OR'
    headers = {"X_FEATURE_FREETEXT_BOOL_METHOD": bool_method} if bool_method else {}

    # Act
    response = client.get("/search", query_string=query, headers=headers)

    assert response.json["total"]["value"] == expected_count
