import pytest

from tests.test_resources.concept_ids import concept_ids_geo as geo


@pytest.mark.parametrize("municipality", [geo.stockholm, geo.malmo, geo.varnamo, geo.kristianstad, geo.karlskrona])
def test_search_municipality(municipality, client):
    """
    Check that all hits have correct municipality concept id when searching for ads in a municipality.
    """
    # Arrange
    query = {"municipality": municipality, "limit": 100}

    # Act
    response = client.get("/search", query_string=query)
    hits = response.json["hits"]

    # Assert
    assert response.json["total"]["value"] > 0

    assert all([hit["workplace_address"]["municipality_concept_id"] == municipality for hit in hits])


@pytest.mark.parametrize("region", [geo.norrbottens_lan, geo.skane_lan, geo.kronobergs_lan])
def test_search_region(region, client):
    """
    Check that all hits have correct region concept id when searching for ads in a region.
    """
    # Arrange
    query = {"region": region, "limit": 100}

    # Act
    response = client.get("/search", query_string=query)
    hits = response.json["hits"]

    # Assert
    assert response.json["total"]["value"] > 0

    assert all([hit["workplace_address"]["region_concept_id"] == region for hit in hits])


@pytest.mark.parametrize("country", [geo.sverige, geo.norge, geo.malta, geo.schweiz])
def test_search_country(country, client):
    """
    Check that all hits have correct country concept id when searching for ads in a country.
    """

    # Arrange
    query = {"country": country, "limit": 100}

    # Act
    response = client.get("/search", query_string=query)
    hits = response.json["hits"]

    # Assert
    assert response.json["total"]["value"] > 0

    assert all([hit["workplace_address"]["country_concept_id"] == country for hit in hits])


@pytest.mark.parametrize(
    "query, expected_count",
    [
        [{"position": "59.3,18.0"}, 65],
        [{"position": "59.3,18.0", "position.radius": 6}, 718],
        [{"position": "59.3,18.0", "position.radius": 10}, 875],
        pytest.param({"position": "59.3,18.0", "position.radius": 50}, 1176, marks=pytest.mark.smoke),
        [{"position": "59.3,18.0", "position.radius": 100}, 1500],
        [{"position": "56.9,12.5", "position.radius": 50}, 104],
        [{"position": "56.9,12.5", "position.radius": 10}, 21],
        [{"position": "18.0,59.3"}, 0],  # lat long reversed
    ],
)
def test_position_search_returns_expected_number_of_results(query, expected_count, client):
    """
    Test 'position' query parameter along with 'position-radius'
    With larger radius, more hits are returned
    """
    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count
