import pytest

import tests.test_resources.concept_ids.concept_ids_geo as geo


@pytest.mark.smoke
def test_abroad_true_returns_hits(client):
    """
    Test that 'arbete-utomlands' set to True returns hits
    """
    # Arrange
    query = {"abroad": True, "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.status_code == 200
    assert response.json["total"]["value"] > 0


@pytest.mark.smoke
def test_abroad_false_returns_hits(client):
    """
    Test that 'arbete-utomlands' set to False returns hits
    """
    # Arrange
    query = {"abroad": False, "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.status_code == 200
    assert response.json["total"]["value"] > 0


def test_abroad_true_has_correct_country_value(client):
    """
    Test that 'arbete-utomlands' set to True returns hits with country value not equal to 'Sverige'
    """
    # Arrange
    query = {"abroad": True, "limit": 100}

    # Act
    response = client.get("/search", query_string=query)
    hits = response.json["hits"]

    # Assert
    assert response.status_code == 200
    assert len(hits) > 0

    assert all(hit["workplace_address"]["country"] != "Sverige" for hit in hits)
    assert all(hit["workplace_address"]["country_concept_id"] != "i46j_HmG_v64" for hit in hits)


@pytest.mark.parametrize(
    "geo_param",
    [
        {"municipality": geo.vasteras},
        pytest.param({"municipality": [geo.uppsala, geo.linkoping]}, marks=pytest.mark.smoke),
        {"region": geo.ostergotlands_lan},
        {"region": [geo.skane_lan, geo.norrbottens_lan]},
        {"unspecified-sweden-workplace": True},
    ],
)
def test_abroad_with_other_geographical_params(geo_param, client):
    """
    if 'arbete-utomlands' is True AND any geographical filter is used (municipality, region, 'unspecified-sweden-workplace')
    the result should include hits for BOTH work abroad and for the Swedish geographical filter
    """
    # Arrange
    abroad_only_query = {"abroad": True, "limit": 0}
    geo_only_query = {**geo_param, "limit": 0}
    geo_with_abroad_query = {**geo_param, "abroad": True, "limit": 0}

    # Act
    abroad_only_response = client.get("/search", query_string=abroad_only_query)
    geo_only_response = client.get("/search", query_string=geo_only_query)
    geo_with_abroad_response = client.get("/search", query_string=geo_with_abroad_query)

    abroad_only_hits = abroad_only_response.json["total"]["value"]
    geo_only_hits = geo_only_response.json["total"]["value"]
    geo_with_abroad_hits = geo_with_abroad_response.json["total"]["value"]

    # Assert
    assert abroad_only_hits > 0
    assert geo_only_hits > 0
    assert geo_with_abroad_hits > abroad_only_hits
    assert geo_with_abroad_hits > geo_only_hits


@pytest.mark.parametrize(
    "country",
    [
        geo.norge,
        geo.schweiz,
        pytest.param([geo.norge, geo.schweiz, geo.aland_tillhor_finland], marks=pytest.mark.smoke),
    ],
)
def test_abroad_in_combination_with_country(country, client):
    """
    'arbete-utomlands' set to True alone OR in combination  with country filter for other countries
     should give the same number of results.
    """
    # Arrange
    abroad_query = {"abroad": True, "limit": 100}
    abroad_with_country_query = {"country": country, "abroad": True, "limit": 100}

    # Act
    abroad_response = client.get("/search", query_string=abroad_query)
    abroad_with_country_response = client.get("/search", query_string=abroad_with_country_query)

    # Assert
    assert abroad_response.json["total"]["value"] > 0
    assert abroad_response.json["total"]["value"] == abroad_with_country_response.json["total"]["value"]


@pytest.mark.parametrize(
    "geo_param",
    [
        {"municipality": geo.stockholm},
        pytest.param({"municipality": [geo.sundsvall, geo.goteborg]}, marks=pytest.mark.smoke),
        {"region": geo.vastra_gotalands_lan},
        {"region": [geo.blekinge_lan, geo.vasterbottens_lan]},
    ],
)
def test_abroad_false_does_not_affect_results(geo_param, client):
    """
    Search with municipality or region concept ids should not be affected if ARBETE_UTOMLANDS is False
    if the parameter ARBETE_UTOMLANDS is missing or False, the search result should not be affected
    """
    # Arrange
    regular_query = {**geo_param, "limit": 0}
    with_abroad_query = {**geo_param, "abroad": False, "limit": 0}

    # Act
    regular_response = client.get("/search", query_string=regular_query)
    with_abroad_response = client.get("/search", query_string=with_abroad_query)

    # Assert
    assert regular_response.json["total"]["value"] > 0
    assert regular_response.json["total"]["value"] == with_abroad_response.json["total"]["value"]
