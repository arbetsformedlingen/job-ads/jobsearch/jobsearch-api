import pytest

from tests.test_resources.test_settings import TEST_USE_STATIC_DATA

LABEL_NYSTARTSJOBB = "nystartsjobb"
LABEL_REKRYTERINGSUTBILDNING = "rekryteringsutbildning"
LABEL_DUMMY = "dummy_label"
LABEL_SWEDISH_CHARS = "räksmörgås"


@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on a fixed set of ads")
@pytest.mark.parametrize(
    "label, expected_count",
    [
        [LABEL_NYSTARTSJOBB, 251],
        [LABEL_REKRYTERINGSUTBILDNING, 248],
    ],
)
def test_freetext_label_search(label, expected_count, client):
    # Arrange
    query = {"q": label, "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count


@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on a fixed set of ads")
@pytest.mark.parametrize(
    "labels, expected_count",
    [
        [f"{LABEL_NYSTARTSJOBB} {LABEL_DUMMY}", 299],
        [
            f"{LABEL_REKRYTERINGSUTBILDNING} kalleankaordutanträff",
            0,
        ],  # zero hits since 'rekryteringsutbildning' is both enriched and a label, together with a non enriched term the result is zero hits
        [f"{LABEL_NYSTARTSJOBB} kalleankaordutanträff", 251],
        [f"python {LABEL_NYSTARTSJOBB}", 5],
    ],
)
def test_freetext_label_search_multiple_labels(labels, expected_count, client):
    # Arrange
    query = {"q": labels, "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count


@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on a fixed set of ads")
@pytest.mark.parametrize(
    "labels, expected_count",
    [
        [LABEL_SWEDISH_CHARS, 10],
        [
            f"{LABEL_REKRYTERINGSUTBILDNING} {LABEL_SWEDISH_CHARS}",
            2,
        ],
        [f"{LABEL_NYSTARTSJOBB} {LABEL_SWEDISH_CHARS}", 260],
    ],
)
def test_freetext_label_search_swedish_chars(labels, expected_count, client):
    # Arrange
    query = {"q": labels, "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count


@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on a fixed set of ads")
def test_freetext_label_search_anycase(client):
    # Arrange
    query = {"q": "wEirD_CaSE_labEL", "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == 10


@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on a fixed set of ads")
@pytest.mark.parametrize(
    "label, expected_count",
    [
        ["-nystartsjobb", 4778],
        ["-rekryteringsutbildning", 4781],
    ],
)
def test_negative_freetext_label_search(label, expected_count, client):
    # Arrange
    query = {"q": label, "limit": 100}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count
