import pytest

NYSTARTSJOBB = "nystartsjobb"
REKRYTERINGSUTBILDNING = "rekryteringsutbildning"

NUMBER_NYSTARTSJOBB = 249
NUMBER_REKRYTERINGSUTBILDNING = 247
NUMBER_BOTH = 15

"""
Tests for parameter "label" which has values that are generally known but can change when new labels ("nyckelord") are added in Ledigt Arbete.
Label can be used to include ads which has a a specific label.
If multiple labels are used, the relation between them is AND

["label-1", "label-2"] == label-1 AND label-2

Test data LA mock api 2024-01-23:
  [["nystartsjobb"], 232],
  [["rekryteringsutbildning"], 226],
  [["dummy_label"], 44],
  [["nystartsjobb", "rekryteringsutbildning"], 14],
  [["weird_case_label"], 10],
  [["räksmörgås"], 7],
  [["rekryteringsutbildning", "dummy_label"], 4],
  [["rekryteringsutbildning", "räksmörgås"], 2],
  [["nystartsjobb", "räksmörgås"], 1],
  [["nystartsjobb", "dummy_label"], 1],
  [["nystartsjobb", "rekryteringsutbildning", "dummy_label"], 1]
"""


@pytest.mark.smoke
@pytest.mark.parametrize(
    "label, expected_count",
    [
        [NYSTARTSJOBB, NUMBER_NYSTARTSJOBB],
        [REKRYTERINGSUTBILDNING, NUMBER_REKRYTERINGSUTBILDNING],
        ["räksmörgås", 10],
        ["weird_case_label", 10],
    ],
)
def test_single_label(label, expected_count, client):
    """
    Returns ads that fulfills any of these conditions:
    - has only this label
    - has this label in combination with other labels
    """
    # Arrange
    query = {"label": label, "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count


@pytest.mark.smoke
@pytest.mark.parametrize(
    "label, expected_count",
    [
        [[REKRYTERINGSUTBILDNING, NYSTARTSJOBB], NUMBER_BOTH],
        [[NYSTARTSJOBB, REKRYTERINGSUTBILDNING], NUMBER_BOTH],
    ],
)
def test_multiple_labels(label, expected_count, client):
    """
    Test that label includes ads
    """
    # Arrange
    query = {"label": label, "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count


def test_label_unknown(client):
    """
    Test that label includes or excludes unknown labels
    the negative filtering should have no effect
    """
    # Arrange
    query = {"label": "unknown", "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == 0


def test_label_with_unknown(client):
    """
    Combine known and unknown labels
    """
    # Arrange
    query = {"label": [REKRYTERINGSUTBILDNING, "unknown"], "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == 0


def test_repeat_label(client):
    """
    Repeating labels should not change result
    """
    # Arrange
    query = {"label": [NYSTARTSJOBB, NYSTARTSJOBB], "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == NUMBER_NYSTARTSJOBB


def test_label_field_in_result_model(client):
    """
    Test that the field label is in the result model.
    """
    # Arrange
    query = {"label": [REKRYTERINGSUTBILDNING], "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] > 0

    for hit in response.json["hits"]:
        assert REKRYTERINGSUTBILDNING in hit["label"]
