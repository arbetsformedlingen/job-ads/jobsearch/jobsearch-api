import pytest


@pytest.mark.smoke
@pytest.mark.parametrize(
    "limit, stats_limit, expected_number_of_hits, expected_number_of_values",
    [
        (10, 0, 10, 5),
        (10, 10, 10, 10),
        (10, 15, 10, 15),
    ],
)
def test_stats(limit, stats_limit, expected_number_of_hits, expected_number_of_values, client):
    # Arrange
    query = {"stats": "region", "offset": 0, "limit": limit, "stats.limit": stats_limit}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    # Check that the number of hits returned are as expected
    assert len(response.json["hits"]) == expected_number_of_hits

    # Check that the number of values in stats are as expected
    assert len(response.json["stats"][0]["values"]) == expected_number_of_values


def test_stats_details(client):
    # Arrange
    query = {"stats": "region", "offset": 0, "limit": 0, "stats.limit": 5}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["stats"][0]["values"] == [
        {
            "code": "01",
            "concept_id": "CifL_Rzy_Mku",
            "count": 1227,
            "term": "Stockholms län",
        },
        {
            "code": "14",
            "concept_id": "zdoY_6u5_Krt",
            "count": 779,
            "term": "Västra Götalands län",
        },
        {
            "code": "12",
            "concept_id": "CaRE_1nn_cSU",
            "count": 643,
            "term": "Skåne län",
        },
        {
            "code": "05",
            "concept_id": "oLT3_Q9p_3nn",
            "count": 249,
            "term": "Östergötlands län",
        },
        {
            "code": "06",
            "concept_id": "MtbE_xWT_eMi",
            "count": 195,
            "term": "Jönköpings län",
        },
    ]
