import pytest

from common.constants import (
    ABROAD,
    EXPERIENCE_REQUIRED,
    MUNICIPALITY,
    OCCUPATION,
    OCCUPATION_FIELD,
    OCCUPATION_GROUP,
    PUBLISHED_AFTER,
    PUBLISHED_BEFORE,
    REMOTE,
    UNSPECIFIED_SWEDEN_WORKPLACE,
)
from tests.test_resources.concept_ids import concept_ids_geo as geo
from tests.test_resources.concept_ids import occupation, occupation_field, occupation_group
from tests.test_resources.test_settings import NUMBER_OF_ADS

TEST_DATE = "2020-12-10T23:59:59"
NUMBER_OF_REMOTE_ADS = 36


@pytest.mark.parametrize(
    "query, expected_count",
    [
        [{REMOTE: True, "q": "utvecklare"}, 2],
        [{REMOTE: False, "q": "utvecklare"}, 156],
        [{REMOTE: None, "q": "utvecklare"}, 158],
        [{REMOTE: True, "q": "säljare"}, 1],
        pytest.param({REMOTE: False, "q": "säljare"}, 224, marks=pytest.mark.smoke),
        [{REMOTE: None, "q": "säljare"}, 225],
        [{REMOTE: True, OCCUPATION: occupation.saljkonsulent}, 0],
        [{REMOTE: None, OCCUPATION: occupation.saljkonsulent}, 4],
        [{OCCUPATION: occupation.saljkonsulent}, 4],
        [{OCCUPATION: occupation.mjukvaruutvecklare}, 50],
        [{REMOTE: True, OCCUPATION: occupation.mjukvaruutvecklare}, 1],
    ],
)
def test_query_remote_and_occupation(query, expected_count, client):
    """
    AND condition between REMOTE and other params
    """
    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count


@pytest.mark.parametrize(
    "query, expected_count",
    [
        [{REMOTE: True, OCCUPATION_GROUP: occupation_group.mjukvaru__och_systemutvecklare_m_fl_}, 4],
        [{REMOTE: None, OCCUPATION_GROUP: occupation_group.mjukvaru__och_systemutvecklare_m_fl_}, 239],
        pytest.param(
            {REMOTE: False, OCCUPATION_GROUP: occupation_group.mjukvaru__och_systemutvecklare_m_fl_},
            235,
            marks=pytest.mark.smoke,
        ),
        [{REMOTE: True, OCCUPATION_GROUP: occupation_group.telefonforsaljare_m_fl_}, 2],
        [{REMOTE: None, OCCUPATION_GROUP: occupation_group.telefonforsaljare_m_fl_}, 75],
        [{REMOTE: True, OCCUPATION_GROUP: occupation_group.foretagssaljare}, 2],
    ],
)
def test_query_remote_and_occupation_group(query, expected_count, client):
    """
    AND condition between REMOTE and other params
    """
    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count


@pytest.mark.parametrize(
    "query, expected_count",
    [
        [{REMOTE: True, OCCUPATION_FIELD: occupation_field.data_it}, 10],
        [{REMOTE: None, OCCUPATION_FIELD: occupation_field.data_it}, 421],
        [{REMOTE: True, OCCUPATION_FIELD: occupation_field.forsaljning__inkop__marknadsforing}, 5],
        pytest.param(
            {REMOTE: None, OCCUPATION_FIELD: occupation_field.forsaljning__inkop__marknadsforing},
            578,
            marks=pytest.mark.smoke,
        ),
        [{REMOTE: False, OCCUPATION_FIELD: occupation_field.forsaljning__inkop__marknadsforing}, 573],
    ],
)
def test_query_remote_and_occupation_field(query, expected_count, client):
    """
    AND condition between REMOTE and other params
    """
    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count


@pytest.mark.parametrize(
    "query, expected_count",
    [
        [{"q": "Stockholm"}, 844],
        [{REMOTE: True, "q": "Stockholm"}, 13],
        [{REMOTE: False, "q": "Stockholm"}, 831],
        pytest.param({MUNICIPALITY: geo.stockholm}, 775, marks=pytest.mark.smoke),
        [{REMOTE: True, MUNICIPALITY: geo.stockholm}, 13],
        [{REMOTE: False, MUNICIPALITY: geo.stockholm}, 762],
    ],
)
def test_query_remote_and_municipality(query, expected_count, client):
    """
    AND condition between REMOTE and other params
    """
    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count


@pytest.mark.parametrize(
    "query, expected_count",
    [
        [{"q": "Stockholms Län"}, 1228],
        pytest.param({REMOTE: True, "q": "Stockholms Län"}, 14, marks=pytest.mark.smoke),
        pytest.param({REMOTE: False, "q": "Stockholms Län"}, 1214, marks=pytest.mark.smoke),
        [{"region": geo.stockholms_lan}, 1227],
        [{REMOTE: True, "region": geo.stockholms_lan}, 14],
        [{REMOTE: None, "region": geo.vastra_gotalands_lan}, 779],
        [{REMOTE: True, "region": geo.vastra_gotalands_lan}, 3],
        pytest.param({REMOTE: False, "region": geo.vastra_gotalands_lan}, 776, marks=pytest.mark.smoke),
        [{"region": geo.vastra_gotalands_lan, "q": "säljare"}, 26],
        [{REMOTE: True, "region": geo.vastra_gotalands_lan, "q": "säljare"}, 0],
    ],
)
def test_query_remote_and_region(query, expected_count, client):
    """
    AND condition between REMOTE and other params
    """
    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count


@pytest.mark.parametrize(
    "query, expected_count",
    [
        [{PUBLISHED_AFTER: TEST_DATE}, 4935],
        [{REMOTE: True, PUBLISHED_AFTER: TEST_DATE}, 32],
        pytest.param({PUBLISHED_BEFORE: TEST_DATE}, 94, marks=pytest.mark.smoke),
        [{REMOTE: True, PUBLISHED_BEFORE: TEST_DATE}, 4],
    ],
)
def test_query_remote_and_publication_dates(query, expected_count, client):
    """
    AND condition between REMOTE and other params
    """
    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count


@pytest.mark.parametrize(
    "query, expected_count",
    [
        pytest.param({ABROAD: True}, 30, marks=pytest.mark.smoke),
        pytest.param({REMOTE: True, ABROAD: False}, NUMBER_OF_REMOTE_ADS, marks=pytest.mark.smoke),
        [{ABROAD: False}, NUMBER_OF_ADS],
    ],
)
def test_query_remote_and_abroad(query, expected_count, client):
    """
    AND condition between REMOTE and other params
    """
    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count


@pytest.mark.parametrize(
    "query, expected_count",
    [
        pytest.param({UNSPECIFIED_SWEDEN_WORKPLACE: True}, 135, marks=pytest.mark.smoke),
        [{UNSPECIFIED_SWEDEN_WORKPLACE: False}, NUMBER_OF_ADS],
        [{REMOTE: True, UNSPECIFIED_SWEDEN_WORKPLACE: True}, 2],
        [{REMOTE: True, UNSPECIFIED_SWEDEN_WORKPLACE: False}, NUMBER_OF_REMOTE_ADS],
    ],
)
def test_query_remote_and_unspecified_workplace(query, expected_count, client):
    """
    AND condition between REMOTE and other params
    """
    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count


@pytest.mark.parametrize(
    "query, expected_count",
    [
        [{EXPERIENCE_REQUIRED: True}, 3938],
        [{EXPERIENCE_REQUIRED: False}, 1091],
        pytest.param({REMOTE: True, EXPERIENCE_REQUIRED: True}, 31, marks=pytest.mark.smoke),
        [{REMOTE: False, EXPERIENCE_REQUIRED: True}, 3907],
        [{REMOTE: True, EXPERIENCE_REQUIRED: False}, 5],
        [{REMOTE: False, EXPERIENCE_REQUIRED: False}, 1086],
    ],
)
def test_experience(query, expected_count, client):
    """
    AND condition between REMOTE and other params
    """
    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count


@pytest.mark.parametrize(
    "query, expected_count",
    [
        [{REMOTE: True, "q": "-Stockholm"}, 23],
        [{REMOTE: True, MUNICIPALITY: f"-{geo.stockholm}"}, 23],
        [{REMOTE: True, "region": f"-{geo.stockholms_lan}"}, 22],
        [{REMOTE: True, "region": f"-{geo.vastra_gotalands_lan}"}, 33],
        pytest.param({REMOTE: True, "region": f"-{geo.skane_lan}"}, 31, marks=pytest.mark.smoke),
        [{REMOTE: True, "region": f"-{geo.norrbottens_lan}"}, 36],
    ],
)
def test_query_remote_and_negative_geography(query, expected_count, client):
    """
    Negative geographical parameters
    AND condition between REMOTE and other params
    """
    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count


def test_that_ads_for_municipality_are_either_remote_or_local(client):
    """
    numbers for REMOTE True + REMOTE False should add upp to numbers when not using REMOTE
    """
    # Arrange
    query_municipality_total = {"municipality": geo.stockholm}
    query_number_remote = {REMOTE: True, "municipality": geo.stockholm}
    query_number_local = {REMOTE: False, "municipality": geo.stockholm}

    # Act
    response_municipality_total = client.get("/search", query_string=query_municipality_total)
    response_number_remote = client.get("/search", query_string=query_number_remote)
    response_number_local = client.get("/search", query_string=query_number_local)

    # Assert
    municipality_total = response_municipality_total.json["total"]["value"]
    number_remote = response_number_remote.json["total"]["value"]
    number_local = response_number_local.json["total"]["value"]

    assert number_remote + number_local == municipality_total


def test_that_ads_for_region_are_either_remote_or_local(client):
    """
    numbers for REMOTE True + REMOTE False should add upp to numbers when not using REMOTE
    AND condition between REMOTE and other params
    """
    # Arrange
    query_region_total = {"region": geo.stockholms_lan}
    query_number_remote = {REMOTE: True, "region": geo.stockholms_lan}
    query_number_local = {REMOTE: False, "region": geo.stockholms_lan}

    # Act

    response_region_total = client.get("/search", query_string=query_region_total)
    response_number_remote = client.get("/search", query_string=query_number_remote)
    response_number_local = client.get("/search", query_string=query_number_local)

    # Assert
    region_total = response_region_total.json["total"]["value"]
    number_remote = response_number_remote.json["total"]["value"]
    number_local = response_number_local.json["total"]["value"]

    assert number_remote + number_local == region_total


@pytest.mark.parametrize(
    "query, expected_count",
    [
        [{REMOTE: False, REMOTE: True}, NUMBER_OF_REMOTE_ADS],  # noqa: F602
        pytest.param({REMOTE: True, REMOTE: False}, NUMBER_OF_ADS - NUMBER_OF_REMOTE_ADS, marks=pytest.mark.smoke),
        [{REMOTE: True, REMOTE: False, REMOTE: True}, NUMBER_OF_REMOTE_ADS],
    ],
)
def test_query_with_duplicate_remote_param(query, expected_count, client):
    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count
