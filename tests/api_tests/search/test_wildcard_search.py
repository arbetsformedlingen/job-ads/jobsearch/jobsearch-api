import pytest


@pytest.mark.parametrize(
    "q, expected_hits",
    [
        ["murar*", 1],
        ["*utvecklare", 183],
        ["utvecklare*", 93],
        pytest.param(
            "*utvecklare*",
            0,
            marks=pytest.mark.smoke,
        ),  # Multiple wildcards are not supported
        pytest.param(
            "ut*are",
            3,
            marks=pytest.mark.smoke,
        ),
        ["Anläggningsarbetar*", 6],
        ["Arbetsmiljöingenjö*", 1],
        ["Behandlingsassisten*", 10],
        ["Bilrekonditionerar*", 3],
        ["Eventkoordinato*", 0],
        ["Fastighetsförvaltar*", 9],
        pytest.param("Fastighetsskötar*", 16, marks=pytest.mark.smoke),
        ["Fastighet*", 244],
        ["Kundtjänstmedarbetar*", 26],
        ["Kundtjänst*", 107],
        ["sjukskö*", 651],
        ["sköterska*", 7],
        ["skötersk*", 11],
        ["sjukvårds*tion", 0],
        [
            "sj",
            0,
        ],  # min 3 characters
        [
            "sj*",
            0,
        ],  # min 3 characters
        ["sju*", 1095],
    ],
)
def test_freetext_wildcard_search_returns_hits(q, expected_hits, client):
    """
    Test different wildcard queries
    check that the number of results is exactly as expected
    """
    # Arrange
    query = {"q": q, "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_hits
