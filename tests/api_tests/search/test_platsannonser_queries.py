from datetime import datetime

import pytest
from dateutil import parser

from common import constants, fields, taxonomy
from tests.test_resources.concept_ids import occupation as work
from tests.test_resources.concept_ids import occupation_field as field
from tests.test_resources.concept_ids import occupation_group as group
from tests.test_resources.concept_ids import taxonomy_values as other
from tests.test_resources.test_settings import NUMBER_OF_ADS


@pytest.mark.smoke
def test_freetext_query_one_param(client):
    # Arrange
    query = {"q": "gymnasielärare"}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == 62


def test_enrich(client):
    # Arrange
    query = {"q": "stresstålig"}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == 164


# Todo: different queries
@pytest.mark.parametrize(
    "minimum_relevance, expected_count",
    [
        (0, 6),
        (1, 3),
        (2, 0),
        (3, 0),
    ],
)
def test_min_relevance_new(minimum_relevance, expected_count, client):
    # Arrange
    query = {"q": "sjuksköterska grundutbildad", constants.MIN_RELEVANCE: minimum_relevance}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count


@pytest.mark.parametrize(
    "query, expected_count",
    [
        [{"q": "python"}, 97],
        [{"q": "python php"}, 105],
        [{"q": "+python php"}, 97],
        [{"q": "+python -php"}, 95],
        pytest.param({"q": "-python -php"}, 4924, marks=pytest.mark.smoke),
        [{"q": "php"}, 10],
        [{"q": "systemutvecklare +python java linux mac"}, 32],
        [{"q": "systemutvecklare +python -java linux mac"}, 22],
        [{"q": "systemutvecklare python java php"}, 68],
        [{"q": "systemutvecklare -python java php"}, 36],
        [{"q": "systemutvecklare python java -php"}, 60],
        [{"q": "lärarexamen"}, 30],
        [{"q": "lärarexamen -lärare"}, 8],
        [{"q": "sjuksköterska"}, 494],
        [{"q": "sjuksköterska -stockholm"}, 431],
        [{"q": "sjuksköterska -malmö"}, 473],
        [{"q": "sjuksköterska -stockholm -malmö"}, 410],
        pytest.param(
            {"q": "sjuksköterska -stockholm -malmö -göteborg -eskilstuna"},
            381,
            marks=pytest.mark.smoke,
        ),
        [{"q": "sjuksköterska Helsingborg -stockholm -malmö -göteborg -eskilstuna"}, 8],
        [{"q": "+it-arkitekt"}, 12],
        [{"q": "-it-arkitekt"}, 5017],
        [{"q": "arkitekt -it-arkitekt"}, 6],
        [{"q": "arkitekt +it-arkitekt"}, 12],
        pytest.param({"q": "+it-arkitekt arkitekt"}, 12, marks=pytest.mark.smoke),
        [{"q": 'arkitekt -"it-arkitekt"'}, 7],
        [{"q": '"it-arkitekt" arkitekt'}, 9],
        [{"q": '"it-arkitekt" arkitekt'}, 9],
        pytest.param({"q": "c-körkort"}, 30, marks=pytest.mark.smoke),
        [{"q": "-c-körkort"}, 4999],
        [{"q": "cad-verktyg"}, 1],
        [{"q": "-cad-verktyg"}, 5028],
        [{"q": "erp-system"}, 7],
        [{"q": "-erp-system"}, 5022],
        [{"q": "it-tekniker"}, 24],
        [{"q": "-it-tekniker"}, 5005],
        [{"q": "backend-utvecklare"}, 19],
        pytest.param({"q": "-backend-utvecklare"}, 5010, marks=pytest.mark.smoke),
    ],
)
def test_freetext_search_with_plus_and_minus_modifiers(query, expected_count, client):
    """
    Tests search with plus and minus modifiers
    """
    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count


@pytest.mark.parametrize(
    "query, expected_count",
    [
        [{"q": "sjukssköterska"}, 494],
        pytest.param({"q": "javasscript"}, 61, marks=pytest.mark.smoke),
        [{"q": "montesori"}, 2],
    ],
)
def test_search_query_with_misspelled_param(query, expected_count, client):
    """
    Test search with misspelled parameter
    """
    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count


@pytest.mark.parametrize(
    "query, expected_count",
    [
        pytest.param({"q": "c++"}, 44, marks=pytest.mark.smoke),
        [{"q": "c#"}, 52],
    ],
)
def test_search_query_with_special_characters(query, expected_count, client):
    """
    Test search with special characters
    """
    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count


@pytest.mark.parametrize(
    "query, expected_count",
    [
        [{"q": "kista"}, 12],
        [{"q": "gärdet"}, 6],
        [{"q": "stockholm"}, 844],
        [{"q": "skåne"}, 656],
        pytest.param({"q": "värmland"}, 106, marks=pytest.mark.smoke),
        [{"q": "örebro"}, 85],
        [{"q": "örebro län"}, 129],
    ],
)
def test_freetext_query_with_geographical_concept(query, expected_count, client):
    """
    Test search with geographical concept
    """
    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count


def test_bugfix_reset_query_rewrite_location(client):
    # Arrange
    query = {"q": "rissne", "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] > 0


@pytest.mark.parametrize(
    "query, expected_count",
    [
        [{"q": "kista kallhäll"}, 13],
        [{"q": "vara"}, 13],
        [{"q": "kallhäll"}, 1],
        [{"q": "kallhäll introduktion"}, 1],
        pytest.param({"q": "kallhäll ystad"}, 17, marks=pytest.mark.smoke),
        [{"q": "stockholm malmö"}, 1071],
        [{"q": "rissne"}, 1],
        [{"q": "skåne län"}, 650],
        [{"q": "skåne"}, 656],
        [{"q": "+trelleborg -stockholm ystad"}, 19],
        [{"q": "fridhemsplan"}, 1],
    ],
)
def test_freetext_query_location_extracted_or_enriched_or_freetext(query, expected_count, client):
    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count


def test_correct_error_message_with_too_large_offset_parameter(client):
    # Arrange
    query = {"offset": 2001, "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.status_code == 400
    assert response.is_json

    assert response.json["message"] == "Input payload validation failed"
    assert response.json["errors"]["offset"] == "Invalid argument: 2001. argument must be within the range 0 - 2000"


def test_total_hits(client):
    # Arrange
    query = {"offset": 0, "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == NUMBER_OF_ADS


def test_freetext_query_job_title_with_hyphen(client):
    # Arrange
    query = {"q": "HR-specialister", "limit": "0"}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    # Verify that correct concept is in response
    assert response.json["freetext_concepts"]["occupation"] == ["hr-specialist"]


def test_freetext_query_with_two_concepts_return_correct_number_of_ads(client):
    # Arrange
    query = {"q": "gymnasielärare lokförare"}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == 62


def test_publication_range(client):
    # Arrange
    date_from = datetime(2020, 12, 1, 0, 0, 0)
    date_until = datetime(2020, 12, 20, 0, 0, 0)
    query = {constants.PUBLISHED_AFTER: date_from, constants.PUBLISHED_BEFORE: date_until, "limit": 100}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    hits = response.json["hits"]
    assert len(hits) == 60

    assert all(parser.parse(hit[fields.PUBLICATION_DATE]) >= date_from for hit in hits)
    assert all(parser.parse(hit[fields.PUBLICATION_DATE]) <= date_until for hit in hits)


def test_driving_license_required_is_true(client):
    """
    Check expected amount of ads when driving license is required
    """
    # Arrange
    query = {taxonomy.DRIVING_LICENCE_REQUIRED: "true"}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    hits = response.json["hits"]
    assert len(hits) > 0

    # Verify that all hits have driving license required set to true
    assert all(hit[fields.DRIVING_LICENCE_REQUIRED] for hit in hits)


def test_driving_license_required_is_false(client):
    """
    Check expected amount of ads when driving license is not required
    """
    # Arrange
    query = {taxonomy.DRIVING_LICENCE_REQUIRED: "false"}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    hits = response.json["hits"]
    assert len(hits) > 0

    # Verify that all hits have driving license required set to false
    assert all(not hit[fields.DRIVING_LICENCE_REQUIRED] for hit in hits)


@pytest.mark.parametrize(
    "query, field, expected",
    [
        (
            {taxonomy.OCCUPATION: work.systemutvecklare_programmerare},
            fields.OCCUPATION,
            work.systemutvecklare_programmerare,
        ),
        (
            {taxonomy.GROUP: group.mjukvaru__och_systemutvecklare_m_fl_},
            fields.OCCUPATION_GROUP,
            group.mjukvaru__och_systemutvecklare_m_fl_,
        ),
        ({taxonomy.FIELD: field.data_it}, fields.OCCUPATION_FIELD, field.data_it),
    ],
)
def test_concept_id_in_search_results(query, field, expected, client):
    """
    Check that correct concept id is present in all returned matches.
    """
    # Act
    response = client.get("/search", query_string=query)

    # Assert
    hits = response.json["hits"]
    assert len(hits) > 0

    # Verify that all hits have correct occupation
    assert all(hit[field]["concept_id"] == expected for hit in hits)


@pytest.mark.parametrize(
    "query, field, expected,",
    [
        (
            {taxonomy.OCCUPATION: f"-{work.systemutvecklare_programmerare}"},
            fields.OCCUPATION,
            work.systemutvecklare_programmerare,
        ),
        ({taxonomy.FIELD: f"-{field.data_it}"}, fields.OCCUPATION_FIELD, field.data_it),
    ],
)
def test_concept_id_in_search_results_when_using_negative_match(query, field, expected, client):
    """
    Check that concept id is _not_ present in all returned matches.
    """
    # Act
    response = client.get("/search", query_string=query)

    # Assert
    hits = response.json["hits"]
    assert len(hits) > 0

    # Verify that all hits have correct occupation
    assert all(not hit[field]["concept_id"] == expected for hit in hits)


@pytest.mark.parametrize(
    "query, field, expected",
    [
        ({taxonomy.GROUP: "2512"}, fields.OCCUPATION_GROUP, "2512"),
        ({taxonomy.FIELD: "3"}, fields.OCCUPATION_FIELD, "3"),
    ],
)
def test_legacy_ams_taxonomy_id_in_search_results(query, field, expected, client):
    """
    Check that correct legacy ams taxonomy id is present in all returned matches.
    """
    # Act
    response = client.get("/search", query_string=query)

    # Assert
    hits = response.json["hits"]
    assert len(hits) > 0

    # Verify that all hits have correct occupation
    assert all(hit[field]["legacy_ams_taxonomy_id"] == expected for hit in hits)


@pytest.mark.parametrize(
    "query, field, expected",
    [
        ({taxonomy.GROUP: "-2512"}, fields.OCCUPATION_GROUP, "2512"),
        ({taxonomy.FIELD: "-3"}, fields.OCCUPATION_FIELD, "3"),
    ],
)
def test_legacy_ams_taxonomy_id_in_search_results_when_using_negative_match(query, field, expected, client):
    """
    Check that  legacy ams taxonomy id is _not_ present in all returned matches.
    """
    # Act
    response = client.get("/search", query_string=query)

    # Assert
    hits = response.json["hits"]
    assert len(hits) > 0

    # Verify that all hits have correct occupation
    assert all(not hit[field]["legacy_ams_taxonomy_id"] == expected for hit in hits)


def test_skill_concept_id_in_all_results(client):
    # Arrange
    query = {taxonomy.SKILL: "DHhX_uVf_y6X", "limit": 100}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    # Verify that concept id is found either in must_have or nice_to_have for all hits.
    for hit in response.json["hits"]:
        present_in_must_have = "DHhX_uVf_y6X" in [skill["concept_id"] for skill in hit["must_have"]["skills"]]
        present_in_nice_to_have = "DHhX_uVf_y6X" in [skill["concept_id"] for skill in hit["nice_to_have"]["skills"]]
        assert present_in_must_have or present_in_nice_to_have


def test_skill_concept_id_not_in_any_result_for_negative_search(client):
    # Arrange
    query = {taxonomy.SKILL: "-DHhX_uVf_y6X", "limit": 100}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    # Verify that concept id is not found either in must_have nor nice_to_have for all hits.
    for hit in response.json["hits"]:
        present_in_must_have = "DHhX_uVf_y6X" in [skill["concept_id"] for skill in hit["must_have"]["skills"]]
        present_in_nice_to_have = "DHhX_uVf_y6X" in [skill["concept_id"] for skill in hit["nice_to_have"]["skills"]]
        assert not (present_in_must_have or present_in_nice_to_have)


def test_worktime_extent(client):
    # Arrange
    query = {taxonomy.WORKTIME_EXTENT: "6YE1_gAC_R2G"}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    hits = response.json["hits"]
    assert len(hits) > 0

    # Verify that all hits have correct worktime extent
    assert all(hit[fields.WORKING_HOURS_TYPE]["concept_id"] == "6YE1_gAC_R2G" for hit in hits)


def test_scope_of_work(client):
    """
    Check that all returned results have scope of work within the specified range.
    """
    # Arrange
    query = {constants.PARTTIME_MIN: 50, constants.PARTTIME_MAX: 80, "limit": 100}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    hits = response.json["hits"]

    assert len(hits) > 0
    assert all(hit["scope_of_work"]["min"] >= 50 and hit["scope_of_work"]["max"] <= 80 for hit in hits)


def test_scope_of_work_results_include_lower_limit(client):
    """
    Check that at least one of the hits have exactly the lower limit.
    """
    # Arrange
    query = {constants.PARTTIME_MIN: 50, constants.PARTTIME_MAX: 80, "limit": 10}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    hits = response.json["hits"]

    assert any(hit["scope_of_work"]["min"] == 50 for hit in hits)


def test_scope_of_work_results_include_upper_limit(client):
    """
    Check that at least one of the hits have exactly the upper limit.
    """
    # Arrange
    query = {constants.PARTTIME_MIN: 50, constants.PARTTIME_MAX: 80, "limit": 10}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    hits = response.json["hits"]

    assert any(hit["scope_of_work"]["max"] == 80 for hit in hits)


@pytest.mark.parametrize(
    "query, expected_count",
    [
        [{"parttime.min": 50}, 4113],
        pytest.param({"parttime.min": 80}, 3914, marks=pytest.mark.smoke),
        [{"parttime.min": 20}, 4208],
    ],
)
def test_partime_min_returns_expected_number_of_results(query, expected_count, client):
    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count


@pytest.mark.parametrize(
    "query, expected_count",
    [
        [{"parttime.max": 80}, 103],
        [{"parttime.max": 20}, 4],
    ],
)
def test_partime_max_returns_expected_number_of_results(query, expected_count, client):
    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count


def test_driving_licence_search(client):
    """
    Check that all returned hits have the specified driving licence.
    """

    # Arrange
    query = {taxonomy.DRIVING_LICENCE: ["VTK8_WRx_GcM"], "limit": 100}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    hits = response.json["hits"]

    assert len(hits) > 0
    assert all("VTK8_WRx_GcM" in [l["concept_id"] for l in hit[fields.DRIVING_LICENCE]] for hit in hits)


def test_employment_type_search(client):
    # Arrange
    query = {taxonomy.EMPLOYMENT_TYPE: other.vanlig_anstallning_v1}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    hits = response.json["hits"]
    assert len(hits) > 0

    assert all(hit[fields.EMPLOYMENT_TYPE]["concept_id"] == other.vanlig_anstallning_v1 for hit in hits)


def test_experience_required_search(client):
    # Arrange
    query = {constants.EXPERIENCE_REQUIRED: True}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    hits = response.json["hits"]
    assert len(hits) > 0

    assert all(hit[fields.EXPERIENCE_REQUIRED] == True for hit in hits)


def test_experience_not_required_search(client):
    # Arrange
    query = {constants.EXPERIENCE_REQUIRED: False}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    hits = response.json["hits"]
    assert len(hits) > 0

    assert all(not hit[fields.EXPERIENCE_REQUIRED] == True for hit in hits)


@pytest.mark.parametrize("region_code", ["01", "03", "18"])
def test_region_search(region_code, client):
    # Arrange
    query = {taxonomy.REGION: region_code}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    hits = response.json["hits"]
    assert len(hits) > 0

    assert all(hit["workplace_address"]["region_code"] == region_code for hit in hits)


@pytest.mark.parametrize("region_code", ["01", "03", "18"])
def test_region_with_negeative_search(region_code, client):
    # Arrange
    query = {taxonomy.REGION: f"-{region_code}"}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    hits = response.json["hits"]
    assert len(hits) > 0

    assert all(not hit["workplace_address"]["region_code"] == region_code for hit in hits)


def test_freetext_query_synonym_param(client):
    # Arrange
    query = {"q": "montessori", "limit": "0"}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["freetext_concepts"]["skill"][0] == "montessoripedagogik"
    assert response.json["total"]["value"] == 3


@pytest.mark.parametrize(
    "query, expected_count",
    [
        [{"employer": "västra götalandsregionen"}, 56],
        [{"employer": "Jobtech"}, 0],
        [{"employer": "Region Stockholm"}, 372],
        [{"employer": "City Dental i Stockholm AB"}, 3681],
        [{"employer": "Premier Service Sverige AB"}, 3633],
        [{"employer": "Smartbear Sweden AB"}, 3613],
        pytest.param({"employer": "Malmö Universitet"}, 109, marks=pytest.mark.smoke),
        [{"employer": "Göteborgs Universitet"}, 89],
        [{"employer": "Blekinge Läns Landsting"}, 13],
        [{"employer": '"Skåne Läns Landsting"'}, 72],  # quoted string
    ],
)
def test_employer_search_returns_expected_number_of_results(query, expected_count, client):
    """
    This test return too many hits
    it will return hits where company name has one of the words in the employer name (e.g. 'Sverige')
    keeping it to document current behavior
    """
    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count
