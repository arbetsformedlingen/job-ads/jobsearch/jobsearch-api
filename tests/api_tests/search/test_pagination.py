import pytest


@pytest.mark.parametrize("limit", [0, 1, 10, 100])
def test_limit_up_to_100_should_return_ok(limit, client):
    # Arrange
    query = {"limit": limit}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert len(response.json["hits"]) == limit


def test_limit_over_100_should_fail(client):
    # Arrange
    query = {"limit": 101}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.status_code == 400
    assert response.json["message"] == "Input payload validation failed"
    assert response.json["errors"]["limit"] == "Invalid argument: 101. argument must be within the range 0 - 100"


def test_offset_parameter_returns_different_but_ordered_results(client):
    """
    Check that the offset parameter returns different entries from the same
    result set. There should be an overlap in hits matching the offset and
    limit parameters used.
    """

    # Arrange
    query_a = {"offset": 5, "limit": 15}
    query_b = {"offset": 15, "limit": 15}

    # Act
    response_a = client.get("/search", query_string=query_a)
    response_b = client.get("/search", query_string=query_b)

    # Assert
    assert len(response_a.json["hits"]) == 15
    assert len(response_b.json["hits"]) == 15

    # Check that there is an overlap of 5 ads
    hits_a = sorted([hit["id"] for hit in response_a.json["hits"]])
    hits_b = sorted([hit["id"] for hit in response_b.json["hits"]])
    assert hits_a != hits_b
    assert len(set(hits_a) & set(hits_b)) == 5


@pytest.mark.parametrize("offset", [0, 1, 100, 2000])
def test_offset_up_to_2000_should_return_ok(offset, client):
    # Arrange
    query = {"offset": offset, "limit": 1}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.status_code == 200
    assert len(response.json["hits"]) == 1


def test_offset_over_2000_should_fail(client):
    # Arrange
    query = {"offset": 2001, "limit": 1}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.status_code == 400
    assert response.json["message"] == "Input payload validation failed"
    assert response.json["errors"]["offset"] == "Invalid argument: 2001. argument must be within the range 0 - 2000"
