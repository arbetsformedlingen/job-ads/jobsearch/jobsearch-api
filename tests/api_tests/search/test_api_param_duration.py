import pytest

from tests.test_resources.test_settings import NUMBER_OF_ADS


@pytest.mark.smoke
@pytest.mark.parametrize(
    "duration, expected_count",
    [
        ["a7uU_j21_mkL", 3207],  # Tills vidare
        ["9RGe_UxD_FZw", 0],  # 12 månader - upp till 2 år
        ["gJRb_akA_95y", 0],  # 6 månader - upp till 12 månader
        ["Xj7x_7yZ_jEn", 361],  # 3 månader - upp till 6 månader
        ["Sy9J_aRd_ALx", 502],  # 11 dagar - upp till 3 månader
        ["cAQ8_TpB_Tdv", 9],  # Upp till 10 dagar
    ],
)
def test_employment_valid_duration(duration, expected_count, client):
    # Arrange
    query = {"duration": duration, "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.status_code == 200
    assert response.json["total"]["value"] == expected_count


@pytest.mark.parametrize(
    "duration, expected_count",
    [
        [1, 3207],  # Tills vidare
        [9, 0],  # 12 månader - upp till 2 år
        [0, 0],  # 6 månader - upp till 12 månader
        [3, 361],  # 3 månader - upp till 6 månader
        [7, 502],  # 11 dagar - upp till 3 månader
        [8, 9],  # Upp till 10 dagar
    ],
)
def test_employment_valid_legacy_duration(duration, expected_count, client):
    # Arrange
    query = {"duration": duration, "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.status_code == 200
    assert response.json["total"]["value"] == expected_count


def test_employment_no_duration(client):
    # Assert
    query = {"duration": None}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.status_code == 200
    assert response.json["total"]["value"] == NUMBER_OF_ADS


@pytest.mark.parametrize(
    "duration, expected_count",
    [
        ["not-a-concept-id", 0],  # Invalid parameter string.
        [True, 0],  # Invalid parameter, boolean.
        [False, 0],  # Invalid parameter, boolean.
    ],
)
def test_employment_on_invalid_duration(duration, expected_count, client):
    # Arrange
    query = {"duration": duration, "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.status_code == 200
    assert response.json["total"]["value"] == expected_count


