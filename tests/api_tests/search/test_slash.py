import pytest


@pytest.mark.parametrize(
    "q, expected_count",
    [
        ["försäljning/marknad", 3],
        ["försäljning marknad", 12],
        pytest.param("försäljning / marknad", 28, marks=pytest.mark.smoke),
        ["lager/logistik", 19],
        ["lager / logistik", 7],
        ["lager logistik", 138],
        pytest.param("psykolog/beteendevetare", 20, marks=pytest.mark.smoke),
        ["psykolog / beteendevetare", 0],
        ["psykolog beteendevetare", 30],
        ["Affärsutvecklare/exploateringsingenjör", 3],
        ["Affärsutvecklare / exploateringsingenjör", 1],
        ["Affärsutvecklare exploateringsingenjör", 11],
        ["Affärsutvecklare/exploateringsingenjörer", 2],
        ["Affärsutvecklare / exploateringsingenjörer", 1],
        ["Affärsutvecklare exploateringsingenjörer", 11],
        ["barnpsykiatri/habilitering", 1],
        ["barnpsykiatri / habilitering", 0],
        pytest.param("barnpsykiatri habilitering", 21, marks=pytest.mark.smoke),
        ["mentor/kontaktlärare", 0],
        ["mentor / kontaktlärare", 0],
        ["mentor kontaktlärare", 0],
        ["Verktygsmakare/Montör", 17],
        pytest.param("Verktygsmakare / Montör", 4, marks=pytest.mark.smoke),
        ["Verktygsmakare Montör", 38],
        ["Kolloledare/specialpedagog", 18],
        ["Kolloledare / specialpedagog", 0],
        ["Kolloledare specialpedagog", 24],
        ["fritidshem/fritidspedagog", 14],
        ["fritidshem / fritidspedagog", 3],
        pytest.param("fritidshem fritidspedagog", 16, marks=pytest.mark.smoke),
        ["UX/UI Designer", 1],
        ["UX / UI Designer", 1],
        ["UX UI Designer", 1],
    ],
)
def test_freetext_search_slash(q, expected_count, client):
    """
    Search with terms that are joined by a slash '/' included (x/y)
    with the terms separately (x y)
    and with a slash surrounded by space (x / y)
    """
    # Arrange
    query = {"q": q, "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count


@pytest.mark.parametrize(
    "q, expected_count",
    [
        [".NET/C#", 14],
        [".NET / C#", 7],
        [".NET C#", 67],
        [".NET /C#", 2],
        [".NET/ C#", 10],
        pytest.param(".NET", 40, marks=pytest.mark.smoke),
        ["C#/.net", 13],
        ["C# .net", 67],
        ["C# /.net", 10],
        ["C# / .net", 7],
        ["C#", 52],
        ["dotnet", 40],
    ],
)
def test_freetext_search_dot_hash_slash(q, expected_count, client):
    """
    Search with terms that are joined by a slash '/' included (x/y)
    with the terms separately (x y)
    and with a slash surrounded by space (x / y)
    for words that have . or # (e.g. '.net', 'c#')
    """
    # Arrange
    query = {"q": q, "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count


@pytest.mark.parametrize(
    "q, expected_count",
    [
        ["programmerare", 157],
        ["Systemutvecklare", 157],
        ["Systemutvecklare/Programmerare", 14],
        ["Systemutvecklare Programmerare", 157],
        pytest.param("Systemutvecklare / Programmerare", 20, marks=pytest.mark.smoke),
    ],
)
def test_freetext_search_slash_short(q, expected_count, client):
    # Arrange
    query = {"q": q, "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count
