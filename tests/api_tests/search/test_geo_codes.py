import pytest

from tests.test_resources.concept_ids.municipality import municipalities


@pytest.mark.slow
@pytest.mark.parametrize(
    "municipality_id, municipality_code, expected_ad_ids",
    municipalities,
)
def test_search_on_municipality(
    municipality_id, municipality_code, expected_ad_ids, client
):
    """
    Check that all municipalities are searchable and that the correct ads are returned.
    """

    # Arrange
    query = {"municipality": municipality_id}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.status_code == 200

    # Verify that correct ad ids are returned as hits.
    found_ad_ids = [hit["id"] for hit in response.json["hits"]]
    assert sorted(found_ad_ids) == sorted(expected_ad_ids)

    # Verify that all returned ads have correct municipality code.
    expected_municipality_code = municipality_code
    assert all(
        [
            hit["workplace_address"]["municipality_code"] == expected_municipality_code
            for hit in response.json["hits"]
        ]
    )


@pytest.mark.smoke
def test_municipality_smoke(client):
    """
    Quick check that one municipality is searchable.
    """

    # Arrange
    query = {"municipality": "K8A2_JBa_e6e", "limit": 1}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.status_code == 200

    # Verify correct number of results
    assert response.json["total"]["value"] == 5

    # Verify that returned ad have correct municipalit code.
    hit = response.json["hits"][0]
    assert hit["workplace_address"]["municipality_code"] == "0360"


@pytest.mark.parametrize(
    "region_id, expected_region_code, expected_count",
    [
        ["DQZd_uYs_oKb", "10", 60],
        ["oDpK_oZ2_WYt", "20", 122],
        ["zupA_8Nt_xcD", "21", 127],
        ["wjee_qH2_yb6", "13", 138],
        ["65Ms_7r1_RTG", "23", 54],
        ["MtbE_xWT_eMi", "06", 195],
        ["9QUH_2bb_6Np", "08", 140],
        ["tF3y_MF9_h5G", "07", 113],
        ["9hXe_F4g_eTG", "25", 137],
        ["xTCk_nT5_Zjm", "18", 129],
        ["oLT3_Q9p_3nn", "05", 249],
        ["CaRE_1nn_cSU", "12", 643],
        ["s93u_BEb_sx2", "04", 123],
        ["CifL_Rzy_Mku", "01", 1227],
        ["zBon_eET_fFU", "03", 176],
        ["K8iD_VQv_2BA", "09", 32],
        ["EVVp_h6U_GSZ", "17", 98],
        ["g5Tt_CAV_zBd", "24", 113],
        ["G6DV_fKE_Viz", "19", 88],
        ["NvUF_SP1_1zo", "22", 121],
        ["zdoY_6u5_Krt", "14", 779],
    ],
)
def test_search_on_region(region_id, expected_region_code, expected_count, client):
    """
    Check that all regions are searchable.
    """

    # Arrange
    query = {"region": region_id, "limit": 1}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.status_code == 200

    # Verify correct number of results
    assert response.json["total"]["value"] == expected_count

    # Verify that returned ad have correct region code.
    hit = response.json["hits"][0]
    assert hit["workplace_address"]["region_code"] == expected_region_code
