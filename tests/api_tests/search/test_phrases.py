import pytest

from common.constants import FRANCHISE, HIRE_WORK_PLACE, LARLING, OPEN_FOR_ALL, REMOTE, TRAINEE
from tests.test_resources.test_settings import (
    FRANCHISE_PHRASES,
    HIRE_WORKPLACE_PHRASES,
    LARLING_PHRASES,
    OPEN_FOR_ALL_PHRASE,
    REMOTE_MATCH_PHRASES,
    TEST_USE_STATIC_DATA,
    TRAINEE_PHRASES,
)


@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on specific data set")
@pytest.mark.parametrize(
    "query, expected_count",
    [
        [{"open_for_all": True, "limit": 0}, 152],
        [{"open_for_all": False, "limit": 0}, 4877],
        pytest.param({"remote": True, "limit": 0}, 36, marks=pytest.mark.smoke),
        [{"remote": False, "limit": 0}, 4993],
        [{"trainee": True, "limit": 0}, 4],
        [{"trainee": False, "limit": 0}, 5025],
        [{"larling": True, "limit": 0}, 4],
        [{"larling": False, "limit": 0}, 5025],
        [{"franchise": True, "limit": 0}, 7],
        [{"franchise": False, "limit": 0}, 5022],
    ],
)
def test_phrase_params_number_of_ads(query, expected_count, client):
    # Act
    response = client.get("/search", query_string=query)

    # Assert
    response.json["total"]["value"] == expected_count


@pytest.mark.parametrize(
    "query, expected_count",
    [
        [{"q": "utvecklare", "open_for_all": True, "limit": 0}, 2],
        [{"q": "mötesbokare", "open_for_all": True, "limit": 0}, 2],
        [{"q": "mötesbokare", "open_for_all": False, "limit": 0}, 7],
        [{"q": "tekniker", "open_for_all": False, "limit": 0}, 25],
        pytest.param(
            {"q": "säljare", "open_for_all": True, "limit": 0},
            8,
            marks=pytest.mark.smoke,
        ),
        [{"q": "säljare", "open_for_all": False, "limit": 0}, 217],
        [{"q": "utvecklare", "remote": True, "limit": 0}, 2],
        [{"q": "mötesbokare", "remote": True, "limit": 0}, 0],
        [{"q": "mötesbokare", "remote": False, "limit": 0}, 9],
        [{"q": "tekniker", "remote": False, "limit": 0}, 26],
        [{"q": "säljare", "remote": True, "limit": 0}, 1],
        [{"q": "säljare", "remote": False, "limit": 0}, 224],
        [{"q": "traineeprogram", "trainee": False, "limit": 0}, 4],
        [{"q": "frisör", "trainee": True, "limit": 0}, 1],
        [{"q": "frisör", "trainee": False, "limit": 0}, 12],
        [{"q": "tekniker", "trainee": True, "limit": 0}, 0],
        [{"q": "säljare", "trainee": True, "limit": 0}, 0],
        [{"q": "säljare", "trainee": False, "limit": 0}, 225],
        [{"q": "lärling", "larling": True, "limit": 0}, 2],
        [{"q": "frisör", "larling": True, "limit": 0}, 0],
        pytest.param(
            {"q": "frisör", "larling": False, "limit": 0},
            13,
            marks=pytest.mark.smoke,
        ),
        [{"q": "snickare", "larling": True, "limit": 0}, 0],
        [{"q": "snickare", "larling": False, "limit": 0}, 32],
        [{"q": "rörmokare", "larling": True, "limit": 0}, 0],
        [{"q": "lärling", "franchise": True, "limit": 0}, 0],
        [{"q": "lärling", "franchise": False, "limit": 0}, 4],
        [{"q": "frisör", "franchise": True, "limit": 0}, 0],
        [{"q": "frisör", "franchise": False, "limit": 0}, 13],
        [{"q": "pressbyrån", "franchise": True, "limit": 0}, 1],
        [{"q": "företagare", "franchise": True, "limit": 0}, 1],
        [{"q": "företagare", "franchise": False, "limit": 0}, 44],
        [{"q": "frisör", "hire-work-place": True, "limit": 0}, 2],
        [{"q": "frisör", "hire-work-place": False, "limit": 0}, 11],
        [{"q": "mötesbokare", "hire-work-place": True, "limit": 0}, 0],
        [{"q": "mötesbokare", "hire-work-place": False, "limit": 0}, 9],
        [{"q": "tekniker", "hire-work-place": True, "limit": 0}, 0],
        pytest.param(
            {"q": "tekniker", "hire-work-place": False, "limit": 0},
            26,
            marks=pytest.mark.smoke,
        ),
        [{"q": "frisörstol", "hire-work-place": True, "limit": 0}, 0],
        [{"q": "frisörstol", "hire-work-place": False, "limit": 0}, 0],
    ],
)
def test_phrase_query(query, expected_count, client):
    # Act
    response = client.get("/search", query_string=query)

    # Assert
    response.json["total"]["value"] == expected_count


def expected_phrase_found_in_ad(ad, phrases):
    """
    Check that at least one of phrases is found in ad description or headline
    """
    return any([phrase in ad["description"]["text"].lower() or phrase in ad["headline"].lower() for phrase in phrases])


@pytest.mark.parametrize(
    "phrase, expected_phrases",
    [
        (REMOTE, REMOTE_MATCH_PHRASES),
        (OPEN_FOR_ALL, OPEN_FOR_ALL_PHRASE),
        (TRAINEE, TRAINEE_PHRASES),
        (LARLING, LARLING_PHRASES),
        (FRANCHISE, FRANCHISE_PHRASES),
        (HIRE_WORK_PLACE, HIRE_WORKPLACE_PHRASES),
    ],
)
@pytest.mark.parametrize("q", ["hemifrån", "säljare", "java", "lärare", "tekniker", "öppen för alla", "trainee"])
def test_query_content(q, phrase, expected_phrases, client):
    """
    Check that ads have at least one phrase associated with "öppen för alla"
    in description or title when using 'open_for_all': True
    """
    # Arrange
    query = {"q": q, phrase: True, "limit": 100}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    hits = response.json["hits"]

    assert all(expected_phrase_found_in_ad(hit, expected_phrases) for hit in hits)


@pytest.mark.parametrize(
    "phrase_params, phrases",
    [
        (REMOTE, REMOTE_MATCH_PHRASES),
        (OPEN_FOR_ALL, OPEN_FOR_ALL_PHRASE),
        (TRAINEE, TRAINEE_PHRASES),
        (LARLING, LARLING_PHRASES),
        (FRANCHISE, FRANCHISE_PHRASES),
        (HIRE_WORK_PLACE, HIRE_WORKPLACE_PHRASES),
    ],
)
def test_phrases_binary_true(phrase_params, phrases, client):
    # Arrange
    query = {phrase_params: True}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    hits = response.json["hits"]

    assert all(expected_phrase_found_in_ad(hit, phrases) for hit in hits)


@pytest.mark.parametrize(
    "phrase_params, phrases",
    [
        (REMOTE, REMOTE_MATCH_PHRASES),
        (OPEN_FOR_ALL, OPEN_FOR_ALL_PHRASE),
        (TRAINEE, TRAINEE_PHRASES),
        (LARLING, LARLING_PHRASES),
        (FRANCHISE, FRANCHISE_PHRASES),
        (HIRE_WORK_PLACE, HIRE_WORKPLACE_PHRASES),
    ],
)
def test_phrases_binary_false(phrase_params, phrases, client):
    # Arrange
    query = {phrase_params: False}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    hits = response.json["hits"]

    assert all(not expected_phrase_found_in_ad(hit, phrases) for hit in hits)


@pytest.mark.parametrize("q", ["säljare", "java", "sjuksköterska", "frisör"])
@pytest.mark.parametrize(
    "phrase, expected_phrases",
    [
        (REMOTE, REMOTE_MATCH_PHRASES),
        (TRAINEE, TRAINEE_PHRASES),
        (LARLING, LARLING_PHRASES),
        (FRANCHISE, FRANCHISE_PHRASES),
        (HIRE_WORK_PLACE, HIRE_WORKPLACE_PHRASES),
    ],
)
def test_phrases_binary_query_true(q, phrase, expected_phrases, client):
    # Arrange
    query = {"q": q, phrase: True}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    hits = response.json["hits"]

    assert all(expected_phrase_found_in_ad(hit, expected_phrases) for hit in hits)


@pytest.mark.parametrize("q", ["säljare", "java", "sjuksköterska", "frisör"])
@pytest.mark.parametrize(
    "phrase, expected_phrases",
    [
        (REMOTE, REMOTE_MATCH_PHRASES),
        (TRAINEE, TRAINEE_PHRASES),
        (LARLING, LARLING_PHRASES),
        (FRANCHISE, FRANCHISE_PHRASES),
        (HIRE_WORK_PLACE, HIRE_WORKPLACE_PHRASES),
    ],
)
def test_phrases_binary_query_false(q, phrase, expected_phrases, client):
    # Arrange
    query = {"q": q, phrase: False}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    hits = response.json["hits"]

    assert all(not expected_phrase_found_in_ad(hit, expected_phrases) for hit in hits)


@pytest.mark.parametrize("q", ["säljare", "java", "sjuksköterska", "frisör"])
def test_phrases_binary_query_open_for_all_true(q, client):
    # Arrange
    query = {"q": q, OPEN_FOR_ALL: True}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    hits = response.json["hits"]

    assert all(expected_phrase_found_in_ad(hit, OPEN_FOR_ALL_PHRASE) for hit in hits)


@pytest.mark.parametrize("q", ["säljare", "java", "sjuksköterska", "frisör"])
def test_phrases_binary_query_open_for_all_false(q, client):
    # Arrange
    query = {"q": q, OPEN_FOR_ALL: False}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    hits = response.json["hits"]

    assert all(not expected_phrase_found_in_ad(hit, OPEN_FOR_ALL_PHRASE) for hit in hits)


@pytest.mark.parametrize("phrase", [REMOTE, OPEN_FOR_ALL, TRAINEE, LARLING, FRANCHISE, HIRE_WORK_PLACE])
def test_sum_phrases(phrase, client):
    # Arrange
    query_all = {"limit": 0}
    query_phrase_true = {phrase: True, "limit": 0}
    query_phrase_false = {phrase: False, "limit": 0}

    # Act
    response_none = client.get("/search", query_string=query_all)
    response_phrase_true = client.get("/search", query_string=query_phrase_true)
    response_phrase_false = client.get("/search", query_string=query_phrase_false)

    # Assert
    count_all = response_none.json["total"]["value"]
    count_phrase_true = response_phrase_true.json["total"]["value"]
    count_phrase_false = response_phrase_false.json["total"]["value"]

    assert count_all > 0
    assert count_phrase_true + count_phrase_false == count_all
