import pytest


@pytest.mark.parametrize(
    "param",
    [
        "published-after",
        "published-before",
    ],
)
@pytest.mark.parametrize(
    "date_string",
    [
        "2023-07",
        "2021-01-05",
        "2021-01-01T07:34:10",
        "2023-06-24T12:00:00Z",
        "2023-06-24T12:00:00+02:00",
    ],
)
def test_date_string_without_errors_return_status_200(param, date_string, client):
    """
    Test that a Bad Request exception is not raised when the system
    can parse the date.
    """
    # Arrange
    query = {param: date_string}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.status_code == 200


@pytest.mark.parametrize(
    "param",
    [
        "published-after",
        "published-before",
    ],
)
def test_date_string_with_invalid_string_returns_400(param, client):
    """
    Test that a Bad Request exception is raised when the system can not
    parse the date because of invalid formatting.
    """
    # Arrange
    query = {param: "not-a-date"}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    # Status code for "bad request"
    assert response.status_code == 400

    # Response json should contain error message
    assert response.json["errors"]
    assert response.json["message"] == "Input payload validation failed"


@pytest.mark.parametrize(
    "minutes",
    [
        "23",
        "67",
    ],
)
def test_date_string_as_minutes_return_status_200(minutes, client):
    """
    Test that a Bad Request exception is not raised when the system
    can parse the date.
    """
    # Arrange
    query = {"published-after": minutes}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.status_code == 200


@pytest.mark.parametrize(
    "query, expected_count",
    [
        [{"published-before": "2015-05-25T00:00:01"}, 0],
        [{"published-before": "2016-06-25T00:00:01"}, 0],
        [{"published-before": "2017-07-25T00:00:01"}, 0],
        [{"published-before": "2018-08-25T00:00:01"}, 0],
        [{"published-before": "2019-09-25T00:00:01"}, 0],
        pytest.param({"published-before": "2020-10-25T00:00:01"}, 18, marks=pytest.mark.smoke),
        [{"published-before": "2021-04-25T00:00:01"}, 5029],
        [{"published-before": "2023-02-20T15:08:27"}, 5029],
        [{"published-before": "1971-01-01T00:00:01"}, 0],
        [{"published-before": "2021-01-01T00:00:01"}, 158],
        [{"published-before": "2021-01-25T07:29:41"}, 304],
        [{"published-after": "2020-11-01T00:00:01"}, 5007],
        [{"published-after": "2020-12-01T00:00:01"}, 4964],
        [{"published-after": "2021-03-10T00:00:01"}, 2810],
        [{"published-after": "2021-03-22T00:00:01"}, 892],
        [{"published-after": "1971-01-01T00:00:01"}, 5029],
        [{"published-after": "2023-02-20T15:08:27"}, 0],
        [{"published-after": "2020-12-15T00:00:01", "published-before": "2020-12-20T00:00:01"}, 20],
        pytest.param(
            {"published-after": "2020-12-01T00:00:01", "published-before": "2020-12-10T00:00:01"},
            26,
            marks=pytest.mark.smoke,
        ),
        [{"published-after": "2020-12-11T00:00:01", "published-before": "2020-12-15T00:00:01"}, 8],
        [{"published-after": "2023-02-20T15:08:27", "published-before": "1971-01-01T00:00:01"}, 0],
        [{"published-after": "2016-02-20T15:08:27", "published-before": "2017-01-01T00:00:01"}, 0],
        [{"published-after": "2018-02-20T15:08:27", "published-before": "2020-08-01T00:00:01"}, 0],
        [{"published-after": "2017-02-20T15:08:27", "published-before": "2017-05-01T00:00:01"}, 0],
        [{"published-before": "2023-05-16T12:02:11"}, 5029],
    ],
)
def test_publication_date_return_expected_number_of_results(query, expected_count, client):
    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count
