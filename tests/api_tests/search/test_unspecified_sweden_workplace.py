import pytest  # noqa: F401


@pytest.mark.smoke
def test_unspecified_sweden_workplace_returns_hits(client):
    # Arrange
    query = {"unspecified-sweden-workplace": "true", "offset": 0, "limit": 0}

    # Assert
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] > 0


def test_unspecified_sweden_workplace_hits_should_not_contain_certain_data(client):
    # Arrange
    query = {"unspecified-sweden-workplace": "true", "offset": 0, "limit": 100}

    # Assert
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] > 0

    for hit in response.json["hits"]:
        assert hit["workplace_address"]["region"] is None
        assert hit["workplace_address"]["municipality"] is None
        assert hit["workplace_address"]["municipality_code"] is None
        assert hit["workplace_address"]["municipality_concept_id"] is None
        assert hit["workplace_address"]["region"] is None
        assert hit["workplace_address"]["region_code"] is None
        assert hit["workplace_address"]["region_concept_id"] is None
        assert hit["workplace_address"]["street_address"] is None
        assert hit["workplace_address"]["postcode"] is None
        assert hit["workplace_address"]["city"] is None
        assert hit["workplace_address"]["coordinates"] == [None, None]
        assert hit["relevance"] == 0.0
