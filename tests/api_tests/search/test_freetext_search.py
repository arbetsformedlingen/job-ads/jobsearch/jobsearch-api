import pytest

from tests.test_resources.concept_ids import concept_ids_geo as geo


@pytest.mark.parametrize(
    "q, municipality, code, municipality_concept_id, expected_number_of_hits",
    [
        ("bagare stockholm", "Stockholm", "0180", geo.stockholm, 0),
        ("lärare stockholm", "Stockholm", "0180", geo.stockholm, 14),
        ("lärare göteborg", "Göteborg", "1480", geo.goteborg, 4),
    ],
)
def test_freetext_work_and_location_details(
    q, municipality, code, municipality_concept_id, expected_number_of_hits, client
):
    # Arrange
    query = {"q": q, "limit": "20"}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_number_of_hits
    assert len(response.json["hits"]) == expected_number_of_hits

    # Check all hits have the correct municipality, code and concept id
    assert all([hit["workplace_address"]["municipality"] == municipality for hit in response.json["hits"]])
    assert all([hit["workplace_address"]["municipality_code"] == code for hit in response.json["hits"]])
    assert all(
        [
            hit["workplace_address"]["municipality_concept_id"] == municipality_concept_id
            for hit in response.json["hits"]
        ]
    )


def test_freetext_search_has_expected_hits(client):
    """
    Verify that freetext search returns expected ads.
    """

    # Arrange
    expected_ad_ids = [
        "24645625",
        "24648363",
        "24648149",
        "24645837",
        "24643198",
        "24642963",
        "24636423",
        "24631812",
        "24627762",
        "24624501",
    ]

    # Act
    response = client.get("/search", query_string={"q": "sjuksköterska läkare Stockholm Göteborg", "limit": "10"})
    hits = response.json["hits"]

    # Assert
    assert len(hits) == 10
    assert [hit["id"] for hit in hits] == expected_ad_ids


def test_freetext_search_results_sorted_by_relevance(client):
    """
    Verify that the search results are sorted by relevance.
    """

    # Arrange
    query = {"q": "lärare språk högstadiet", "limit": "20"}

    # Act
    response = client.get("/search", query_string=query)
    hits = response.json["hits"]

    # Assert
    assert all(hits[i]["relevance"] >= hits[i + 1]["relevance"] for i in range(len(hits) - 1))


@pytest.mark.parametrize(
    "q, expected_hit_count, first_hit_ad_id",
    [
        ["bagare kock Stockholm Göteborg", 4, "24599773"],
        ["kock bagare Stockholm Göteborg", 4, "24599773"],
        ["kallskänka kock Stockholm Göteborg", 4, "24599773"],
    ],
)
def test_freetext_two_work_and_two_locations(q, expected_hit_count, first_hit_ad_id, client):
    """
    Test that the top hit for a search has not changed and that the number of hits for query has not changed
    This documents current behavior
    """
    # Arrange
    query = {"q": q, "limit": "1"}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_hit_count

    assert response.json["hits"][0]["id"] == first_hit_ad_id


@pytest.mark.parametrize(
    "q, expected_hit_count, first_hit_ad_id",
    [
        ["Sirius crew", 4, "24625524"],
        ["Säsong", 20, "24570725"],
    ],
)
def test_freetext_search_without_known_concepts(q, expected_hit_count, first_hit_ad_id, client):
    """
    Tests from examples
    Test that specific queries should return only one hit (identified by id)
    and that freetext concepts are not included in search result
    """
    # Arrange
    query = {"q": q, "limit": "1"}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_hit_count

    assert response.json["hits"][0]["id"] == first_hit_ad_id

    # freetext_concepts should be empty
    assert response.json["freetext_concepts"]["skill"] == []
    assert response.json["freetext_concepts"]["occupation"] == []
    assert response.json["freetext_concepts"]["location"] == []
    assert response.json["freetext_concepts"]["skill_must"] == []
    assert response.json["freetext_concepts"]["occupation_must"] == []
    assert response.json["freetext_concepts"]["location_must"] == []
    assert response.json["freetext_concepts"]["skill_must_not"] == []
    assert response.json["freetext_concepts"]["occupation_must_not"] == []
    assert response.json["freetext_concepts"]["location_must_not"] == []


def test_freetext_search_with_known_concepts_and_quoted_string(client):
    # Arrange
    query = {"q": 'systemutvecklare python java stockholm "klarna bank ab"', "limit": "1"}

    # Act
    response = client.get("/search", query_string=query)
    hit = response.json["hits"][0]

    # Assert
    assert "klarna bank ab" in hit["employer"]["name"].lower()

    assert response.json["freetext_concepts"]["skill"] == ["python", "java"]
    assert response.json["freetext_concepts"]["occupation"] == ["systemutvecklare"]
    assert response.json["freetext_concepts"]["location"] == ["stockholm"]
    assert response.json["freetext_concepts"]["skill_must"] == []
    assert response.json["freetext_concepts"]["occupation_must"] == []
    assert response.json["freetext_concepts"]["location_must"] == []
    assert response.json["freetext_concepts"]["skill_must_not"] == []
    assert response.json["freetext_concepts"]["occupation_must_not"] == []
    assert response.json["freetext_concepts"]["location_must_not"] == []

    assert "Systemutvecklare/Programmerare" in hit["occupation"]["label"]
    assert hit["workplace_address"]["municipality"] == "Stockholm"


def test_freetext_search_with_known_concepts_and_unquoted_string(client):
    # Arrange
    query = {"q": "systemutvecklare python java stockholm klarna bank ab", "limit": "1"}

    # Act
    response = client.get("/search", query_string=query)
    hit = response.json["hits"][0]

    # Assert
    assert "klarna bank ab" in hit["employer"]["name"].lower()

    assert response.json["freetext_concepts"]["skill"] == ["python", "java", "bank"]
    assert response.json["freetext_concepts"]["occupation"] == ["systemutvecklare"]
    assert response.json["freetext_concepts"]["location"] == ["stockholm"]
    assert response.json["freetext_concepts"]["skill_must"] == []
    assert response.json["freetext_concepts"]["occupation_must"] == []
    assert response.json["freetext_concepts"]["location_must"] == []
    assert response.json["freetext_concepts"]["skill_must_not"] == []
    assert response.json["freetext_concepts"]["occupation_must_not"] == []
    assert response.json["freetext_concepts"]["location_must_not"] == []

    assert "Systemutvecklare/Programmerare" in hit["occupation"]["label"]
    assert hit["workplace_address"]["municipality"] == "Stockholm"
