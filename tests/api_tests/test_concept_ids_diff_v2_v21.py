import pytest


@pytest.mark.parametrize(
    "occupation_name, expected_count",
    [
        # these concept ids for occupation-name have changed between version 2 and version 21 of taxonomoy
        # and they are found in the test ads so that at least one ad is found for each test case
        ["p17k_znk_osi", 73],
        ["rJih_KYS_qUK", 2],
        ["4Z7F_oBG_vMG", 22],
        ["Jd8e_1UD_8Vo", 6],
        pytest.param("zGHj_2Jf_oKy", 31, marks=pytest.mark.smoke),
        ["edjC_rnL_gS3", 1],
        ["yUZ5_WDk_24i", 10],
        ["Hob8_zfc_5ge", 15],
        ["bLKd_4Wg_3rn", 20],
        ["QaQC_ozP_Bme", 22],
        ["ZtTD_4VC_f4i", 11],
        pytest.param("Y3QA_5pk_uXd", 88, marks=pytest.mark.smoke),
        ["PQkQ_Dmk_ZF8", 24],
        ["CmFu_Dkt_fkX", 2],
        ["BK8D_hZe_dtk", 39],
        ["geNh_Yah_F3J", 8],
        ["fg7B_yov_smw", 94],
        ["8jrL_5RZ_9Kt", 42],
        ["dYo1_D8c_87U", 42],
        ["n2Tp_B8E_gED", 22],
        ["8Uhp_XYo_z5f", 65],
        ["3abF_Jyu_WtS", 2],
        ["uyJg_i94_xmr", 12],
        ["raVz_oTw_N3e", 30],
        ["J5oq_tNd_6RU", 12],
        ["gPCx_T6e_6P8", 22],
        ["HWPy_9jT_TX2", 10],
        pytest.param("heGV_uHh_o8W", 28, marks=pytest.mark.smoke),
        ["pYiF_Z6V_ULQ", 3],
        ["PXPJ_LKH_pmy", 7],
        ["uaJs_9YA_Cnp", 22],
        ["c32f_KpH_CfN", 35],
        ["Pj8T_bST_f7D", 7],
        ["DudY_Vj2_wa5", 2],
        ["bB8c_hxh_wPj", 1],
        ["y22E_Nz6_5VU", 4],
        ["Kh3N_Kg8_KK5", 3],
        ["7YaM_bPu_c1T", 6],
        ["WX68_sGe_oUm", 4],
        ["BHdM_TVA_baC", 2],
        ["M6Yt_ynM_FDQ", 2],
        ["Sbzf_iBA_MAS", 11],
        ["TTDZ_Neo_9GJ", 9],
        ["CZkP_hCz_KM8", 14],
        ["rGGf_KLs_To7", 27],
        ["pAdE_uVY_JrR", 8],
        ["HdcS_g6a_jPk", 4],
        ["EwkH_3Xb_r8j", 4],
        ["rUcW_z9R_Qsv", 41],
        ["ZS86_sdx_bx5", 3],
        ["C6Cv_9ZR_7Gb", 15],
        ["qSXj_aXc_EGp", 3],
        ["reEg_XH4_15c", 5],
        ["WRaF_xkF_AoB", 3],
        ["SgGz_7JC_dtw", 8],
        ["xMbt_y3G_BrD", 2],
        ["zHXG_Wfc_TY7", 1],
        ["u5R6_Lhs_uf4", 6],
        ["s5pR_WNm_R8W", 35],
        ["FcaP_vhz_Cuy", 2],
        ["KQty_E1u_cia", 35],
        ["YKhj_n5P_g4P", 47],
        ["Pi5N_5NB_7BY", 5],
        ["eeih_cfx_RhP", 2],
        ["cyCt_LLE_qDy", 3],
        pytest.param("pBhS_6fg_727", 49, marks=pytest.mark.smoke),
        ["mC28_vEv_AVx", 5],
        ["Jx4V_6tm_fUH", 4],
        ["kHno_7rL_Lcm", 22],
        ["zVkQ_wLL_78i", 11],
        ["aWNX_iEg_NhN", 2],
        ["YtrL_oQj_sck", 1],
        ["T9MX_dmQ_t5n", 5],
        ["4QjK_RpG_h4w", 2],
        ["MMTE_fiP_1Ng", 20],
        ["w8eg_Ufq_B5X", 10],
        ["TTF3_T1t_58U", 2],
        ["BT8G_RDJ_ofc", 5],
        ["bbSi_hWq_H98", 8],
        ["hQtr_vwy_MGb", 10],
        ["1ciU_tMj_Toc", 7],
        ["dh7h_HbH_Cus", 2],
        ["wHUj_vsP_Jhn", 30],
        ["Nzmh_iTb_z7h", 77],
        ["MGBV_LVk_AtK", 5],
        pytest.param("iYgm_hfD_Bez", 89, marks=pytest.mark.smoke),
        ["uwYE_D2Z_45B", 12],
        ["YG1s_tUg_jWJ", 23],
        ["XugJ_6hw_wVV", 2],
        ["HciA_Cu7_FXt", 3],
        ["aR3a_5Ly_TDw", 1],
        ["tdCZ_6Pn_VzT", 21],
        ["Ma36_ont_7tz", 6],
        ["XGDo_aFb_64o", 3],
        ["yjaM_pAe_u9m", 14],
        ["EvmU_s9w_euP", 9],
        pytest.param("KRGZ_uFp_LuK", 15, marks=pytest.mark.smoke),
        ["bXfh_r3N_Wue", 4],
        ["zrQ9_1z2_kT3", 4],
        ["wV1P_Ms5_HB2", 3],
        ["4PUw_jdZ_1wC", 5],
        ["eDCK_B3K_cqW", 1],
        ["H9ej_iRx_9G6", 2],
        ["Wcmx_rPX_ooC", 1],
        ["JjXV_QEg_EQ1", 1],
        ["Cqo8_tUs_JHu", 2],
        ["sDjW_zkk_bXW", 2],
        ["qtPc_BJB_SYL", 2],
        ["EEN6_NCm_XgJ", 1],
        ["An93_8wX_7R6", 1],
        ["tHZB_LJU_8LG", 4],
        ["X7oZ_fbn_TgG", 5],
        ["urWs_F5u_msS", 6],
        ["cEY1_Bh8_URP", 5],
        ["tDS5_hG2_ML5", 4],
        ["s28Q_sya_S2b", 8],
        ["fo9m_5cC_y8G", 6],
        ["6TQs_8i6_per", 5],
        ["74pa_EJB_huE", 5],
        ["dCVP_nzp_jDd", 2],
        ["TWef_KC4_wak", 1],
        ["3jF9_6FT_qTr", 3],
        ["m49c_MEL_Ft2", 1],
        ["rp67_sqK_cLB", 7],
        ["j6P1_gJq_a2Q", 2],
        ["ajWc_95L_PnQ", 4],
        ["FC86_VDF_rqB", 8],
        ["MiYT_1JK_fKe", 1],
        ["kf7K_UAZ_ed8", 1],
        ["Hw94_pWL_RZ4", 1],
        ["MbHj_ZVr_WsC", 6],
        ["1d3z_y59_2Hm", 9],
        pytest.param("Fcpk_WQT_4hS", 12, marks=pytest.mark.smoke),
        ["Fr7W_Yjv_3ik", 1],
        ["d2gz_es2_pX5", 10],
        ["f8q7_Rwv_TP4", 37],
        ["QREG_zsK_FLo", 1],
        ["juPt_n2E_aBv", 1],
        ["iyQT_LJi_c2Z", 2],
        ["8tSq_t1z_MRT", 6],
        ["9NVZ_LfD_vqT", 8],
        ["Ce5J_e8j_7oh", 3],
        ["ws82_14c_tmG", 2],
        ["qLCw_Azv_m1h", 1],
        ["7K8b_4EE_w5e", 7],
        ["wEd2_PmP_Pbi", 1],
        ["vPyM_A9W_WqV", 1],
        ["XzvX_AHE_ECV", 2],
        ["MQHi_q9J_x2d", 7],
        ["hCMr_L4F_8hq", 1],
        ["1oPB_E5L_n8e", 2],
        ["MJvj_tVV_zpZ", 1],
        ["MgLp_anW_jr5", 1],
        ["FvY5_W17_kDE", 4],
        ["hFXv_BPn_oow", 1],
        pytest.param("yAhe_6tb_jn8", 21, marks=pytest.mark.smoke),
        ["tsdt_MEd_bUS", 2],
        ["GrZU_ofe_QAx", 11],
        ["YNzT_LZS_SZe", 3],
        ["bhk6_aNU_xRe", 8],
        ["EBur_MWp_Xkz", 1],
        ["GZXT_V3B_3yJ", 1],
        ["etkf_dBn_JbW", 5],
        ["yCma_MSk_AMH", 3],
        ["XHyR_e9A_sqQ", 2],
        ["EPe8_EQu_YCL", 5],
        ["iqE3_kXw_wPM", 3],
        ["ELkQ_6o5_XSi", 2],
        ["HPwW_eF7_CgY", 3],
        ["7rct_y2Q_NSe", 3],
        ["Dgx7_yw6_CaN", 35],
        ["Jt42_Mjo_zC5", 1],
        ["wUKi_LRW_Wxk", 3],
        ["461J_HHW_wzZ", 4],
        ["7Kxn_MMG_TuY", 1],
        ["V1MG_d8m_H8f", 3],
        ["LJZw_d7H_QWZ", 1],
        ["7sZa_8wP_hqW", 5],
        pytest.param("pHNj_F3k_Dmd", 12, marks=pytest.mark.smoke),
        ["QjKb_RaU_KEd", 3],
        ["1hZc_5BC_K7q", 1],
        ["NikA_KEh_Ky4", 4],
        ["JQ6T_58U_sTP", 2],
        ["r6AA_UrV_qw3", 1],
        ["hu7j_cLR_HBo", 1],
        ["QBXT_ESH_eoP", 1],
        ["NqN3_Mur_sGB", 1],
        ["rTeH_EoT_FuA", 2],
        ["iwfM_TL3_Duw", 1],
        ["a8AC_eQX_cgd", 4],
        ["pJKs_jnz_qCp", 2],
        ["VpYo_k5f_N9R", 2],
        ["5Nkq_7DE_hoo", 5],
        ["aRp4_qjZ_tPV", 2],
        ["p96L_aTJ_kkG", 2],
        ["UEee_Vz8_EN6", 2],
        ["LX5L_e6N_4jS", 1],
        ["vE2q_wQB_NGv", 1],
        ["eH4L_6k6_Eok", 1],
        ["PeNQ_qpR_wTS", 1],
        ["yeAd_C6h_DqV", 1],
        ["soGu_JNp_Pni", 1],
        ["sFsH_YqP_pZ4", 1],
        ["Xoz2_dQe_9KC", 1],
        ["TFdz_Dvb_vwc", 1],
        ["jsCP_rsS_i9W", 2],
        ["NzUp_Gcr_4Hw", 2],
        ["UXrZ_zPo_KPu", 5],
        ["ofiS_5F2_YmV", 3],
        ["a6Hu_zpi_ALp", 2],
        ["ZyDH_2Xe_bfK", 1],
        ["ZCjB_SX4_Mjj", 2],
        ["nRLP_eqC_ow9", 3],
        ["2YDV_mGq_LpN", 4],
        ["3NQt_h54_Qy7", 1],
        ["2ouu_Ebh_M4B", 1],
        ["a1E3_mkT_gPe", 2],
        ["Wz1E_3L9_Uqt", 1],
        ["JZur_dmf_EPY", 1],
        ["fRKT_Fdr_fHL", 1],
        ["WhsT_BWb_dPy", 3],
        ["cJ62_FKk_ua6", 2],
        ["CJc9_tSF_DN2", 1],
        ["YHB5_wmX_UCt", 5],
        ["XSoL_LLU_PYV", 2],
        ["ytrP_JCv_bwH", 1],
        ["CsB1_HC2_EbL", 2],
        ["ujWA_LfK_Kxk", 2],
        ["Q9B2_yN1_XtF", 1],
        ["q1k8_CXz_7Nd", 1],
        ["1mBD_3PT_t2h", 2],
        ["G4NL_bcU_Xpr", 39],
        ["1Wdh_jwR_Foy", 1],
        ["X6Et_pui_vj1", 1],
        ["wr3k_f6E_vh2", 1],
        ["bXhQ_wQa_GE7", 1],
        ["ueNj_nxL_pMh", 1],
        ["dKVR_7ew_sCA", 7],
        ["fi74_DJo_f75", 2],
        ["Yx7u_AbE_Jqk", 3],
        ["tzHp_Aw9_qQQ", 1],
        ["Y86G_M6o_9R8", 5],
        ["WFfW_RYg_t2A", 1],
        ["V9Es_YZL_3kL", 1],
        ["pdKf_yyX_vWc", 2],
        ["JQ3q_X9q_aTC", 1],
        ["APXv_QHZ_dkt", 1],
        ["wtxi_ss9_8Cj", 1],
        ["vLvB_4hV_koL", 1],
        ["6m7f_UQX_P7j", 1],
        ["nvUH_JLD_s4F", 2],
        ["gqQC_PXg_gkU", 2],
        ["cYP6_Tur_q6m", 3],
        ["UYaR_8e9_wGy", 1],
        ["YmFo_cvw_1Kv", 2],
        ["TppD_R5Y_kiG", 1],
        ["hJuL_n8p_UEM", 1],
        ["wonQ_qBw_Wef", 1],
        ["nPMz_8iK_NmL", 1],
        ["HFuj_YCt_Ymn", 4],
        ["2RC6_tLW_jCJ", 1],
        ["vUZt_BHY_mk2", 1],
        ["TGHy_oZw_vad", 1],
        ["Edjj_2Q7_P8X", 1],
        ["9iGw_Yfv_VZS", 1],
        ["ghs4_JXU_BYt", 1],
        ["geo3_qtw_3eP", 6],
        ["Uoeq_F5q_18w", 1],
        ["QUmN_cfC_zPn", 4],
        ["Khp2_9py_dv2", 7],
        ["BduY_UUY_pvK", 1],
        ["VCsj_Uve_6EJ", 1],
        ["Ym1L_d8s_cad", 1],
        ["7yzW_dCg_7s7", 1],
        ["RhoN_Vdo_F5H", 1],
        ["sbKP_NHi_eiv", 2],
        ["pfiK_oXH_eYR", 1],
        ["gg8C_YzS_Dxq", 1],
        ["g2XL_xyr_wS7", 1],
    ],
)
def test_occupation_name_diff_v2_v21(occupation_name, expected_count, client):
    # Arrange
    query = {"occupation-name": occupation_name, "limit": 0}

    # Act
    response = client.get("/search", query_string=query)

    # Assert
    assert response.json["total"]["value"] == expected_count
