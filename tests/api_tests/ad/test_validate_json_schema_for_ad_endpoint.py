import pytest
from jsonschema import validate

AD_SCHEMA = {
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "type": "object",
    "properties": {
        "id": {"type": "string"},
        "external_id": {"type": ["string", "null"]},
        "original_id": {"type": ["string", "null"]},
        "label": {
            "type": "array",
            "items": {"type": "string"},
            "minItems": 0,
        },
        "webpage_url": {"type": "string"},  # url
        "logo_url": {"type": ["string", "null"]},  # url
        "headline": {"type": "string"},
        "application_deadline": {"type": "string"},  # datetime
        "number_of_vacancies": {"type": "integer", "minimum": 0},
        "description": {
            "type": "object",
            "properties": {
                "text": {"type": "string"},
                "text_formatted": {"type": "string"},
                "company_information": {"type": ["string", "null"]},
                "needs": {"type": ["string", "null"]},
                "requirements": {"type": ["string", "null"]},
                "conditions": {"type": ["string", "null"]},
            },
            "additionalProperties": False,
            "required": [
                "text",
                "text_formatted",
                "company_information",
                "needs",
                "requirements",
                "conditions",
            ],
        },
        "employment_type": {"$ref": "#/$defs/concept_id"},
        "salary_type": {"$ref": "#/$defs/concept_id"},
        "salary_description": {"type": ["string", "null"]},
        "duration": {"$ref": "#/$defs/concept_id"},
        "working_hours_type": {"$ref": "#/$defs/concept_id"},
        "scope_of_work": {
            "type": "object",
            "properties": {
                "min": {"type": "integer", "minimum": 0, "maximum": 100},
                "max": {"type": "integer", "minimum": 0, "maximum": 100},
            },
            "additionalProperties": False,
            "required": ["min", "max"],
        },
        "access": {"type": ["string", "null"]},
        "employer": {
            "type": "object",
            "properties": {
                "phone_number": {"type": ["string", "null"]},
                "email": {"type": ["string", "null"]},
                "url": {"type": ["string", "null"]},  # url
                "organization_number": {"type": "string"},
                "name": {"type": "string"},
                "workplace": {"type": "string"},
            },
            "additionalProperties": False,
            "required": [
                "phone_number",
                "email",
                "url",
                "organization_number",
                "name",
                "workplace",
            ],
        },
        "application_details": {
            "type": "object",
            "properties": {
                "information": {"type": ["string", "null"]},
                "reference": {"type": ["string", "null"]},
                "email": {"type": ["string", "null"]},  # email
                "via_af": {"type": "boolean"},
                "url": {"type": ["string", "null"]},  # url
                "other": {"type": ["string", "null"]},
            },
            "additionalProperties": False,
            "required": ["information", "reference", "email", "via_af", "url", "other"],
        },
        "experience_required": {"type": "boolean"},
        "access_to_own_car": {"type": "boolean"},
        "driving_license_required": {"type": "boolean"},
        # Not required?
        "driving_license": {
            "oneOf": [
                {"type": "null"},
                {
                    "type": "array",
                    "items": {"$ref": "#/$defs/concept_id"},
                    "minItems": 0,
                    "uniqueItems": True,
                },
            ]
        },
        "occupation": {"$ref": "#/$defs/concept_id"},
        "occupation_group": {"$ref": "#/$defs/concept_id"},
        "occupation_field": {"$ref": "#/$defs/concept_id"},
        "workplace_address": {
            "type": "object",
            "properties": {
                "municipality": {"type": ["string", "null"]},
                "municipality_code": {"type": ["string", "null"]},
                "municipality_concept_id": {"type": ["string", "null"]},
                "region": {"type": ["string", "null"]},
                "region_code": {"type": ["string", "null"]},
                "region_concept_id": {"type": ["string", "null"]},
                "country": {"type": "string"},
                "country_code": {"type": "string"},
                "country_concept_id": {"type": "string"},
                "street_address": {"type": ["string", "null"]},
                "postcode": {"type": ["string", "null"]},
                "city": {"type": ["string", "null"]},
                "coordinates": {
                    "oneOf": [
                        # {"type": "null"},  # This is where the null should be.
                        {
                            "type": "array",
                            "items": {
                                "oneOf": [
                                    {"type": "null"},  # This is ugly
                                    {
                                        "type": "number",
                                        "minimum": -180,
                                        "maximum": 180,
                                    },
                                ]
                            },
                            "minItems": 2,
                            "maxItems": 2,
                        },
                    ]
                },
            },
            "additionalProperties": False,
            "required": [
                "municipality",
                "municipality_code",
                "municipality_concept_id",
                "region",
                "region_code",
                "region_concept_id",
                "country",
                "country_code",
                "country_concept_id",
                "street_address",
                "postcode",
                "city",
                "coordinates",
            ],
        },
        "must_have": {
            "type": "object",
            "properties": {
                "skills": {
                    "type": "array",
                    "items": {"$ref": "#/$defs/concept_id_with_weight"},
                    "minItems": 0,
                    "uniqueItems": True,
                },
                "languages": {
                    "type": "array",
                    "items": {"$ref": "#/$defs/concept_id_with_weight"},
                    "minItems": 0,
                    "uniqueItems": True,
                },
                "work_experiences": {
                    "type": "array",
                    "items": {"$ref": "#/$defs/concept_id_with_weight"},
                    "minItems": 0,
                    "uniqueItems": True,
                },
                "education": {
                    "type": "array",
                    "items": {"$ref": "#/$defs/concept_id_with_weight"},
                    "minItems": 0,
                    "uniqueItems": True,
                },
                "education_level": {
                    "type": "array",
                    "items": {"$ref": "#/$defs/concept_id_with_weight"},
                    "minItems": 0,
                    "uniqueItems": True,
                },
            },
            "additionalProperties": False,
            "required": ["skills", "languages", "work_experiences", "education", "education_level"],
        },
        "nice_to_have": {
            "type": "object",
            "properties": {
                "skills": {
                    "type": "array",
                    "items": {"$ref": "#/$defs/concept_id_with_weight"},
                    "minItems": 0,
                    "uniqueItems": True,
                },
                "languages": {
                    "type": "array",
                    "items": {"$ref": "#/$defs/concept_id_with_weight"},
                    "minItems": 0,
                    "uniqueItems": True,
                },
                "work_experiences": {
                    "type": "array",
                    "items": {"$ref": "#/$defs/concept_id_with_weight"},
                    "minItems": 0,
                    "uniqueItems": True,
                },
                "education": {
                    "type": "array",
                    "items": {"$ref": "#/$defs/concept_id_with_weight"},
                    "minItems": 0,
                    "uniqueItems": True,
                },
                "education_level": {
                    "type": "array",
                    "items": {"$ref": "#/$defs/concept_id_with_weight"},
                    "minItems": 0,
                    "uniqueItems": True,
                },
            },
            "additionalProperties": False,
            "required": ["skills", "languages", "work_experiences", "education", "education_level"],
        },
        "application_contacts": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "name": {"type": "string"},
                    "description": {"type": "string"},
                    "email": {"type": "string"},
                    "telephone": {"type": "string"},
                    "contact_type": {"type": ["string", "null"]},
                },
                "additionalProperties": False,
                "required": ["name", "description", "email", "telephone", "contact_type"],
            },
            "minItems": 0,
        },
        "publication_date": {"type": "string"},  # datetime
        "last_publication_date": {"type": "string"},  # datetime
        "removed": {"type": "boolean"},
        "removed_date": {"type": ["string", "null"]},  # datetime
        "source_type": {"type": "string"},
        "timestamp": {"type": "integer", "minimum": 0},
    },
    "additionalProperties": False,
    # All items in required is currently always provided by api.
    "required": [
        "id",  # non-null required
        "external_id",
        "original_id",
        "label",
        "webpage_url",  # non-null required
        "logo_url",
        "headline",
        "application_deadline",
        "number_of_vacancies",
        "description",
        "employment_type",
        "salary_type",
        "salary_description",
        "duration",
        "working_hours_type",
        "scope_of_work",
        "access",
        "employer",
        "application_details",
        "experience_required",
        "access_to_own_car",
        "driving_license_required",
        "driving_license",
        "occupation",  # non-null required
        "occupation_group",  # non-null required
        "occupation_field",  # non-null required
        "workplace_address",
        "must_have",
        "nice_to_have",
        "application_contacts",
        "publication_date",  # non-null required
        "last_publication_date",
        "removed",  # non-null required
        "removed_date",
        "source_type",
        "timestamp",  # non-null required
    ],
    "$defs": {
        "concept_id": {
            "type": "object",
            "properties": {
                "concept_id": {"type": "string"},
                "label": {"type": "string"},
                "legacy_ams_taxonomy_id": {"type": "string"},
            },
            "required": ["concept_id", "label", "legacy_ams_taxonomy_id"],
        },
        "concept_id_with_weight": {
            "type": "object",
            "properties": {
                "concept_id": {"type": "string"},
                "label": {"type": "string"},
                "legacy_ams_taxonomy_id": {"type": "string"},
                "weight": {"type": "number", "minimum": 0},
            },
            "required": ["concept_id", "label", "legacy_ams_taxonomy_id", "weight"],
        },
    },
}


@pytest.mark.parametrize("ad_id", ["23910624", "24419009", "24471898", "24580979", "24630183", "24645115"])
def test_valid_schema(ad_id, client):
    # Act
    response = client.get(f"/ad/{ad_id}")

    # Assert
    assert response.status_code == 200
    assert response.is_json

    validate(response.json, AD_SCHEMA)  # Throws ValidationError if fails.


NO_AD_SCHEMA = {
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "type": "object",
    "properties": {
        "message": {"type": "string"},
        "status": {"type": "integer"},
    },
    "additionalProperties": False,
}


def test_404_schema(client):
    # Act
    response = client.get("/ad/12345")

    # Assert
    assert response.status_code == 404
    assert response.is_json

    validate(response.json, NO_AD_SCHEMA)  # Throws ValidationError if fails.
