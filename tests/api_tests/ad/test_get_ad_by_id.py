import pytest


@pytest.mark.smoke
@pytest.mark.parametrize("ad_id", ["23910624", "23448227", "24238908", "24252077", "24263960"])
def test_get_ad_by_id(ad_id, client):
    """
    Check that it is possible to get an ad by id.
    """

    # Act
    response = client.get(f"/ad/{ad_id}")

    # Assert
    assert response.status_code == 200
    assert response.is_json
    assert response.json["id"] == ad_id


def test_get_missing_ad_by_id_returns_error(client):
    """
    Check that an error is returned when trying to get an ad that does not exist.
    """
    # Act
    response = client.get("/ad/12345")

    # Assert
    assert response.status_code == 404
    assert response.is_json

    # Verify error message present
    assert response.json["message"].startswith("Ad not found")
