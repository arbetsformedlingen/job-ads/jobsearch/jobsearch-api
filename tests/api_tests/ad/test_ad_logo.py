import pytest


@pytest.mark.skip(reason="No good to test in api_tests since it depends on external service")
def test_request(client):
    assert False


def test_logo_ad_not_found(client):
    # Act
    response = client.get("/ad/112233/logo")

    # Assert
    assert response.status_code == 404

    assert response.json == {
        "message": "Ad not found. You have requested this URI [/ad/112233/logo] but did you mean /ad/<id>/logo ?"
    }
