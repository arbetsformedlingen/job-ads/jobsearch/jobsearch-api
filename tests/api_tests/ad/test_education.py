import pytest  # noqa: F401


def test_must_have_education_fields_present(client):
    """
    Ad should have data in the education* fields
    """
    # Arrange
    ad_id = "24252077"

    # Act
    response = client.get(f"/ad/{ad_id}")

    assert response.json["must_have"]["education"]
    assert response.json["must_have"]["education_level"]


def test_nice_to_have_education_fields_present(client):
    """
    Ad should have data in the education* fields
    """
    # Arrange
    ad_id = "23791205"

    # Act
    response = client.get(f"/ad/{ad_id}")

    assert response.json["nice_to_have"]["education"]
    assert response.json["nice_to_have"]["education_level"]
