import pytest


@pytest.mark.smoke
def test_use_api_key_client(client):
    """
    Check that nothing breaks when using api-key in header
    """
    # Arrange
    headers = {"api-key": "test_api_key"}
    params = {"q": "nyckel", "limit": "5"}

    # Act
    response = client.get("/search", query_string=params, headers=headers)

    # Assert
    assert response.status_code == 200
    assert response.is_json

    # Check that all hits have a headline
    assert all([hit["headline"] for hit in response.json["hits"]])
