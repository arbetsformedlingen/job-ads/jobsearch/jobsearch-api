import pytest  # noqa: F401

import common.constants


def test_swagger_contains_correct_version_string(client):
    # Act
    response = client.get("/swagger.json")

    # Assert
    assert response.status_code == 200
    assert response.json["info"]["version"] == common.constants.API_VERSION


def test_swagger_contains_endpoints(client):
    # Act
    response = client.get("/swagger.json")

    # Assert
    assert response.status_code == 200

    assert response.json["paths"]["/ad/{id}"]
    assert response.json["paths"]["/ad/{id}/logo"]
    assert response.json["paths"]["/complete"]
    assert response.json["paths"]["/search"]
