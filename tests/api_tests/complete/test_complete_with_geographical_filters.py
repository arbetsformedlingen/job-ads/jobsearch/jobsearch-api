import pytest

from tests.test_resources.concept_ids import concept_ids_geo as geo


@pytest.mark.parametrize("q", ["sjuksköt", "servit", "progr"])
def test_returns_less_expected_hits_for_municipality_filter(q, client):
    """
    Check that number of hits is lower when using municipality filter
    """
    # Assert
    query_no_filter = {"q": q, "limit": 1}
    query_municipality_filter = {**query_no_filter, "municipality": geo.stockholm}

    # Act
    response_no_filter = client.get("/complete", query_string=query_no_filter)
    response_municipality_filter = client.get("/complete", query_string=query_municipality_filter)

    # Assert

    top_value_no_filter = response_no_filter.json["typeahead"][0]["occurrences"]
    top_value_municipality_filter = response_municipality_filter.json["typeahead"][0]["occurrences"]

    assert top_value_municipality_filter > 0
    assert top_value_no_filter > top_value_municipality_filter


@pytest.mark.parametrize("q", ["sjuksköt", "servit", "progr"])
def test_returns_less_expected_hits_for_region_filter(q, client):
    """
    Check that number of hits is lower when using region filter
    """
    # Assert
    query_no_filter = {"q": q, "limit": 1}
    query_region_filter = {**query_no_filter, "region": geo.gavleborgs_lan}

    # Act
    response_no_filter = client.get("/complete", query_string=query_no_filter)
    response_region_filter = client.get("/complete", query_string=query_region_filter)

    # Assert

    top_value_no_filter = response_no_filter.json["typeahead"][0]["occurrences"]
    top_value_region_filter = response_region_filter.json["typeahead"][0]["occurrences"]

    assert top_value_region_filter > 0
    assert top_value_no_filter > top_value_region_filter
