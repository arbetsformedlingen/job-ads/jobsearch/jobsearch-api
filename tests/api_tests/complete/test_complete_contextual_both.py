import logging

import pytest
import requests

log = logging.getLogger(__name__)

# from tests.test_resources.helper import get_complete, get_complete_expect_error, compare_two_lists, \
#     get_typeahead_values, check_complete_results


@pytest.mark.parametrize("query, expected", [
    ("", ['sverige', 'svenska', 'stockholms län', 'körkort', 'engelska', 'stockholm', 'västra götaland',
          'västra götalands län', 'skåne', 'skåne län']),
    (" ", ['sverige', 'svenska', 'stockholms län', 'körkort', 'engelska', 'stockholm', 'västra götaland',
           'västra götalands län', 'skåne', 'skåne län']),
    ("\t", ['sverige', 'svenska', 'stockholms län', 'körkort', 'engelska', 'stockholm', 'västra götaland',
            'västra götalands län', 'skåne', 'skåne län']),
    ("qwerty", []),
    ("qwerty ", []),
    ("java      python ",
     ['java python stockholms län', 'java python systemutvecklare', 'java python stockholm', 'java python design',
      'java python västra götaland', 'java python västra götalands län', 'java python programmerare',
      'java python göteborg', 'java python data', 'java python engelska']
     ),
    ("java\tpython ",
     ['java python stockholms län', 'java python systemutvecklare', 'java python stockholm', 'java python design',
      'java python västra götaland', 'java python västra götalands län', 'java python programmerare',
      'java python göteborg', 'java python data', 'java python engelska']

     ),
    ("java\t python ",
     ['java python stockholms län', 'java python systemutvecklare', 'java python stockholm', 'java python design',
      'java python västra götaland', 'java python västra götalands län', 'java python programmerare',
      'java python göteborg', 'java python data', 'java python engelska']
     ),
])
def test_complete_basic_tests(query, expected, client):
    """
    Testing basic & extreme cases (including whitespaces)
    """
    response = client.get("/complete", query_string={'q': query})
    actual = [item['value'] for item in response.json['typeahead']]

    log.debug(f"actual: {actual}")

    assert sorted(actual) == sorted(expected), f"Lists do not match.\nActual list: {actual}\nExpected list: {expected}"


@pytest.mark.parametrize("contextual", [True, False])
@pytest.mark.parametrize("query, expected", [('servit', ['servitris', 'servitör', 'servitriser', 'servitörer']),
                                             ('systemutvecklare angu', ['systemutvecklare angular',
                                                                        'systemutvecklare angularjs']),
                                             ('angu', ['angular', 'angularjs', 'angular.js']),
                                             ('ang', ['angularjs', 'angular', 'angular.js', 'angered']),
                                             ('pyth', ['python', 'python-utvecklare', 'pythonutvecklare'])])
def test_complete_endpoint_synonyms_typeahead(query, expected, contextual, client):
    """
    Test that incomplete search queries will return suggestions
    first arg is the query
    second arg is a list of expected synonyms, which all must be found in the response
    """
    response = client.get("/complete", query_string={'q': query, 'contexual': contextual})
    actual = [item['value'] for item in response.json['typeahead']]

    assert sorted(actual) == sorted(expected), f"Lists do not match.\nActual list: {actual}\nExpected list: {expected}"


@pytest.mark.parametrize("contextual", [True, False])
@pytest.mark.parametrize("query, expected",
                         [('c#', ['c#.net', 'c#.net stockholm', 'c#.net stockholms län']), ('c+', ['c++', 'c++-utvecklare', 'c++ developer'])])
def test_synonyms_special_chars(query, expected, contextual, client):
    response = client.get("/complete", query_string={'q': query, 'contexual': contextual})
    actual = [item['value'] for item in response.json['typeahead']]

    assert sorted(actual) == sorted(expected), f"Lists do not match.\nActual list: {actual}\nExpected list: {expected}"


@pytest.mark.parametrize("query, expected", [
    ('systemutvecklare angular', ['systemutvecklare angularjs']),
    ('#coro', ['corona', 'corona helsingborg', 'corona skåne', 'corona skåne län']),
    ('#coron', ['corona', 'corona helsingborg', 'corona skåne', 'corona skåne län']),
    ('c',
     ['civilingenjör', 'cloud', 'c#', 'coachning', 'c++', 'chaufför', 'cad', 'ci/cd', 'c-körkort', 'css']
     ),
    ('uppd', ['uppdragsledning', 'uppdragsutbildning', 'uppdragsutbildningar', 'uppdragsansvarig', 'uppdragsledare',
              'uppdragsanalyser', 'uppdragsansvar', 'uppdragsledarerfarenhet', 'uppdragsverksamhet', 'uppdukning']),
    ('underh',
     ['underhållsarbete', 'underhållsmekaniker', 'underhållning', 'underhållsarbeten', 'underhållsplaner',
      'underhållsprojekt', 'underhållssystem', 'underhållsingenjör', 'underhållstekniker', 'underhåll och reparation']
     ),
    ('sjuks',
     ['sjuksköterska', 'sjuksköterskor', 'sjuksköterskeuppgifter', 'sjuksköterskelegitimation', 'sjuksköterskeexamen',
      'sjuksköterskan', 'sjuksköterskearbete', 'sjuksköterskemottagning', 'sjuksköterskeutbildning',
      'sjukskrivningsfrågor']
     ),
    ('arbetsl',
     ['arbetslivserfarenhet', 'arbetsledning', 'arbetsledare', 'arbetsliv', 'arbetslivspsykologi', 'arbetslivsfrågor',
      'arbetslivserfarenheter', 'arbetslagsarbete', 'arbetsledaransvar', 'arbetsledarerfarenhet']
     ),

])
def test_complete_endpoint_with_spellcheck_typeahead(query, expected, client):
    response = client.get("/complete", query_string={'q': query})
    actual = [item['value'] for item in response.json['typeahead']]
    log.debug(f"actual: {actual}")

    assert sorted(actual) == sorted(expected), f"Lists do not match.\nActual list: {actual}\nExpected list: {expected}"


def test_check_400_bad_request_when_limit_is_greater_than_allowed(client):
    """
    Test that a limit of 51 will give a '400 BAD REQUEST' response with a meaningful error message
    """
    # response = get_complete_expect_error({'q': 'x', 'limit': 51},
    #                                      expected_http_code=requests.codes.bad_request)
    expected_http_code = requests.codes.bad_request
    response = client.get("/complete", query_string={'q': 'x', 'limit': 51})
    assert response.status_code == expected_http_code, f"Expected http return code to be {expected_http_code} , but got {response.status_code}"


    response_json = response.json
    assert response_json['errors']['limit'] == 'Invalid argument: 51. argument must be within the range 0 - 50'
    assert response_json['message'] == 'Input payload validation failed'


@pytest.mark.parametrize("query, expected", [
    ("stor",
     ['storkök', 'storage', 'storhushåll', 'storstädning', 'storköksutbildning', 'stora datamängder', 'storstädningar',
      'storbritannien', 'storuman', 'storhushållsutbildning']
     ),
    ("stor s",
     ['stor sverige', 'stor svenska', 'stor stockholms län', 'stor stockholm', 'stor skåne', 'stor skåne län',
      'stor sjukvård', 'stor sjuksköterska', 'stor samverkan', 'stor språkkunskaper']),
    ("storag",
     ['storage', 'storage stockholm', 'storage stockholms län', 'storage göteborg', 'storage västra götaland',
      'storage västra götalands län']),
    ("storage s",
     ['storage sverige', 'storage stockholm', 'storage stockholms län', 'storage sales', 'storage svenska',
      'storage scala', 'storage sql', 'storage switch', 'storage systemutvecklare', 'storage sampling']),
])
def test_complete_multiple_words(query, expected, client):
    response = client.get("/complete", query_string={'q': query})
    actual = [item['value'] for item in response.json['typeahead']]

    assert sorted(actual) == sorted(expected), f"Lists do not match.\nActual list: {actual}\nExpected list: {expected}"


@pytest.mark.parametrize("contextual", [True, False])
def test_complete_for_locations_with_space_and_contextual_param(contextual, client):
    """
    Test typeahead for location with trailing space after city name,
    and using parameter 'contextual' True or False
    """
    query = 'malmö '
    expected = ['malmö försäljare',
                'malmö säljare',
                'malmö sjuksköterska',
                'malmö innesäljare',
                'malmö lärare',
                'malmö telefonförsäljare']

    response = client.get("/complete", query_string={'q': query, 'limit': 10, 'contextual': contextual})
    actual = [item['value'] for item in response.json['typeahead']]

    assert sorted(actual) == sorted(expected), f"Lists do not match.\nActual list: {actual}\nExpected list: {expected}"


@pytest.mark.parametrize("query, expected", [
    ("göteborg sjuksköterska", ['göteborg sjuksköterskan']),
    ("götteborg sjuksköterska", ['göteborg sjuksköterska']),

    ("götteborg sjukssköterska",
     ['göteborg sjuksköterska', 'göteborg sjuksköterskan', 'göteborg sjukssköterska', 'götteborg sjuksköterska',
      'götteborg sjuksköterskan']),
    ("göteborg sjukssköterska", ['göteborg sjuksköterska', 'göteborg sjuksköterskan']),
    ("malmö sjukssköterska läckare",
     ['malmö sjuksköterska lärare', 'malmö sjuksköterskan lärare', 'malmö sjukssköterska lärare',
      'malmö sjuksköterska läkare', 'malmö sjuksköterska packare', 'malmö sjuksköterska plockare',
      'malmö sjuksköterskan läkare', 'malmö sjukssköterska läkare', 'malmö sjuksköterska lättare',
      'malmö sjuksköterska läckare']
     ),
    ("kiruna sjukssköterska läkkare",
     ['kiruna sjuksköterska lärare', 'kiruna sjuksköterska läkare', 'kiruna sjuksköterska väktare',
      'kiruna sjuksköterskan lärare', 'kiruna sjuksköterska mäklare', 'kiruna sjuksköterska lättare',
      'kiruna sjukssköterska lärare', 'kiruna sjuksköterska läkkare', 'kiruna sjuksköterskan läkare',
      'kiruna sjukssköterska läkare']
     ),
    ("piteå sjukssköterska lääkare",
     ['piteå sjuksköterska lärare', 'piteå sjuksköterska läkare', 'piteå sjuksköterskan lärare',
      'piteå sjukssköterska lärare', 'piteå sjuksköterskan läkare', 'piteå sjukssköterska läkare',
      'piteå sjuksköterska lättare', 'piteå sjuksköterska lääkare', 'boteå sjukssköterska lärare',
      'boteå sjuksköterska lääkare']
     ),
    ("göteborg sjukssköterska läkare", ['göteborg sjuksköterska läkare', 'göteborg sjuksköterskan läkare']),
    ("läckare", ['läkare', 'läkare stockholms län', 'läkare skåne', 'läkare skåne län', 'läkare stockholm',
                 'läkare jönköpings län']),
    ("götteborg", ['göteborg', 'göteborg systemutvecklare', 'göteborg sjuksköterska', 'göteborg civilingenjör',
                   'göteborg mjukvaruutvecklare', 'göteborg programmerare', 'göteborg personlig assistent']

     ),
    ("stokholm", ['stockholms län', 'stockholm', 'stockholm city']),

    ("stockhlm", ['stockholms län', 'stockholm', 'stockholm city']),
    ("stockhlm ", ['stockholms län', 'stockholm', 'stockholm city']),  # trailing space
    ("   stockhlm   ", ['stockholms län', 'stockholm', 'stockholm city']),  # leading + trailing spaces

    ("stokholm lärarre",
     ['stockholms lärande', 'stockholms lärare', 'stockholms läraren', 'stockholm lärande', 'stockholms läkare',
      'stockholm lärare', 'stockholm läraren', 'stockholm läkare', 'stockholms bärare', 'stockholm bärare']
     ),
    ("karlstad sjukssköterska läckare",
     ['karlstad sjuksköterska läkare', 'karlstad sjuksköterskan läkare', 'karlstad sjukssköterska läkare',
      'karlstad sjuksköterska lärare', 'karlstad sjuksköterskan lärare', 'karlstad sjuksköterska packare',
      'karlstad sjuksköterska plockare', 'karlstad sjukssköterska lärare', 'karlstad sjuksköterska lättare',
      'karlstad sjuksköterska läckare']
     ),
    ("hässleholm läckare sjukssköterska",
     ['hässleholm lärare sjuksköterska', 'hässleholm lärare sjuksköterskan', 'hässleholm lärare sjukssköterska',
      'hässleholm läkare sjuksköterska', 'hässleholm packare sjuksköterska', 'hässleholm plockare sjuksköterska',
      'hässleholm läkare sjuksköterskan', 'hässleholm läkare sjukssköterska', 'hässleholm lättare sjuksköterska',
      'hässleholm läckare sjuksköterska']
     ),
    ("läckare götteborg",
     ['lärare göteborg', 'läkare göteborg', 'lättare göteborg', 'packare göteborg', 'plockare göteborg',
      'lärare götteborg', 'läckare göteborg', 'läkare götteborg', 'lättare götteborg', 'packare götteborg']
     ),
    ("läckare göteborg",
     ['lärare göteborg', 'läkare göteborg', 'lättare göteborg', 'packare göteborg', 'plockare göteborg']),
    ("stockholm läckare göteborg",
     ['stockholm lärare göteborg', 'stockholm läkare göteborg', 'stockholm lättare göteborg',
      'stockholm packare göteborg', 'stockholm plockare göteborg']),
    ("stockhlm läckare göteborg",
     ['stockholm lärare göteborg', 'stockholm läkare göteborg', 'stockholm lättare göteborg',
      'stockholms lättare göteborg', 'stockholms lärare göteborg', 'stockholms läkare göteborg',
      'stockholms packare göteborg', 'stockholms plockare göteborg', 'stockholm packare göteborg',
      'stockholm plockare göteborg']
     ),
])
def test_complete_spelling_correction_multiple_words(query, expected, client):
    """
    Test typeahead with multiple (misspelled) words
    """
    response = client.get("/complete", query_string={'q': query})
    actual = [item['value'] for item in response.json['typeahead']]

    log.debug(f"actual: {actual}")

    assert sorted(actual) == sorted(expected), f"Lists do not match.\nActual list: {actual}\nExpected list: {expected}"

@pytest.mark.parametrize("query, expected", [
    ("stokholm ", ['stockholms län', 'stockholm', 'stockholm city']),  # trailing space
    ("stokholm  ", ['stockholms län', 'stockholm', 'stockholm city']),  # 2 trailing spaces
    ("simrishamn läckare sjukssköterska ",
     ['simrishamn lärare sjuksköterska', 'simrishamn läkare sjuksköterska', 'simrishamn lärare sjuksköterskan',
      'simrishamn lättare sjuksköterska', 'simrishamn packare sjuksköterska', 'simrishamn plockare sjuksköterska',
      'simrishamn lärare sjukssköterska', 'simrishamn läckare sjuksköterska', 'simrishamn läkare sjuksköterskan',
      'simrishamn läkare sjukssköterska']
     ),
    ("stockhlm läckare göteborg ",
     ['stockholm lärare göteborg', 'stockholm läkare göteborg', 'stockholm lättare göteborg',
      'stockholms lättare göteborg', 'stockholms lärare göteborg', 'stockholms läkare göteborg',
      'stockholms packare göteborg', 'stockholms plockare göteborg', 'stockholm packare göteborg',
      'stockholm plockare göteborg']
     ),  # trailing space
    ("stockhlm läckare göteborg  ",
     ['stockholm lärare göteborg', 'stockholm läkare göteborg', 'stockholm lättare göteborg',
      'stockholms lättare göteborg', 'stockholms lärare göteborg', 'stockholms läkare göteborg',
      'stockholms packare göteborg', 'stockholms plockare göteborg', 'stockholm packare göteborg',
      'stockholm plockare göteborg']
     ),  # 2 trailing space
    ("stockholm läkare götteborg ",
     ['stockholm läkare göteborg']
     ),  # only last word misspelled, trailing space
])
def test_complete_spelling_correction_multiple_words_and_trailing_space(query, expected, client):
    """
    Test typeahead with multiple (misspelled) words
    """
    response = client.get("/complete", query_string={'q': query})
    actual = [item['value'] for item in response.json['typeahead']]

    log.debug(f"actual: {actual}")

    assert sorted(actual) == sorted(expected), f"Lists do not match.\nActual list: {actual}\nExpected list: {expected}"


@pytest.mark.parametrize("query, expected", [
    ("läckare", ['läkare', 'läkare stockholms län', 'läkare skåne', 'läkare skåne län', 'läkare stockholm',
                 'läkare jönköpings län']),
    ("götteborg", ['göteborg', 'göteborg systemutvecklare', 'göteborg sjuksköterska', 'göteborg civilingenjör',
                   'göteborg mjukvaruutvecklare', 'göteborg programmerare', 'göteborg personlig assistent']

     ),

    ("stokholm", ['stockholms län', 'stockholm', 'stockholm city']),
    ("stokholm ", ['stockholms län', 'stockholm', 'stockholm city']),  # trailing space
    ("stokholm  ", ['stockholms län', 'stockholm', 'stockholm city']),  # 2 trailing spaces
    ("stockhlm", ['stockholms län', 'stockholm', 'stockholm city']),
    ("stockhlm ", ['stockholms län', 'stockholm', 'stockholm city']),  # trailing space
    ("   stockhlm   ", ['stockholms län', 'stockholm', 'stockholm city']),  # leading + trailing spaces

    ("stockholm pythan", ['stockholm python', 'stockholm python-utvecklare']),
    ("läkare götteborg", ['läkare göteborg']),
    ("läkare götteborg", ['läkare göteborg']),
])
def test_spelling_correction_with_complete_suggest(query, expected, client):
    """
    Test typeahead with only one (misspelled) word
    """
    response = client.get("/complete", query_string={'q': query})
    actual = [item['value'] for item in response.json['typeahead']]

    assert sorted(actual) == sorted(expected), f"Lists do not match.\nActual list: {actual}\nExpected list: {expected}"



@pytest.mark.parametrize("query, expected", [
    ("python ",
     ['python stockholms län', 'python design', 'python stockholm', 'python systemutvecklare', 'python västra götaland',
      'python västra götalands län', 'python data', 'python göteborg', 'python amazon web services', 'python aws']

     ),
    ("python  ",
     ['python stockholms län', 'python design', 'python stockholm', 'python systemutvecklare', 'python västra götaland',
      'python västra götalands län', 'python data', 'python göteborg', 'python amazon web services', 'python aws']

     ),  # 2 trailing spaces
    ("läkare ",
     ['läkare specialistläkare', 'läkare st-läkare', 'läkare stockholms län', 'läkare sjukvård', 'läkare allmänmedicin',
      'läkare skåne', 'läkare skåne län', 'läkare sjuksköterska', 'läkare språkkunskaper', 'läkare stockholm']

     ),
    ("   läkare   ",
     ['läkare specialistläkare', 'läkare st-läkare', 'läkare stockholms län', 'läkare sjukvård', 'läkare allmänmedicin',
      'läkare skåne', 'läkare skåne län', 'läkare sjuksköterska', 'läkare språkkunskaper', 'läkare stockholm']

     ),  # leading + trailing spaces
])
def test_remove_same_word_function(query, expected, client):
    """
    Test when input is word with space, the result will not show the same word
    """
    response = client.get("/complete", query_string={'q': query})
    actual = [item['value'] for item in response.json['typeahead']]

    assert sorted(actual) == sorted(expected), f"Lists do not match.\nActual list: {actual}\nExpected list: {expected}"


@pytest.mark.parametrize("query, expected", [
    ("java nystartsjob", ['java nystartsjobb']),
    ("java nystar", ['java nystartsjobb']),
    ("java rekryteringsut",
     ['java rekryteringsutbildning']),  # rekryteringsutbildning is both an enriched term and a label.
    ("java räksm", ['java räksmörgås']),
])
def test_label_complete(query, expected, client):
    """
    Test that complete for ad labels works.
    """
    response = client.get("/complete", query_string={'q': query})
    actual = [item['value'] for item in response.json['typeahead']]

    assert sorted(actual) == sorted(expected), f"Lists do not match.\nActual list: {actual}\nExpected list: {expected}"


@pytest.mark.parametrize("qfield, expected", [
    ('occupation', ['sjuksköterska', 'lärare', 'försäljare', 'säljare', 'lärare i grundskolan', 'personlig assistent',
                    'systemutvecklare', 'sjuksköterskor', 'grundlärare', 'ämneslärare']),
    ('skill',
     ['svenska', 'körkort', 'engelska', 'b-körkort', 'arbetslivserfarenhet', 'försäljning', 'sjukvård', 'datorvana',
      'barn', 'dokumentation']),
    ('location',
     ['sverige', 'stockholms län', 'stockholm', 'västra götaland', 'västra götalands län', 'skåne', 'skåne län',
      'göteborg', 'östergötland', 'östergötlands län']),
    ('employer', ['viva bemanning', 'randstad', 'region skåne', 'västra götalandsregionen', 'annsam', 'academic work',
                  'centric care', 'studentconsulting', 'humana', 'almia']),

])
def test_q_fields(qfield, expected, client):
    """
     ['occupation', 'skill', 'location', 'employer']
    """
    response = client.get("/complete", query_string={'qfields': qfield})
    actual = [item['value'] for item in response.json['typeahead']]

    assert sorted(actual) == sorted(expected), f"Lists do not match.\nActual list: {actual}\nExpected list: {expected}"


@pytest.mark.parametrize("query, expected", [
    ("python ",
     ['python stockholms län', 'python design', 'python stockholm', 'python systemutvecklare', 'python västra götaland',
      'python västra götalands län', 'python data', 'python göteborg', 'python amazon web services', 'python aws']
     ),
    ("python  ",
     ['python stockholms län', 'python design', 'python stockholm', 'python systemutvecklare', 'python västra götaland',
      'python västra götalands län', 'python data', 'python göteborg', 'python amazon web services', 'python aws']

     ),  # 2 trailing spaces
    ("läkare ",
     ['läkare specialistläkare', 'läkare st-läkare', 'läkare stockholms län', 'läkare sjukvård', 'läkare allmänmedicin',
      'läkare skåne', 'läkare skåne län', 'läkare sjuksköterska', 'läkare språkkunskaper', 'läkare stockholm']

     ),
    (" läkare ",
     ['läkare specialistläkare', 'läkare st-läkare', 'läkare stockholms län', 'läkare sjukvård', 'läkare allmänmedicin',
      'läkare skåne', 'läkare skåne län', 'läkare sjuksköterska', 'läkare språkkunskaper', 'läkare stockholm']

     ),  # leading + trailing spaces
    ("snickare jönköpings lä",
     ['snickare jönköpings län', 'snickare jönköpings läsa ritningar']
     ),
    ("stockholms län jönköpings lä",
     ['stockholms län jönköpings län', 'stockholms län jönköpings ls', 'stockholms län jönköpings la']
     ),
])
def test_remove_same_word_function( query, expected, client):
    """
    Test when input is word with space, the result will not show the same word
    """
    response = client.get("/complete", query_string={'q': query, 'contextual': True})
    actual = [item['value'] for item in response.json['typeahead']]
    log.debug(f"actual: {actual}")

    assert sorted(actual) == sorted(expected), f"Lists do not match.\nActual list: {actual}\nExpected list: {expected}"
