import pytest

from tests.test_resources.concept_ids import concept_ids_geo as geo


@pytest.mark.parametrize('query', ['sjukköt', 'sjuksköt', 'servit', 'serivtr', 'progr', 'proggram'])
def test_different_numbers_with_filter( query, client):
    """
    Check that number of hits is lower when using a geographical filter and a misspelled word
    """
    json_response_no_filter = client.get("/complete", query_string={'q': query}).json
    top_value_no_filter = json_response_no_filter['typeahead'][0]['occurrences']
    top_word_no_filter = json_response_no_filter['typeahead'][0]['value']

    json_response_municipality_filter = client.get("/complete", query_string={'q': query, 'municipality': geo.stockholm}).json
    top_value_municipality_filter = json_response_municipality_filter['typeahead'][0]['occurrences']
    top_word_municipality_filter = json_response_municipality_filter['typeahead'][0]['value']

    json_response_region_filter = client.get("/complete", query_string={'q': query, 'region': geo.stockholms_lan}).json
    top_value_region_filter = json_response_region_filter['typeahead'][0]['occurrences']
    top_word_regionfilter = json_response_region_filter['typeahead'][0]['value']

    msg_city = f"Query: {query}, no filter: {top_word_no_filter}: {top_value_no_filter}, geo filter: {top_word_municipality_filter}: {top_value_municipality_filter} "
    msg_region = f"Query: {query}, no filter: {top_word_no_filter}: {top_value_no_filter}, geo filter: {top_word_regionfilter}: {top_value_region_filter} "

    assert top_value_no_filter > top_value_municipality_filter, msg_city
    assert top_value_no_filter > top_value_region_filter, msg_region

    assert top_value_municipality_filter > 0
    assert top_value_region_filter > 0
