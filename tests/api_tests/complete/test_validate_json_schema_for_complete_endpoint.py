import pytest
from jsonschema import validate

COMPLETE_SCHEMA = {
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "type": "object",
    "properties": {
        "result_time_in_millis": {"type": "integer", "minimum": 0},
        "time_in_millis": {"type": "integer", "minimum": 0},
        "typeahead": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "value": {"type": "string"},
                    "found_phrase": {"type": "string"},
                    "type": {
                        "enum": [
                            # What other types are there?
                            "compound",
                            "location",
                            "occupation",
                            "skill",
                            "location_occupation",
                            "occupation_remainder",
                        ]
                    },
                    "occurrences": {"type": "integer", "minimum": 0},
                },
                "additionalProperties": False,
                "required": ["value", "found_phrase", "type", "occurrences"],
            },
            "minItems": 0,
        },
    },
    "additionalProperties": False,
    "required": ["result_time_in_millis", "time_in_millis"],
}


@pytest.mark.parametrize(
    "query",
    [
        {"q": ""},
        {"q": "s"},
        {"q": "stock"},
        {"q": "eksjö"},
        {"q": "eksjö "},  # Note the trailing space
        {"q": "python skatt"},
        {"q": "lärare "},
        {"q": "xyz"},
        {"q": "ingen", "published-before": "2021-01-25T07:29:41"},
    ],
)
def test_valid_schema(query, client):
    # Act
    response = client.get(f"/complete", query_string=query)

    # Assert
    assert response.status_code == 200
    assert response.is_json

    validate(response.json, COMPLETE_SCHEMA)  # Throws ValidationError if fails.
