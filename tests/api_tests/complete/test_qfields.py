import pytest


@pytest.mark.parametrize(
    "qfield, expected_suggestions",
    [
        [
            "occupation",
            [
                "skadedjurstekniker",
                "skadereglerare",
                "skadetekniker",
                "skattehandläggare",
                "skattejurist",
            ],
        ],
        [
            "skill",
            [
                "skattedeklaration",
                "skatt",
                "skattedeklarationer",
                "skattefrågor",
                "skatteberäkningar",
                "skatter och avgifter",
                "skadedjursbekämpning",
                "skadereglering",
                "skadesanering",
                "skadeverksamhet",
                "skadeverkstad",
                "skapande verksamhet",
                "skarvning",
                "skatteberäkning",
                "skatterapportering",
                "skatteregler",
                "skattesystem",
            ],
        ],
        [
            "location",
            [
                "skara",
                "skarpnäck",
                "skanör",
            ],
        ],
        [
            "employer",
            [
                "skara kommun",
                "skatteverket",
                "skandinaviska stift för utbildning",
                "skansen",
                "skara djursjukhus/smådjur",
                "skaraborgs sjukhus, m5, rättspsykiatri",
                "skaraborgs sjukhus, område kirurgi, k6, läkare an-op-iva, skas",
                "skaraborgs sjukhus, område medicin, m2, avd 83-84",
                "skaraborgs sjukhus, vuxenpsykiatrisk öppenvårdsmott falköping",
                "skaraborgsassistans",
            ],
        ],
    ],
)
def test_complete_with_qfields(qfield, expected_suggestions, client):
    """
    ['occupation', 'skill', 'location', 'employer']
    """
    # Arrange
    query = {"q": "ska", "qfields": qfield, "limit": 20}

    # Act
    response = client.get("/complete", query_string=query)

    # Assert
    assert [s["value"] for s in response.json["typeahead"]] == expected_suggestions
