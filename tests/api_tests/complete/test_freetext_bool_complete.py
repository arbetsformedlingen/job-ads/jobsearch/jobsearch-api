import pytest

from common.constants import X_FEATURE_FREETEXT_BOOL_METHOD


@pytest.mark.parametrize(
    "q", ["stockholm sj", "stor s", "lärare st", "special a", "stockholm  ", "  ", "programmering  "]
)
def test_freetext_bool_method_should_do_nothing(q, client):
    """
    test of /complete endpoint with X_FEATURE_FREETEXT_BOOL_METHOD set to 'and' / 'or' / default value
    Verifies that results are identical regardless of bool method
    """
    # Arrange
    query = {"q": q, "limit": 10}

    # Act
    # no special header, default values are used
    response_default = client.get("/complete", query_string=query)
    # use 'and'
    response_with_and = client.get("/complete", query_string=query, headers={X_FEATURE_FREETEXT_BOOL_METHOD: "and"})
    # use 'or'
    response_with_or = client.get("/complete", query_string=query, headers={X_FEATURE_FREETEXT_BOOL_METHOD: "or"})

    # Assert
    # Make sure we have something to compare.
    assert len(response_default.json["typeahead"]) > 0

    # check that results are identical regardless of which bool_method is used
    assert (
        response_default.json["typeahead"] == response_with_and.json["typeahead"] == response_with_or.json["typeahead"]
    )
