import pytest
import logging
log = logging.getLogger(__name__)

def test_complete_returns_status_200(client):
    # Act
    response = client.get("/complete")

    # Assert
    assert response.status_code == 200


@pytest.mark.smoke
def test_complete_returns_expected_typeahead(client):
    # Arrange
    query = {"q": ""}

    # Act
    response = client.get("/complete", query_string=query)

    # Assert
    assert response.status_code == 200

    assert [s["value"] for s in response.json["typeahead"]] == [
        "sverige",
        "svenska",
        "stockholms län",
        "körkort",
        "engelska",
        "stockholm",
        "västra götaland",
        "västra götalands län",
        "skåne",
        "skåne län",
    ]


@pytest.mark.parametrize(
    "q",
    [
        "fiskare ",
        "fiskare göteborg ",
        "fiskare malmö ",
        "tekniker ",
        "tekniker operatör ",
        "tekniker öperatör fordon ",
    ],
)
def test_complete_with_contextual_false(q, client):
    """
    When contextual is set to false all searches should ignore previous words.
    """
    # Arrange
    query = {"q": q, "contextual": "false"}

    # Act
    response = client.get("/complete", query_string=query)

    # Assert
    actual_completions = [s["value"] for s in response.json["typeahead"]]
    expected_completions = [
        f"{q}{completion}"
        for completion in [
            "stockholms län",
            "körkort",
            "engelska",
            "stockholm",
            "västra götaland",
            "västra götalands län",
            "skåne",
            "skåne län",
            "b-körkort",
            "sjuksköterska",
        ]
    ]
    assert actual_completions == expected_completions


@pytest.mark.parametrize(
    "q, expected_completions",
    [
        [
            "tekniker operatör ",
            [
                "tekniker operatör engelska",
                "tekniker operatör felsökning",
                "tekniker operatör körkort",
                "tekniker operatör stockholms län",
                "tekniker operatör teknik",
                "tekniker operatör el",
                "tekniker operatör arbetslivserfarenhet",
                "tekniker operatör industri",
                "tekniker operatör stockholm",
                "tekniker operatör västra götaland",
            ],
        ],
        [
            "tekniker fordon ",
            [
                "tekniker fordon automation",
                "tekniker fordon el",
                "tekniker fordon engelska",
                "tekniker fordon felsökning",
                "tekniker fordon fräsning",
                "tekniker fordon grönytemaskiner",
                "tekniker fordon hydraulik",
                "tekniker fordon lastbilar",
                "tekniker fordon pneumatik",
                "tekniker fordon produktionsutrustning",
            ],
        ],
        [
            "tekniker hydraulik ",
            [
                "tekniker hydraulik el",
                "tekniker hydraulik felsökning",
                "tekniker hydraulik svetsning",
                "tekniker hydraulik automation",
                "tekniker hydraulik drift",
                "tekniker hydraulik drifting",
                "tekniker hydraulik elektronik",
                "tekniker hydraulik engelska",
                "tekniker hydraulik fastigheter",
                "tekniker hydraulik fordon",
            ],
        ],
    ],
)
def test_complete_with_contextual_true(q, expected_completions, client):
    """
    When contextual is set to true all words in the query should affect the response.
    """
    # Arrange
    query = {"q": q, "contextual": "true"}

    # Act
    response = client.get("/complete", query_string=query)

    # Assert
    actual_completions = [s["value"] for s in response.json["typeahead"]]
    assert actual_completions == expected_completions


@pytest.mark.parametrize("q", ["c", "uppd", "underh"])
def test_complete_with_partial_word(q, client):
    """
    When the query is a partial word, the response should contain completions for that word.
    """
    # Arrange
    query = {"q": q, "limit": 50}

    # Act
    response = client.get("/complete", query_string=query)

    # Assert
    # Make sure we have suggestions
    assert len(response.json["typeahead"]) > 0

    # All suggestions should start with the partial word from the query
    assert all([completion.startswith(q) for completion in [s["value"] for s in response.json["typeahead"]]])


@pytest.mark.parametrize(
    "q, expected_suggestions",
    [
        [
            "läckare",
            [
                "läkare",
                "läkare stockholms län",
                "läkare skåne",
                "läkare skåne län",
                "läkare stockholm",
                "läkare jönköpings län",
            ],
        ],
        [
            "lärarre",
            [
                "lärare",
                "läraren",
                "lärarlegitimation",
                "lärare i gymnasieskolan",
                "lärare i gymnasiet",
                "lärare i förskola",
                "lärare i fritidshem",
                "lärare i förskoleklass",
            ],
        ],
        [
            "stokholm",
            [
                "stockholm city",
                "stockholms län",
                "stockholm",
            ],
        ],
    ],
)
def test_unordered_complete_with_misspelt_word(q, expected_suggestions, client):
    """
    Test queries with misspelt word.
    """
    # Arrange
    query = {"q": q, "limit": 10}

    # Act
    response = client.get("/complete", query_string=query)

    # Assert
    json_response = response.json["typeahead"]
    # Check that all expected suggestions are in the response, unordered since the order is not guaranteed.
    for expected_suggestion in expected_suggestions:
        assert expected_suggestion in [s["value"] for s in json_response]


@pytest.mark.parametrize(
    "q, expected_suggestions",
    [
        [
            "läckare götteborg",
            [
                "lättare götteborg",
                "lärare göteborg",
                "lättare göteborg",
                "läkare göteborg",
                "packare göteborg",
                "plockare göteborg",
                "lärare götteborg",
                "läckare göteborg",
                "läkare götteborg",
                "packare götteborg",
            ],
        ],
        [
            "stokholm lärarre",
            ['stockholms lärande', 'stockholms lärare', 'stockholms läraren', 'stockholm lärande', 'stockholms läkare',
             'stockholm lärare', 'stockholm läraren', 'stockholm läkare', 'stockholms bärare', 'stockholm bärare'],
        ],
        [
            "götteborg sjukssköterska",
            ['göteborg sjuksköterska', 'göteborg sjuksköterskan', 'göteborg sjukssköterska', 'götteborg sjuksköterska',
             'götteborg sjuksköterskan'],
        ],
    ],
)
def test_complete_with_multiple_misspelt_words(q, expected_suggestions, client):
    """
    Test queries with multiple misspelt word.
    """
    # Arrange
    query = {"q": q, "limit": 20}

    # Act
    response = client.get("/complete", query_string=query)

    # Assert
    actual_completions = [s["value"] for s in response.json["typeahead"]]
    log.debug(f"actual: {actual_completions}")
    assert actual_completions == expected_suggestions


def test_complete_with_extra_trailing_spaces(client):
    """
    Test that query with multiple trailing spaces return the same result as with a single trailing space.
    """
    # Arrange
    single_space_query = {"q": "python ", "limit": 10}
    multi_space_query = {"q": "python    ", "limit": 10}

    # Act
    single_space_response = client.get("/complete", query_string=single_space_query)
    multi_space_query = client.get("/complete", query_string=multi_space_query)

    # Assert
    assert single_space_response.json["typeahead"] == multi_space_query.json["typeahead"]


def test_complete_with_leading_spaces(client):
    """
    Test that query with multiple leading spaces return the same result as with a single trailing space.
    """
    # Arrange
    no_space_query = {"q": "python", "limit": 10}
    multi_space_query = {"q": "   python", "limit": 10}

    # Act
    no_space_response = client.get("/complete", query_string=no_space_query)
    multi_space_query = client.get("/complete", query_string=multi_space_query)

    # Assert
    assert no_space_response.json["typeahead"] == multi_space_query.json["typeahead"]


@pytest.mark.parametrize(
    "q, expected_suggestions",
    [
        ["java nystartsjob", ["java nystartsjobb"]],
        ["java nystar", ["java nystartsjobb"]],
        [
            "java rekryteringsut",
            ["java rekryteringsutbildning"],
        ],  # rekryteringsutbildning is both an enriched term and a label.
        ["java räksm", ["java räksmörgås"]],
    ],
)
def test_complete_with_label_in_query(q, expected_suggestions, client):
    """
    Test that complete works for labels.
    """
    # Arrange
    query = {"q": q, "limit": 20}

    # Act
    response = client.get("/complete", query_string=query)

    # Assert
    assert [s["value"] for s in response.json["typeahead"]] == expected_suggestions
