import pytest


LABEL_NYSTARTSJOBB = 'NYSTARTSJOBB'.lower()

@pytest.mark.parametrize("query, expected, contextual", [
    ('nys', [LABEL_NYSTARTSJOBB], True),
    ('nysta', [LABEL_NYSTARTSJOBB], True),
    ('#nysta', [LABEL_NYSTARTSJOBB], True),
    ('nys', [LABEL_NYSTARTSJOBB], False),
    ('nysta', [LABEL_NYSTARTSJOBB], False),
    ('#nysta', [LABEL_NYSTARTSJOBB], False),
])
def test_complete_for_label_contextual( query, expected, contextual, client):
    """
    Testing complete for labels.
    """
    response = client.get("/complete", query_string={'q': query, 'contextual': contextual})
    actual = [item['value'] for item in response.json['typeahead']]

    assert sorted(actual) == sorted(expected), f"Lists do not match.\nActual list: {actual}\nExpected list: {expected}"

