import pytest  # noqa: F401

from common.constants import MAX_COMPLETE_LIMIT


def test_check_400_bad_request_when_limit_is_greater_than_allowed(client):
    """
    Test that a limit of 51 will give a '400 BAD REQUEST' response with a meaningful error message
    """
    # Arrange
    query = {"q": "x", "limit": MAX_COMPLETE_LIMIT + 1}

    # Act
    response = client.get("/complete", query_string=query)

    # Assert
    assert response.status_code == 400

    assert response.json["errors"]["limit"] == "Invalid argument: 51. argument must be within the range 0 - 50"
    assert response.json["message"] == "Input payload validation failed"


def test_check_400_bad_request_when_limit_is_negative(client):
    """
    Test that a limit of -1 will give a '400 BAD REQUEST' response with a meaningful error message
    """
    # Arrange
    query = {"q": "x", "limit": -1}

    # Act
    response = client.get("/complete", query_string=query)

    # Assert
    assert response.status_code == 400

    assert response.json["errors"]["limit"] == "Invalid argument: -1. argument must be within the range 0 - 50"
    assert response.json["message"] == "Input payload validation failed"
