import pytest


@pytest.mark.smoke
@pytest.mark.parametrize(
    "q",
    [
        "Systemutvecklare / Programmerare",
        "v[rd o",
        "kock) l" '"jobba på',
        "Vitvaror & Malmö",
        "M] l",
        "på distans ",
        "v< undersköterska < ",
        "kjell ! co",
        "< undersköterska",
        " < undersköterska",
        "Djurtekniker ( 1-2 st ",
        "Djurtekniker ( 1-2 st",
        "Djurtekniker [1-2 st ",
        "Lokalvård / Städare (med B-körkort",
        "M) l",
        "kjell & co",
    ],
)
def test_error_nar_862_b(q, client):
    """
    Test that queries that used to give 500 errors now return status code 200
    """
    # Arrange
    query = {"q": q, "limit": 0}

    # Act
    response = client.get("/complete", query_string=query)

    # Assert
    assert response.status_code == 200


def test_error_nar_1552(client):
    """
    'sverige' or 'svenska' should not show up as suggestions as second word after first word + space
    """
    # Arrange
    query = {"q": "sjuksköterska "}

    # Act
    response = client.get("/complete", query_string=query)

    # Assert
    assert all(not suggestion["value"] in ["sverige", "svenska"] for suggestion in response.json["typeahead"])


def test_error_nar_1552_b(client):
    """
    'sverige' or 'svenska' may  show up as suggestions as second word if matching
    """
    # Arrange
    query = {"q": "sjuksköterska s"}

    # Act
    response = client.get("/complete", query_string=query)

    assert "sverige" in response.json["typeahead"][0]["value"]
    assert "svenska" in response.json["typeahead"][1]["value"]
