import pytest
import logging

log = logging.getLogger(__name__)


@pytest.mark.parametrize("query, expected", [
  ("", ['sverige', 'svenska', 'stockholms län', 'körkort', 'engelska', 'stockholm', 'västra götaland',
  'västra götalands län', 'skåne', 'skåne län']),
  (" ", ['sverige', 'svenska', 'stockholms län', 'körkort', 'engelska', 'stockholm', 'västra götaland',
   'västra götalands län', 'skåne', 'skåne län']),
  ("\t", ['sverige', 'svenska', 'stockholms län', 'körkort', 'engelska', 'stockholm', 'västra götaland',
  'västra götalands län', 'skåne', 'skåne län']),
  ("qwerty", []),
  ("qwerty ",
   ['qwerty stockholms län', 'qwerty körkort', 'qwerty engelska', 'qwerty stockholm', 'qwerty västra götaland',
  'qwerty västra götalands län', 'qwerty skåne', 'qwerty skåne län', 'qwerty b-körkort', 'qwerty sjuksköterska']
   ),
])
def test_complete_basic_tests( query, expected, client):
  """
  Testing basic & extreme cases (including whitespaces)
  """
  response = client.get("/complete", query_string={'q': query, 'contextual': False})
  actual = [item['value'] for item in response.json['typeahead']]
  log.debug(f"actual: {actual}")

  assert sorted(actual) == sorted(expected), f"Lists do not match.\nActual list: {actual}\nExpected list: {expected}"


@pytest.mark.parametrize("query, expected", [
  ('systemutvecklare angular',
   ['systemutvecklare angularjs', 'systemutvecklare angular.js']),
  ('#coro', ['corona', 'corona helsingborg', 'corona skåne', 'corona skåne län']),
  ('#coron', ['corona', 'corona helsingborg', 'corona skåne', 'corona skåne län']),
  ('c',
   ['civilingenjör', 'cloud', 'c#', 'coachning', 'c++', 'chaufför', 'cad', 'ci/cd', 'c-körkort', 'css', 'compliance',
  'crm', 'chaufförer', 'customer service', 'certifikat', 'ce-körkort', 'civilingenjörsutbildning', 'configuration',
  'controller', 'ci', 'continuous integration', 'chefserfarenhet', 'civilingenjörsexamen', 'cosmic',
  'cnc- operatör', 'cnc-operatör', 'coordinator', 'ce-chaufför', 'cafébiträde', 'customer advisor', 'c-chaufför',
  'chief information officer', 'cio', 'co', 'coach', 'cypern', 'c++-utvecklare', 'c++ developer', 'c-chaufförer',
  'ce chaufför', 'ce-chaufförer', 'charlottenberg']),
  ('uppd', ['uppdragsledning', 'uppdragsutbildning', 'uppdragsutbildningar', 'uppdragsansvarig', 'uppdragsledare',
  'uppdragsanalyser', 'uppdragsansvar', 'uppdragsledarerfarenhet', 'uppdragsverksamhet', 'uppdukning']),
  ('underh',
   ['underhållsarbete', 'underhållsmekaniker', 'underhållning', 'underhållsarbeten', 'underhållsplaner',
  'underhållsprojekt', 'underhållssystem', 'underhållsingenjör', 'underhållstekniker', 'underhåll och reparation',
  'underhållsfrågor', 'underhållsteknik', 'underhållsavdelning', 'underhållsplanering', 'underhållsverksamhet',
  'underhållsanalytiker', 'underhållsansvarig', 'underhållschef', 'underhållsledare', 'underhållsansvar',
  'underhållsberedning', 'underhållsbesiktning', 'underhållsbesiktningar', 'underhållsinsatser',
  'underhållsinstruktioner', 'underhållsrutiner', 'underhållsstrategi', 'underhållare', 'underhållsmontör',
  'underhållsplanerare']
   ),
  ('sjuks',
   ['sjuksköterska', 'sjuksköterskor', 'sjuksköterskeuppgifter', 'sjuksköterskelegitimation', 'sjuksköterskeexamen',
  'sjuksköterskan', 'sjuksköterskearbete', 'sjuksköterskemottagning', 'sjuksköterskeutbildning',
  'sjukskrivningsfrågor', 'sjuksköterska inom psykiatri', 'sjuksköterskeassistent']),
  ('arbetsl',
   ['arbetslivserfarenhet', 'arbetsledning', 'arbetsledare', 'arbetsliv', 'arbetslivspsykologi', 'arbetslivsfrågor',
  'arbetslivserfarenheter', 'arbetslagsarbete', 'arbetsledaransvar', 'arbetsledarerfarenhet', 'arbetsledarskap',
  'arbetslivsinriktad rehabilitering', 'arbetslagsledare']),

])
def test_complete_endpoint_with_spellcheck_typeahead( query, expected, client):
  """
  test of /complete endpoint
  parameters: query and list of expected result(s)
  """
  response = client.get("/complete", query_string={'q': query, 'limit': 50, 'contextual': False})
  actual = [item['value'] for item in response.json['typeahead']]

  assert sorted(actual) == sorted(expected), f"Lists do not match.\nActual list: {actual}\nExpected list: {expected}"



@pytest.mark.parametrize("query, expected", [
  ("stor",
   ['storkök', 'storage', 'storhushåll', 'storstädning', 'storköksutbildning', 'stora datamängder', 'storstädningar',
  'storbritannien', 'storuman', 'storhushållsutbildning']

   ),
  ("stor s",
   ['stor sverige', 'stor svenska', 'stor stockholms län', 'stor stockholm', 'stor skåne', 'stor skåne län',
  'stor sjuksköterska', 'stor sjukvård', 'stor säljare', 'stor systemutvecklare']),
  ("storag",
   ['storage', 'storage stockholm', 'storage stockholms län', 'storage göteborg', 'storage västra götaland',
  'storage västra götalands län']),
  ("storage s",
   ['storage sverige', 'storage svenska', 'storage stockholms län', 'storage stockholm', 'storage skåne',
  'storage skåne län', 'storage sjuksköterska', 'storage sjukvård', 'storage säljare', 'storage systemutvecklare']
   ),
])
def test_complete_multiple_words( query, expected, client):
  response = client.get("/complete", query_string={'q': query, 'contextual': False})
  actual = [item['value'] for item in response.json['typeahead']]

  assert sorted(actual) == sorted(expected), f"Lists do not match.\nActual list: {actual}\nExpected list: {expected}"




@pytest.mark.parametrize("query, expected", [
  ("götteborg sjuksköterska",
   ['götteborg sjuksköterskan', 'götteborg sjuksköterska inom psykiatri']
   ),
  ("götteborg sjukssköterska",
   ['göteborg sjuksköterska', 'göteborg sjuksköterskan', 'götteborg sjuksköterska', 'göteborg sjukssköterska', 'götteborg sjuksköterskan']),
  ("göteborg sjukssköterska", ['göteborg sjuksköterska', 'göteborg sjuksköterskan']),
  ("götteborg sjukssköterska",
     ['göteborg sjuksköterska', 'göteborg sjuksköterskan', 'götteborg sjuksköterska', 'göteborg sjukssköterska', 'götteborg sjuksköterskan']),
    ("göteborg sjukssköterska", ['göteborg sjuksköterska', 'göteborg sjuksköterskan']),
  ("sundsvall sjukssköterska läckare",
   ['sundsvall sjuksköterska lärare', 'sundsvall sjuksköterska läkare', 'sundsvall sjuksköterskan lärare',
    'sundsvall sjuksköterska lättare', 'sundsvall sjuksköterska packare', 'sundsvall sjuksköterska plockare',
    'sundsvall sjukssköterska lärare', 'sundsvall sjuksköterska läckare', 'sundsvall sjuksköterskan läkare',
    'sundsvall sjukssköterska läkare']),
  ("piteå sjukssköterska läkkare",
   ['piteå sjuksköterska lärare', 'piteå sjuksköterska läkare', 'piteå sjuksköterska väktare',
    'piteå sjuksköterskan lärare', 'piteå sjuksköterska mäklare', 'piteå sjuksköterska lättare',
    'piteå sjukssköterska lärare', 'piteå sjuksköterska läkkare', 'piteå sjuksköterskan läkare',
    'piteå sjukssköterska läkare']),
  ("växjö sjukssköterska lääkare",
   ['växjö sjuksköterska lärare', 'växjö sjuksköterska läkare', 'växjö sjuksköterskan lärare',
    'växjö sjuksköterska lättare', 'växjö sjukssköterska lärare', 'växjö sjuksköterska lääkare',
    'växjö sjuksköterskan läkare', 'växjö sjukssköterska läkare', 'växjö sjuksköterskan lättare',
    'växjö sjuksköterskan lääkare']),
  ("uppsala sjukssköterska läkare", ['uppsala sjuksköterska läkare', 'uppsala sjuksköterskan läkare']),
  ("läckare", ['läkare', 'läkare stockholms län', 'läkare skåne', 'läkare skåne län', 'läkare stockholm',
   'läkare jönköpings län']),
  ("götteborg", ['göteborg', 'göteborg systemutvecklare', 'göteborg sjuksköterska', 'göteborg civilingenjör',
   'göteborg mjukvaruutvecklare', 'göteborg programmerare', 'göteborg personlig assistent']
   ),
  ("stokholm", ['stockholms län', 'stockholm', 'stockholm city']),
  ("stockhlm", ['stockholms län', 'stockholm', 'stockholm city']),
  ("stokholm lärarre",
   ['stockholms lärande', 'stockholms lärare', 'stockholms läraren', 'stockholm lärande', 'stockholms läkare',
    'stockholm lärare', 'stockholm läraren', 'stockholm läkare', 'stockholms bärare', 'stockholm bärare']
   ),
  ("karlstad sjukssköterska läckare",
   ['karlstad sjuksköterska läkare', 'karlstad sjuksköterskan läkare', 'karlstad sjukssköterska läkare',
    'karlstad sjuksköterska lärare', 'karlstad sjuksköterskan lärare', 'karlstad sjuksköterska packare',
    'karlstad sjuksköterska plockare', 'karlstad sjukssköterska lärare', 'karlstad sjuksköterska lättare',
    'karlstad sjuksköterska läckare']
   ),
  ("malmö läckare sjukssköterska ",
   ['malmö läckare sjukssköterska stockholms län', 'malmö läckare sjukssköterska körkort',
  'malmö läckare sjukssköterska engelska', 'malmö läckare sjukssköterska stockholm',
  'malmö läckare sjukssköterska västra götaland', 'malmö läckare sjukssköterska västra götalands län',
  'malmö läckare sjukssköterska skåne', 'malmö läckare sjukssköterska skåne län',
  'malmö läckare sjukssköterska b-körkort', 'malmö läckare sjukssköterska sjuksköterska']

   ),
  ("kiruna läckare sjukssköterska",
   ['kiruna lärare sjuksköterska', 'kiruna läkare sjuksköterska', 'kiruna lärare sjuksköterskan',
    'kiruna lättare sjuksköterska', 'kiruna packare sjuksköterska', 'kiruna plockare sjuksköterska',
    'kiruna lärare sjukssköterska', 'kiruna läckare sjuksköterska', 'kiruna läkare sjuksköterskan',
    'kiruna läkare sjukssköterska']
   ),
  ("läckare götteborg",
   ['lärare göteborg', 'läkare göteborg', 'lättare göteborg', 'packare göteborg', 'plockare göteborg',
  'lärare götteborg', 'läckare göteborg', 'läkare götteborg', 'lättare götteborg', 'packare götteborg']
   ),
  ("läckare göteborg",
   ['läkare göteborg', 'lärare göteborg', 'lättare göteborg', 'packare göteborg', 'plockare göteborg']),
  ("stockholm läckare malmö", ['stockholm läkare malmö',
    'stockholm lärare malmö',
    'stockholm lättare malmö',
    'stockholm packare malmö',
    'stockholm plockare malmö']),
  ("stockhlm läckare malmö",
   ['stockholm lärare malmö', 'stockholm lättare malmö', 'stockholm läkare malmö', 'stockholms lättare malmö',
    'stockholms lärare malmö', 'stockholms läkare malmö', 'stockholms packare malmö', 'stockholms plockare malmö',
    'stockholm packare malmö', 'stockholm plockare malmö']),
])
def test_complete_spelling_correction_multiple_words(query, expected, client):
  """
  Test typeahead with multiple (misspelled) words
  """
  # TODO: It's a strange behaviour to correct all the words in the query if only the last word is misspelled.
  #  It would make more sense to only correct the last misspelled word, and not to correct all of the (previous) words.
  response = client.get("/complete", query_string={'q': query, 'contextual': False})
  actual = [item['value'] for item in response.json['typeahead']]
  log.debug(f"actual: {actual}")

  assert sorted(actual) == sorted(expected), f"Lists do not match.\nActual list: {actual}\nExpected list: {expected}"


@pytest.mark.parametrize("query, expected", [
  ("stokholm ",
   ['stokholm stockholms län', 'stokholm körkort', 'stokholm engelska', 'stokholm stockholm',
  'stokholm västra götaland', 'stokholm västra götalands län', 'stokholm skåne', 'stokholm skåne län',
  'stokholm b-körkort', 'stokholm sjuksköterska']

   ),  # trailing space
  ("stokholm  ",
   ['stokholm stockholms län', 'stokholm körkort', 'stokholm engelska', 'stokholm stockholm',
  'stokholm västra götaland', 'stokholm västra götalands län', 'stokholm skåne', 'stokholm skåne län',
  'stokholm b-körkort', 'stokholm sjuksköterska']

   ),  # 2 trailing spaces
  ("stockhlm ",
   ['stockhlm stockholms län', 'stockhlm körkort', 'stockhlm engelska', 'stockhlm stockholm',
  'stockhlm västra götaland', 'stockhlm västra götalands län', 'stockhlm skåne', 'stockhlm skåne län',
  'stockhlm b-körkort', 'stockhlm sjuksköterska']

   ),  # trailing space
  (" stockhlm ",
   ['stockhlm stockholms län', 'stockhlm körkort', 'stockhlm engelska', 'stockhlm stockholm',
  'stockhlm västra götaland', 'stockhlm västra götalands län', 'stockhlm skåne', 'stockhlm skåne län',
  'stockhlm b-körkort', 'stockhlm sjuksköterska']

   ),  # leading + trailing spaces
  ("stockhlm läckare göteborg ", ['stockhlm läckare göteborg stockholms län', 'stockhlm läckare göteborg körkort',
    'stockhlm läckare göteborg engelska', 'stockhlm läckare göteborg stockholm',
    'stockhlm läckare göteborg västra götaland',
    'stockhlm läckare göteborg västra götalands län', 'stockhlm läckare göteborg skåne',
    'stockhlm läckare göteborg skåne län', 'stockhlm läckare göteborg b-körkort',
    'stockhlm läckare göteborg sjuksköterska']

   ),  # trailing space
  ("stockhlm läckare göteborg  ", ['stockhlm läckare göteborg stockholms län', 'stockhlm läckare göteborg körkort',
     'stockhlm läckare göteborg engelska', 'stockhlm läckare göteborg stockholm',
     'stockhlm läckare göteborg västra götaland',
     'stockhlm läckare göteborg västra götalands län',
     'stockhlm läckare göteborg skåne', 'stockhlm läckare göteborg skåne län',
     'stockhlm läckare göteborg b-körkort', 'stockhlm läckare göteborg sjuksköterska']

   ),  # 2 trailing space
])
def test_complete_spelling_correction_multiple_words_and_trailing_space(query, expected, client):
  """
  Test typeahead with multiple (misspelled) words
  """
  response = client.get("/complete", query_string={'q': query, 'contextual': False})
  actual = [item['value'] for item in response.json['typeahead']]

  assert sorted(actual) == sorted(expected), f"Lists do not match.\nActual list: {actual}\nExpected list: {expected}"




@pytest.mark.parametrize("query, expected", [
  ("läckare", ['läkare', 'läkare stockholms län', 'läkare skåne', 'läkare skåne län', 'läkare stockholm',
   'läkare jönköpings län']),
  ("götteborg", ['göteborg', 'göteborg systemutvecklare', 'göteborg sjuksköterska', 'göteborg civilingenjör',
   'göteborg mjukvaruutvecklare', 'göteborg programmerare', 'göteborg personlig assistent']
   ),
  ("stokholm", ['stockholms län', 'stockholm', 'stockholm city']),
  ("stokholm ",
   ['stokholm stockholms län', 'stokholm körkort', 'stokholm engelska', 'stokholm stockholm',
  'stokholm västra götaland', 'stokholm västra götalands län', 'stokholm skåne', 'stokholm skåne län',
  'stokholm b-körkort', 'stokholm sjuksköterska']
   ),  # trailing space
  ("stokholm  ",
   ['stokholm stockholms län', 'stokholm körkort', 'stokholm engelska', 'stokholm stockholm',
  'stokholm västra götaland', 'stokholm västra götalands län', 'stokholm skåne', 'stokholm skåne län',
  'stokholm b-körkort', 'stokholm sjuksköterska']
   ),  # 2 trailing spaces
  ("stockhlm", ['stockholms län', 'stockholm', 'stockholm city']),
  ("stockhlm ",
   ['stockhlm stockholms län', 'stockhlm körkort', 'stockhlm engelska', 'stockhlm stockholm',
  'stockhlm västra götaland', 'stockhlm västra götalands län', 'stockhlm skåne', 'stockhlm skåne län',
  'stockhlm b-körkort', 'stockhlm sjuksköterska']
   ),  # trailing space
  (" stockhlm ",
   ['stockhlm stockholms län', 'stockhlm körkort', 'stockhlm engelska', 'stockhlm stockholm',
  'stockhlm västra götaland', 'stockhlm västra götalands län', 'stockhlm skåne', 'stockhlm skåne län',
  'stockhlm b-körkort', 'stockhlm sjuksköterska']

   ),  # leading + trailing spaces
  ("stockholm pythan", ['stockholm python', 'stockholm python-utvecklare']),
  ("läkare götteborg", ['läkare göteborg']),
  ("läkare götteborg", ['läkare göteborg']),
])
def test_spelling_correction_with_complete_suggest( query, expected, client):
  """
  Test typeahead with only one (misspelled) word
  """
  response = client.get("/complete", query_string={'q': query, 'contextual': False})
  actual = [item['value'] for item in response.json['typeahead']]

  assert sorted(actual) == sorted(expected), f"Lists do not match.\nActual list: {actual}\nExpected list: {expected}"

@pytest.mark.parametrize("query, expected", [
  ("python ",
   ['python stockholms län', 'python körkort', 'python engelska', 'python stockholm', 'python västra götaland',
  'python västra götalands län', 'python skåne', 'python skåne län', 'python b-körkort', 'python sjuksköterska']
   ),
  ("python  ",
   ['python stockholms län', 'python körkort', 'python engelska', 'python stockholm', 'python västra götaland',
  'python västra götalands län', 'python skåne', 'python skåne län', 'python b-körkort', 'python sjuksköterska']

   ),  # 2 trailing spaces
  ("läkare ",
   ['läkare stockholms län', 'läkare körkort', 'läkare engelska', 'läkare stockholm', 'läkare västra götaland',
  'läkare västra götalands län', 'läkare skåne', 'läkare skåne län', 'läkare b-körkort', 'läkare sjuksköterska']

   ),
  (" läkare ",
   ['läkare stockholms län', 'läkare körkort', 'läkare engelska', 'läkare stockholm', 'läkare västra götaland',
  'läkare västra götalands län', 'läkare skåne', 'läkare skåne län', 'läkare b-körkort', 'läkare sjuksköterska']

   ),  # leading + trailing spaces
    ("snickare jönköpings lä",
     ['snickare jönköpings lärare', 'snickare jönköpings lärare i grundskolan', 'snickare jönköpings län',
      'snickare jönköpings lärarlegitimation', 'snickare jönköpings läkare', 'snickare jönköpings lärare i gymnasiet',
      'snickare jönköpings lärare i förskola', 'snickare jönköpings lärare i gymnasieskolan',
      'snickare jönköpings lärare i fritidshem', 'snickare jönköpings läkemedel']
     ),
])
def test_remove_same_word_function( query, expected, client):
  """
  Test when input is word with space, the result will not show the same word
  """
  response = client.get("/complete", query_string={'q': query, 'contextual': False})
  actual = [item['value'] for item in response.json['typeahead']]
  log.debug(f"actual: {actual}")

  assert sorted(actual) == sorted(expected), f"Lists do not match.\nActual list: {actual}\nExpected list: {expected}"


@pytest.mark.parametrize("query, expected", [
    ("stockholms lä",
     ['stockholms län', 'stockholms lärare', 'stockholms lärare i grundskolan', 'stockholms lärarlegitimation',
      'stockholms läkare', 'stockholms lärare i gymnasiet', 'stockholms lärare i förskola',
      'stockholms lärare i gymnasieskolan', 'stockholms lärare i fritidshem', 'stockholms läkemedel']
     ),
    ("stockholms län jönköpings lä",
     ['stockholms län jönköpings lärare', 'stockholms län jönköpings lärare i grundskolan',
      'stockholms län jönköpings län', 'stockholms län jönköpings lärarlegitimation', 'stockholms län jönköpings läkare',
      'stockholms län jönköpings lärare i gymnasiet', 'stockholms län jönköpings lärare i förskola',
      'stockholms län jönköpings lärare i gymnasieskolan', 'stockholms län jönköpings lärare i fritidshem',
      'stockholms län jönköpings läkemedel']
     ), # Test to see that 'stockholms län jönköpings län' (previously 'län' was removed so only 'stockholms län jönköpings' was returned
    ("stockholms bildemontering ",
     ['stockholms bildemontering stockholms län', 'stockholms bildemontering körkort',
      'stockholms bildemontering engelska', 'stockholms bildemontering stockholm',
      'stockholms bildemontering västra götaland', 'stockholms bildemontering västra götalands län',
      'stockholms bildemontering skåne', 'stockholms bildemontering skåne län', 'stockholms bildemontering b-körkort',
      'stockholms bildemontering sjuksköterska']
     ), # Test to see that 'stockholms bildemontering stockholms län' is returned
    ("blekinge län bildemontering ",
     ['blekinge län bildemontering stockholms län', 'blekinge län bildemontering körkort',
      'blekinge län bildemontering engelska', 'blekinge län bildemontering stockholm',
      'blekinge län bildemontering västra götaland', 'blekinge län bildemontering västra götalands län',
      'blekinge län bildemontering skåne', 'blekinge län bildemontering b-körkort',
      'blekinge län bildemontering sjuksköterska', 'blekinge län bildemontering göteborg']
     ), # Test to see that 'stockholms bildemontering stockholms län' is returned
])
def test_handle_duplicate_words( query, expected, client):
    """
    Test when input is word with space, the result will not show the same word
    """
    response = client.get("/complete", query_string={'q': query, 'contextual': False})
    actual = [item['value'] for item in response.json['typeahead']]
    log.debug(f"actual: {actual}")

    assert sorted(actual) == sorted(expected), f"Lists do not match.\nActual list: {actual}\nExpected list: {expected}"
