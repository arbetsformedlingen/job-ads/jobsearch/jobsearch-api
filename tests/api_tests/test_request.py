import pytest  # noqa F401


def test_request(client):
    # Act
    response = client.get("/")

    # Assert
    assert response.status_code == 200

    assert response.headers["Content-Type"] == "text/html; charset=utf-8"
