import pytest


@pytest.mark.parametrize("ad_id, expected", [(23448227, 1), (23699999, 3), (23791205, 1), (23837044, 1), (23910624, 1)])
def test_verify_facklig_representant_not_in_ads(ad_id, expected, client):
    """
    get ads which had union representatives in LA mock data (should not be imported)
    and check field 'application_contact'
    """
    # Act
    response = client.get(f"/ad/{ad_id}")

    # Assert
    assert len(response.json["application_contacts"]) == expected

    # Verify that the string "facklig representant" is not in the application_contacts values
    assert "facklig representant" not in str(response.json["application_contacts"])
