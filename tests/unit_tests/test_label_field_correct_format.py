from unittest.mock import patch

import pytest

from search.rest.endpoint.jobsearch import AdById


@patch("search.rest.endpoint.jobsearch.fetch_ad_by_id")
def test_label_field_returned_as_list(mock_fetch_ad_by_id):
    # Arrange
    mock_fetch_ad_by_id.return_value = {"label": ["test"]}
    endpoint = AdById()

    # Act
    result = endpoint.get(123456789)

    # Assert
    assert isinstance(result["label"], list)
    assert result["label"] == ["test"]
