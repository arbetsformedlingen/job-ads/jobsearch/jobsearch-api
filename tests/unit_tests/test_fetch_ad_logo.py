from unittest.mock import patch

import pytest
import requests
from werkzeug.exceptions import ServiceUnavailable

from search.common_search.logo import fetch_ad_logo, file_formatter, get_correct_logo_url


@patch("search.common_search.logo.fetch_ad_by_id")
def test_get_correct_logo_url_for_ad_is_none(mock_fetch_ad_by_id):
    """
    Test that get_correct_logo_url function handles None values correctly
    """
    mock_fetch_ad_by_id.return_value = {}

    result = get_correct_logo_url(None)

    assert result is None


def test_get_correct_logo_url_with_valid_logo_url():
    """
    Test get_correct_logo_url function when the ad has a value for logo_url.
    """
    ad = {"logo_url": "https://example.com/logo.png"}
    result = get_correct_logo_url(ad)
    assert result is not None
    assert result == "https://example.com/logo.png"


def test_get_correct_logo_url_with_missing_logo_url():
    """
    Test get_correct_logo_url function when the ad is missing a value for logo_url.
    """
    ad = {}
    result = get_correct_logo_url(ad)
    assert result is None


@patch("search.common_search.logo.io.BytesIO")
@patch("search.common_search.logo.send_file")
def test_file_formatter_calls_send_file(mock_send_file, MockBytesIO):
    # Arrange
    MockBytesIO.return_value = "mock BytesIO object"

    # Act
    data = file_formatter("mock file object")

    # Assert
    MockBytesIO.assert_called_with("mock file object")
    mock_send_file.assert_called_with("mock BytesIO object", download_name="logo.png", mimetype="image/png")


@patch("search.common_search.logo.settings.COMPANY_LOGO_CERT", "path/to/cert")
@patch("search.common_search.logo.file_formatter")
@patch("search.common_search.logo.requests.get")
@patch("search.common_search.logo.fetch_ad_by_id")
def test_fetch_ad_logo_from_url_returns_file_as_image(mock_fetch_ad_by_id, mock_requests_get, mock_file_formatter):
    # Arrange
    mock_fetch_ad_by_id.return_value = {"id": "24373453", "logo_url": "https://example.com/logo.png"}

    mock_requests_get.return_value.status_code = 200
    mock_requests_get.return_value.raw.read.return_value = "logo bytes"

    mock_file_formatter.return_value = "logo bytes formatted as image file"

    # Act
    result = fetch_ad_logo("24373453")

    # Assert
    mock_fetch_ad_by_id.assert_called_with("24373453")
    mock_requests_get.assert_called_once_with(
        "https://example.com/logo.png",
        stream=True,
        timeout=5,
        proxies={"http": None, "https": None},
        verify="path/to/cert",
    )

    mock_file_formatter.assert_called_with("logo bytes")

    assert result == "logo bytes formatted as image file"


@patch("search.common_search.logo.file_formatter")
@patch("search.common_search.logo.get_not_found_logo_file")
@patch("search.common_search.logo.fetch_ad_by_id")
def test_fetch_ad_logo_for_missing_logo(mock_fetch_ad_by_id, mock_get_not_found_logo_file, mock_file_formatter):
    # Arrange
    mock_fetch_ad_by_id.return_value = {"id": "24373453", "logo_url": None}

    mock_get_not_found_logo_file.return_value = "not found logo bytes"

    mock_file_formatter.return_value = "not found logo bytes formatted as image file"

    # Act
    result = fetch_ad_logo("24373453")

    # Assert
    mock_fetch_ad_by_id.assert_called_with("24373453")

    mock_get_not_found_logo_file.assert_called_once()

    mock_file_formatter.assert_called_with("not found logo bytes")

    assert result == "not found logo bytes formatted as image file"


@patch("search.common_search.logo.settings.COMPANY_LOGO_CERT", "path/to/cert")
@patch("search.common_search.logo.requests.get")
@patch("search.common_search.logo.fetch_ad_by_id")
def test_fetch_ad_logo_from_url_raises_error_for_exceptions_thrown_by_requests(
    mock_fetch_ad_by_id, mock_requests_get, caplog
):
    # Arrange
    mock_fetch_ad_by_id.return_value = {"id": "24373453", "logo_url": "https://example.com/logo.png"}

    mock_requests_get.side_effect = requests.exceptions.RequestException("Test Error")

    # Act
    with pytest.raises(ServiceUnavailable) as error:
        fetch_ad_logo("24373453")

    # Assert
    mock_fetch_ad_by_id.assert_called_with("24373453")

    mock_requests_get.assert_called_once_with(
        "https://example.com/logo.png",
        stream=True,
        timeout=5,
        proxies={"http": None, "https": None},
        verify="path/to/cert",
    )

    assert str(error.value) == "503 Service Unavailable: Error getting logo for id 24373453"

    assert "Error for logo url https://example.com/logo.png: Test Error" in caplog.messages


@patch("search.common_search.logo.settings.COMPANY_LOGO_CERT", "path/to/cert")
@patch("search.common_search.logo.requests.get")
@patch("search.common_search.logo.fetch_ad_by_id")
def test_fetch_ad_logo_from_url_rethrows_error_for_status_code_404(mock_fetch_ad_by_id, mock_requests_get, caplog):
    # Arrange
    mock_fetch_ad_by_id.return_value = {"id": "24373453", "logo_url": "https://example.com/logo.png"}

    mock_requests_get.return_value.status_code = 404
    mock_requests_get.return_value.raise_for_status.side_effect = requests.exceptions.HTTPError("Test Error")

    # Act
    with pytest.raises(requests.exceptions.HTTPError) as error:
        fetch_ad_logo("24373453")

    # Assert
    mock_fetch_ad_by_id.assert_called_with("24373453")

    mock_requests_get.assert_called_once_with(
        "https://example.com/logo.png",
        stream=True,
        timeout=5,
        proxies={"http": None, "https": None},
        verify="path/to/cert",
    )

    assert str(error.value) == "Test Error"

    assert "Status code 404 for https://example.com/logo.png" in caplog.messages
