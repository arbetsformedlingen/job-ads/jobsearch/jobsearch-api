from unittest.mock import patch

import pytest  # noqa: F401


@patch("search.common_search.logo.fetch_ad_by_id")
@patch("search.common_search.logo.get_correct_logo_url")
@patch("search.common_search.logo.requests.get")
def test_fetch_ad_logo_success(mock_requests_get, mock_get_correct_logo_url, mock_fetch_ad_by_id, client):
    # Arrange
    mock_fetch_ad_by_id.return_value = {"ad_id": 123, "logo_url": "http://example.com/logo.png"}
    mock_get_correct_logo_url.return_value = "http://example.com/logo.png"
    mock_requests_get.return_value.status_code = 200
    binary_data = b"fake_logo_data"
    mock_requests_get.return_value.raw.read.return_value = binary_data

    # Act
    response = client.get("/ad/123/logo")

    # Assert
    assert response.status_code == 200
    assert response.content_length == len(binary_data)
    assert response.content_type == "image/png"
