import pytest
from tests.unit_tests.test_resources.mock_for_querybuilder_tests import all_query_builders
from search.common_search.ad_search import transform_ad_search_stats_result
from tests.test_resources.stats import region_stats, region_stats_result, municipality_stats, municipality_stats_result, \
    country_stats, country_stats_result, occupation_field_stats, occupation_field_stats_result, occupation_group_stats, \
    occupation_group_stats_result, occupation_name_stats, occupation_name_stats_result


@pytest.mark.parametrize("mock_query_builder", all_query_builders)
@pytest.mark.parametrize("args, query_result, stats_result", [
    ({'stats': ['occupation-name']}, occupation_name_stats, occupation_name_stats_result),
    ({'stats': ['occupation-group']}, occupation_group_stats, occupation_group_stats_result),
    ({'stats': ['occupation-field']}, occupation_field_stats, occupation_field_stats_result),
    ({'stats': ['country']}, country_stats, country_stats_result),
    ({'stats': ['municipality']}, municipality_stats, municipality_stats_result),
    ({'stats': ['region']}, region_stats, region_stats_result)])
def test_stats_transform_platsannons_query_result(args, query_result, stats_result, mock_query_builder):
    result = transform_ad_search_stats_result(args, query_result, mock_query_builder, {})
    print(stats_result)
    assert result.get('stats', stats_result) == stats_result
