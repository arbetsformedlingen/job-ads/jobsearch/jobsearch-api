from flask import Flask
from search.common_search.logo import file_formatter


def test_flask_send_file():
    """
    Tests that the response from the send_file() function is as expected
    and that no parameter names have changed
    """
    app = Flask(__name__)
    with app.test_request_context():
        binary_data = b'testing the send_file() function in Flask'
        response = file_formatter(binary_data)
        assert response.status == '200 OK'
        assert response.content_length == len(binary_data)
        assert response.content_type == 'image/png'
        for header_type, header_content in response.headers:
            if header_type == 'Content-Disposition':
                assert header_content == 'inline; filename=logo.png'
            elif header_type == 'Content-Type':
                assert header_content == 'image/png'
            elif header_type == 'Content-Length':
                assert header_content == str(len(binary_data))
