from search.common_search.helpers import clean_plus_minus


def test_clean_plus_minus():
    """
    + and - signs at the beginning of words are removed, those at the end or in the middle are not removed
    """
    cleaned_text = clean_plus_minus(
        '-mållare målare +undersköterska java-utvecklare -key account manager c-sharp -java -noggrann flexibel')
    assert cleaned_text == 'mållare målare undersköterska java-utvecklare key account manager c-sharp java noggrann flexibel'


def test_clean_plus_minus2():
    """
    + and - signs at the beginning of words are removed, those at the end or in the middle are not removed
    """
    cleaned_text = clean_plus_minus(
        'mållare- målare undersköterska+ java-utvecklare key- account manager c-sharp -java- -noggrann- flexibel')
    assert cleaned_text == 'mållare- målare undersköterska+ java-utvecklare key- account manager c-sharp java- noggrann- flexibel'
