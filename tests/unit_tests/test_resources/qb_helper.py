def _assert_json_structure(result, expected):
    return _walk_dictionary(result, expected)


def _walk_dictionary(result, expected):
    if isinstance(result, str) and isinstance(expected, str):
        return result == expected
    else:
        for item in expected:
            if item in result:
                if isinstance(result[item], list):
                    for listitem in result[item]:
                        if _walk_dictionary(listitem, expected[item]):
                            return True
                else:
                    return _walk_dictionary(result[item], expected[item])

        return False
