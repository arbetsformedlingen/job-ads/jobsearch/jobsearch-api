import pytest

from tests.unit_tests.test_resources.mock_for_querybuilder_tests import all_query_builders
from tests.unit_tests.test_resources import occupation_query_test_data as test_data


# this will run unit tests whenever api or integration tests are run, or when only unit tests are selected for test


@pytest.mark.parametrize("mock_query_builder", all_query_builders)
@pytest.mark.parametrize("occupation_list, expected", test_data.occupation_queries)
def test_occupation_name_1(mock_query_builder, occupation_list, expected):
    result = mock_query_builder._build_yrkes_query(occupation_list, None, None)
    assert result == expected


@pytest.mark.parametrize("mock_query_builder", all_query_builders)
@pytest.mark.parametrize("occupation_group_list, expected", test_data.occupation_group_queries)
def test_occupation_group(mock_query_builder, occupation_group_list, expected):
    result = mock_query_builder._build_yrkes_query(None, occupation_group_list, None)
    assert result == expected


@pytest.mark.parametrize("mock_query_builder", all_query_builders)
@pytest.mark.parametrize("occupation_field_list, expected", test_data.occupation_field_queries)
def test_occupation_field(mock_query_builder, occupation_field_list, expected):
    result = mock_query_builder._build_yrkes_query(None, None, occupation_field_list)
    assert result == expected


@pytest.mark.parametrize("mock_query_builder", all_query_builders)
@pytest.mark.parametrize("occupation_collections_list, expected", test_data.occupation_collections_queries)
def test_occupation_collections(mock_query_builder, occupation_collections_list, expected):
    result = mock_query_builder.build_yrkessamlingar_query(occupation_collections_list)
    assert result == expected


@pytest.mark.parametrize("mock_query_builder", all_query_builders)
@pytest.mark.parametrize("test_data", test_data.combined_queries)
def test_occupation_field_2(mock_query_builder, test_data):
    result = mock_query_builder._build_yrkes_query(test_data[0], test_data[1], test_data[2])
    assert result == test_data[3]


@pytest.mark.parametrize("type_error_param", [True, -1])
@pytest.mark.parametrize("mock_query_builder", all_query_builders)
def test_type_error_params(mock_query_builder, type_error_param):
    # "TypeError: <x> object is not iterable" is an expected error
    with pytest.raises(TypeError):
        mock_query_builder._build_yrkes_query(type_error_param, None, None)
