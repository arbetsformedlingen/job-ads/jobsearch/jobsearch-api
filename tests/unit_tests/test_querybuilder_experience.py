import pytest

from tests.unit_tests.test_resources.mock_for_querybuilder_tests import mock_querybuilder_jobsearch
from tests.test_resources.helper import is_dst


@pytest.mark.parametrize('value', [True, False])
def test_experience_values_jobsearch(value):
    args = {'experience': value}

    expected_time = 'now+2H/m' if is_dst() else 'now+1H/m'  # f"{expected_time}"


    mock_querybuilder = mock_querybuilder_jobsearch
    filter = [{'range': {'publication_date': {'lte': f"{expected_time}"}}},
              {'range': {'last_publication_date': {'gte': f"{expected_time}"}}},
              {'term': {'removed': False}}]

    expected_query_dsl = {'from': 0, 'size': 10, 'track_total_hits': True, 'track_scores': True, 'query': {
        'bool': {'must': [{'term': {'experience_required': value}}],
                 'filter': filter}},
                          'aggs': {'positions': {'sum': {'field': 'number_of_vacancies'}}},
                          'sort': ['_score', {'publication_date': 'desc'}]}

    result = mock_querybuilder.build_query(args)
    assert result == expected_query_dsl
