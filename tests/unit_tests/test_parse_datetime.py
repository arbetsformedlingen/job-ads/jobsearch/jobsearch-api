import pytest  # noqa: F401

from datetime import datetime
from common.helpers import parse_datetime


@pytest.mark.parametrize(
    "string_or_offset, expected",
    [
        ["2024-02-03T11:12:13", datetime(2024, 2, 3, 11, 12, 13)],
        ["2024-02-03T11:12", datetime(2024, 2, 3, 11, 12, 0)],
        ["2024-02-03", datetime(2024, 2, 3, 0, 0, 0)],
        ["13", datetime(2024, 6, 24, 16, 33, 7)],
        ["2024", datetime(2024, 6, 23, 7, 2, 7)],  # Parse as timedelta in minutes
    ],
)
def test_parse_datetime(string_or_offset, expected):
    def now_fn():
        return datetime(2024, 6, 24, 16, 46, 7)

    assert parse_datetime(string_or_offset, now_fn=now_fn) == expected
