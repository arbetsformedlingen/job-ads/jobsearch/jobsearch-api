# coding=utf-8
import pytest
from common.ad_formatter import format_one_ad
from tests.unit_tests.test_resources.ad_format_test_data import ad_before, ad_after_formatting


def test_full_ad():
    before = ad_before
    expected = ad_after_formatting
    formatted = format_one_ad(before)
    assert formatted == expected


def test_none():
    with pytest.raises(AttributeError):
        format_one_ad(None)
