import pytest

from search.common_search.complete import merge_missing_words, clean_unwanted_completions, is_multiple_words, \
    is_unwanted_suggested_word, remove_multiple_spaces


@pytest.mark.parametrize("input_string, expected_output", [
    ("This  is   an example    string.", "This is an example string."),
    ("No  multiple   spaces", "No multiple spaces"),
    ("   Leading and trailing   ", "Leading and trailing"),
    ("SingleWord", "SingleWord"),
    ("", ""),
])
def test_remove_multiple_spaces_removes_extra_spaces(input_string, expected_output):
    assert remove_multiple_spaces(input_string) == expected_output


@pytest.mark.parametrize("base_string, compare_string, expected_output", [
    ("stockholms län jönköpings", "jönköpings län", "stockholms län jönköpings län"),
    ("hello world", "world peace", "hello world peace"),
    ("one two", "three four", "one two three four"),
    ("", "empty string", "empty string"),
    ("no missing words", "no missing words", "no missing words"),
    ("a b c", "c d e", "a b c d e"),
    ("a b c", "b c d", "a b c d"),
])
def test_merge_missing_words(base_string, compare_string, expected_output):
    assert merge_missing_words(base_string, compare_string) == expected_output

@pytest.mark.parametrize("typeahead_query, result, expected_output", [
    ("query", {"aggs": [{"found_phrase": "query", "value": "query"}]}, []),
    ("query", {"aggs": [{"found_phrase": "query extra", "value": "query extra"}]}, [{"found_phrase": "query extra", "value": "query extra"}]),
    ("", {"aggs": [{"found_phrase": "", "value": ""}]}, []),
    ("query", {"aggs": []}, []),
])
def test_clean_unwanted_completions_removes_unwanted_completions(typeahead_query, result, expected_output):
    assert clean_unwanted_completions(typeahead_query, result) == expected_output

@pytest.mark.parametrize("text, expected_output", [
    ("multiple words", True),
    ("singleword", False),
    ("", False),
    ("   ", False),
    ("word1 word2 word3", True),
])
def test_is_multiple_words_checks_word_count(text, expected_output):
    assert is_multiple_words(text) == expected_output

@pytest.mark.parametrize("value, last_word, expected_output", [
    ("svenska", "", True),
    ("sverige", "*", True),
    # ("test", "sven", True), # FIXME: It's unclear if the function shoud return True or False in this case
    ("godkänt", "", False),
    ("godkänt", "*", False),
    ("", "*", False),
])
def test_is_unwanted_suggested_word_checks_unwanted_words(value, last_word, expected_output):
    assert is_unwanted_suggested_word(value, last_word) == expected_output

