import time


def is_dst():
    """
    Daylight Savings Time
    1 = DST right now
    0 = not DST right now
    """
    return time.localtime().tm_isdst
