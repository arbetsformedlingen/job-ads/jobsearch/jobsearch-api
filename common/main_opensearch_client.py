import logging

from jobtech.common.customlogging import configure_logging

from common import settings
from common.opensearch_connection_with_retries import create_opensearch_client_with_retry

configure_logging([__name__.split(".")[0]])
log = logging.getLogger(__name__)
log.info(logging.getLevelName(log.getEffectiveLevel()) + " log level activated")


class OpensearchClient(object):
    """
    Main opensearch client
    Created the first time a query search is made
    Can also be created beforehand by using launch()
    """

    def __init__(self):
        self.opensearch = None

    def launch(self, db_timeout=10):
        if self.opensearch is None:
            log.info("Creating main opensearch client")
            log.info(f"Using Opensearch node(s) at {', '.join(settings.DB_HOST)} (port {settings.DB_PORT})")
            log.info(f"Using Opensearch index {settings.ADS_ALIAS}")
            self.opensearch = create_opensearch_client_with_retry(
                settings.DB_HOST, settings.DB_PORT, settings.DB_USER, settings.DB_PWD, db_timeout
            )

    def __call__(self):
        if self.opensearch is None:
            self.launch()
        return self.opensearch


opensearch_client = OpensearchClient()
