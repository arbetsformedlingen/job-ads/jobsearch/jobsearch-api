import logging

from common import constants

log = logging.getLogger(__name__)


def configure_app(flask_app):
    flask_app.config.SWAGGER_UI_DOC_EXPANSION = constants.RESTPLUS_SWAGGER_UI_DOC_EXPANSION
    flask_app.config.RESTPLUS_VALIDATE = constants.RESTPLUS_VALIDATE
    flask_app.config.RESTPLUS_MASK_SWAGGER = constants.RESTPLUS_MASK_SWAGGER
    flask_app.config.ERROR_404_HELP = constants.RESTPLUS_ERROR_404_HELP


def initialize_app(flask_app, api):
    configure_app(flask_app)
    api.init_app(flask_app)
