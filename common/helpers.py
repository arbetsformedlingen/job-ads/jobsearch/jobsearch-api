import time
import re

from datetime import datetime, timedelta
from dateutil import parser


def calculate_utc_offset():
    is_dst = time.daylight and time.localtime().tm_isdst > 0
    utc_offset = - (time.altzone if is_dst else time.timezone)
    return int(utc_offset / 3600) if utc_offset > 0 else 0


# Helper function to parse date strings and relative time offsets
def parse_datetime(datestring_or_offset, now_fn=datetime.now):
    if re.match(r"^\d+$", datestring_or_offset):  # is digits
        now = now_fn()
        return now - timedelta(minutes=int(datestring_or_offset))
    else:
        return parser.parse(datestring_or_offset)
