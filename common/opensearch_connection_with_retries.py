import os
import sys
import time
import logging
import platform
from opensearchpy import OpenSearch, OpenSearchException
from opensearchpy.helpers.actions import scan

from common import settings
import requests

log = logging.getLogger(__name__)


def opensearch_search_with_retry(client, query, index, hard_failure=False, db_request_timeout=settings.DB_DEFAULT_TIMEOUT):
    fail_count = 0
    max_retries = settings.DB_RETRIES
    for i in range(max_retries):
        try:
            return client.search(body=query, index=index, request_timeout=db_request_timeout)
        except (OpenSearchException, Exception) as e:
            fail_count += 1
            time.sleep(settings.DB_RETRIES_SLEEP)
            log.warning(f"Failed to search opensearch, failed attempt {fail_count}. Error: {e}")
            if fail_count >= max_retries:
                log.error(f"Failed to search opensearch after: {fail_count} attempts.")
                if hard_failure:
                    log.error(f"Failed to search opensearch after: {fail_count} attempts. Exit!.")
                    hard_kill()
                else:
                    return None


def opensearch_scan_with_retry(client, query, index, size, source):
    fail_count = 0
    max_retries = settings.DB_RETRIES
    for i in range(max_retries):
        try:
            return scan(client, query, index=index, size=size, _source=source)
        except (OpenSearchException, Exception) as e:
            fail_count += 1
            time.sleep(settings.DB_RETRIES_SLEEP)
            log.warning(f"Failed to search opensearch, failed attempt {fail_count}. Error: {e}")
            if fail_count >= max_retries:
                log.error(f"Failed to search opensearch after: {fail_count} attempts. Exit!")
                hard_kill()


def create_opensearch_client_with_retry(host, port=settings.DB_PORT, user=settings.DB_USER, password=settings.DB_PWD,
                                        db_timeout=settings.DB_DEFAULT_TIMEOUT):
    start_time = int(time.time() * 1000)
    log.info(f"Creating opensearch client, host: {', '.join(host)}, port: {port} user: {user} ")
    fail_count = 0
    max_retries = settings.DB_RETRIES
    auth = None
    if user and password:
        auth = (user, password)

    for i in range(max_retries):
        try:
            if auth:
                opensearch = OpenSearch(hosts=[{"host": host, "port": settings.DB_PORT} for host in settings.DB_HOST],
                                        http_compress=True,  # enables gzip compression for request bodies
                                        http_auth=auth,
                                        use_ssl=settings.DB_USE_SSL,
                                        verify_certs=settings.DB_VERIFY_CERTS,
                                        ssl_assert_hostname=False,
                                        ssl_show_warn=False,
                                        timeout=db_timeout,
                                        max_retries=10,
                                        retry_on_timeout=True,
                                        )
            else:
                opensearch = OpenSearch(hosts=[{"host": host, "port": settings.DB_PORT} for host in settings.DB_HOST],
                                        http_compress=True,  # enables gzip compression for request bodies
                                        use_ssl=settings.DB_USE_SSL,
                                        verify_certs=settings.DB_VERIFY_CERTS,
                                        ssl_assert_hostname=False,
                                        ssl_show_warn=False,
                                        timeout=db_timeout,
                                        max_retries=10,
                                        retry_on_timeout=True,
                                        )

            # check connection
            info = opensearch.info()
            log.info(f"Successfully connected to opensearch node {info['name']} of cluster {info['cluster_name']}")
            log.debug(f"opensearch info: {info}")
            log.debug(
                f"opensearch node created and connected after: {int(time.time() * 1000 - start_time)} milliseconds")
            return opensearch

        except (OpenSearchException, Exception, requests.exceptions.BaseHTTPError) as e:
            fail_count += 1
            time.sleep(settings.DB_RETRIES_SLEEP)
            log.warning(f"Failed to connect to opensearch, failed attempt: {fail_count}. Error: {e}")
            if fail_count >= max_retries:
                log.error(f"Failed to connect to opensearch after attempts: {fail_count}. Exit!")
                hard_kill()


def hard_kill():
    """
    to shut down uwsgi and exit
    for this to work, it needs to be configured not to restart:
    - die-on-term = true in uwsgi.ini
    - autorestart=false in supervisord.conf
    """
    log.error("(hard_kill)Shutting down...")
    if platform.system() != 'Windows':
        os.system('killall -15 uwsgi')  # to send SIGTERM
    sys.exit(1)
