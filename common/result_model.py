from flask_restx import fields, Api
from common import settings, fields as f

root_api = Api()


class AdUrl(fields.Raw):
    def format(self, value):
        if settings.BASE_PB_URL[-1] == '/':
            return "%s%s" % (settings.BASE_PB_URL, value)
        else:
            return "%s/%s" % (settings.BASE_PB_URL, value)


taxonomy_item = root_api.model('JobTechTaxonomyItem', {
    'concept_id': fields.String(),
    'label': fields.String(),
    'legacy_ams_taxonomy_id': fields.String()
})

weighted_taxonomy_item = root_api.inherit('WeightedJobtechTaxonomyItem',
                                          taxonomy_item, {
                                              'weight': fields.Integer()
                                          })

min_max = root_api.model('ScopeOfWork', {
    'min': fields.Integer(),
    'max': fields.Integer()
})

description = root_api.model('JobAdDescription', {
    'text': fields.String(),
    'text_formatted': fields.String(),
    'company_information': fields.String(),
    'needs': fields.String(),
    'requirements': fields.String(),
    'conditions': fields.String()
})

employer = root_api.model('Employer', {
    'phone_number': fields.String(),
    'email': fields.String(),
    'url': fields.String(),
    'organization_number': fields.String(),
    'name': fields.String(),
    'workplace': fields.String()
})

appl_details = root_api.model('ApplicationDetails', {
    'information': fields.String(),
    'reference': fields.String(),
    'email': fields.String(),
    'via_af': fields.Boolean(),
    'url': fields.String(),
    'other': fields.String()
})

work_address = root_api.model('WorkplaceAddress', {
    'municipality': fields.String(),
    'municipality_code': fields.String(),
    'municipality_concept_id': fields.String(),
    'region': fields.String(),
    'region_code': fields.String(),
    'region_concept_id': fields.String(),
    'country': fields.String(),
    'country_code': fields.String(),
    'country_concept_id': fields.String(),
    'street_address': fields.String(),
    'postcode': fields.String(),
    'city': fields.String(),
    'coordinates': fields.List(fields.Float())
})

application_contact = root_api.model('ApplicationContact', {
    'name': fields.String(),
    'description': fields.String(),
    'email': fields.String(),
    'telephone': fields.String(),
    'contact_type': fields.String(attribute='contactType')
})

requirements = root_api.model('Requirements', {
    'skills': fields.List(fields.Nested(weighted_taxonomy_item)),
    'languages': fields.List(fields.Nested(weighted_taxonomy_item)),
    'work_experiences': fields.List(fields.Nested(weighted_taxonomy_item)),
    'education': fields.List(fields.Nested(weighted_taxonomy_item)),
    'education_level': fields.List(fields.Nested(weighted_taxonomy_item))
})

job_ad = root_api.model('JobAd', {
    f.ID: fields.String(),
    f.EXTERNAL_ID: fields.String(),
    "original_id": fields.String(),
    f.LABEL: fields.List(fields.String()),
    f.AD_URL: AdUrl(attribute='id'),
    f.LOGO_URL: fields.String(),
    f.HEADLINE: fields.String(),
    f.APPLICATION_DEADLINE: fields.DateTime(),
    f.NUMBER_OF_VACANCIES: fields.Integer(),
    'description': fields.Nested(description),
    'employment_type': fields.Nested(taxonomy_item),
    'salary_type': fields.Nested(taxonomy_item),
    'salary_description': fields.String(),
    'duration': fields.Nested(taxonomy_item),
    'working_hours_type': fields.Nested(taxonomy_item),
    'scope_of_work': fields.Nested(min_max),
    f.ACCESS: fields.String(),
    'employer': fields.Nested(employer),
    'application_details': fields.Nested(appl_details),
    f.EXPERIENCE_REQUIRED: fields.Boolean(),
    f.ACCESS_TO_OWN_CAR: fields.Boolean(),
    f.DRIVING_LICENCE_REQUIRED: fields.Boolean(),
    'driving_license': fields.List(fields.Nested(taxonomy_item, skip_none=True)),
    'occupation': fields.Nested(taxonomy_item),
    'occupation_group': fields.Nested(taxonomy_item),
    'occupation_field': fields.Nested(taxonomy_item),
    'workplace_address': fields.Nested(work_address),
    'must_have': fields.Nested(requirements),
    'nice_to_have': fields.Nested(requirements),
    'application_contacts': fields.Nested(application_contact),
    f.PUBLICATION_DATE: fields.DateTime(),
    f.LAST_PUBLICATION_DATE: fields.DateTime(),
    f.REMOVED: fields.Boolean(),
    f.REMOVED_DATE: fields.DateTime(),
    f.SOURCE_TYPE: fields.String(),
    'timestamp': fields.Integer(),
})
