import os
from pathlib import Path

# Opensearch settings
DB_HOST = os.getenv("DB_HOST", "127.0.0.1").split(",")
DB_PORT = os.getenv("DB_PORT", 9200)
DB_USER = os.getenv("DB_USER")
DB_PWD = os.getenv("DB_PWD")
DB_USE_SSL = os.getenv("DB_USE_SSL", "true").lower() == "true"
DB_VERIFY_CERTS = os.getenv("DB_VERIFY_CERTS", "true").lower() == "true"

ADS_ALIAS = os.getenv("ADS_ALIAS", "current_ads-read")

DB_TAX_INDEX_ALIAS = os.getenv("DB_TAX_INDEX_ALIAS", "taxonomy")
DB_RETRIES = int(os.getenv("DB_RETRIES", 1))
DB_RETRIES_SLEEP = int(os.getenv("DB_RETRIES_SLEEP", 2))
DB_DEFAULT_TIMEOUT = int(os.getenv("DB_DEFAULT_TIMEOUT", 10))

DELAY_LOAD_SYNONYM_DICTIONARY_STARTUP = os.getenv("DELAY_LOAD_SYNONYM_DICTIONARY_STARTUP", "false").lower() == "true"

BASE_PB_URL = os.getenv("BASE_PB_URL", "https://arbetsformedlingen.se/platsbanken/annonser/")

COMPANY_LOGO_TIMEOUT = os.getenv("COMPANY_LOGO_TIMEOUT", 5)
COMPANY_LOGO_CERT = os.getenv("COMPANY_LOGO_CERT") or Path(__file__).parent / ".." / "cert" / "af_jobtech_bundle.crt"

# If a proxy is needed to reach the internal network of AF it can be set with AF_PROXY.
AF_PROXY = os.getenv("AF_PROXY", None)

ADS_LABELS_CACHE_EXP_SECONDS = int(os.getenv("ADS_LABELS_CACHE_EXP_SECONDS", 600))

JAE_API_URL = os.getenv("JAE_API_URL", "https://jobad-enrichments-api.jobtechdev.se")
