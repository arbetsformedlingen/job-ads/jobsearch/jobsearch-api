FROM  docker.io/library/python:3.10.15-slim-bookworm

EXPOSE 8081

ENV TZ=Europe/Stockholm \
    PROC_NR=8 \
    HARAKIRI=120

RUN apt-get update -y && \
    apt-get install -y \
    psmisc \
    git \
    build-essential && \
    apt-get clean -y && \
    ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

COPY . /app
COPY cert/* /CERT/

WORKDIR /app

RUN python -m pip install --upgrade setuptools wheel pip && \
    pip install -r requirements.txt && \
    python -m pytest -svv tests/unit_tests && \
    rm -rf tests && \
    echo "" && echo Application: jobsearch && echo Process total: $PROC_NR && echo ""

# Remove git because of current vulnerability https://access.redhat.com/security/cve/CVE-2023-27536
RUN apt-get remove -y git &&\
    apt-get autoremove -y

CMD  ["uwsgi", "--http", ":8081", "--manage-script-name", "--mount", "/=wsgi:app", "--ini", "uwsgi.ini"]

# TODO:
# Have a separate build step copy only necessary files to final image to get a small one.
