import logging
import json
from common.main_opensearch_client import opensearch_client

from common.opensearch_connection_with_retries import opensearch_search_with_retry

from common import settings
from common import fields
from common.utils.decorators import singleton

log = logging.getLogger(__name__)

@singleton
class AdsLocations(object):

    def __init__(self):
        self.client = opensearch_client()
        self.ads_index = settings.ADS_ALIAS
        self.extracted_locations = self._load_locations()

    def get_extracted_locations(self):
        return self.extracted_locations

    def _load_locations(self) -> set:
        '''
        Loads locations from the ads in Opensearch.
        :return: A set with locations/cities from scraped structured ad data.
        '''
        query = {
            "aggs": {
                "ext_locations": {
                    "terms": {
                        "field": "%s.location.raw" % fields.KEYWORDS_EXTRACTED,
                        "size": 20000
                    }
                }
            },
            "query": {
                "match_all": {}
            },
            "size": 0
        }
        log.info(f"AdsLocations(_load_locations). Index: {self.ads_index}")
        log.debug(f"AdsLocations(_load_locations). Query: {json.dumps(query)}")
        results = opensearch_search_with_retry(self.client, query, self.ads_index, hard_failure=True)
        ext_buckets = results.get('aggregations', {}).get('ext_locations', {}).get('buckets', [])
        found_locations = [p['key'] for p in ext_buckets if not p['key'].isnumeric()]
        return set(found_locations)