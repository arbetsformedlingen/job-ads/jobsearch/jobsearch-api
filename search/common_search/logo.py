import io
import logging
import os

import requests
from flask import send_file
from werkzeug.exceptions import ServiceUnavailable

from common import settings
from search.common_search.ad_search import fetch_ad_by_id

log = logging.getLogger(__name__)

current_dir = os.path.dirname(os.path.realpath(__file__)) + "/"

not_found_file = None


def get_not_found_logo_file():
    global not_found_file
    if not_found_file is None:
        not_found_filepath = current_dir + "../resources/1x1-00000000.png"
        log.debug(f"Opening global file: {not_found_filepath}")
        with open(not_found_filepath, "rb") as f:
            not_found_file = f.read()

    return not_found_file


def fetch_ad_logo(ad_id: str):
    ad = fetch_ad_by_id(ad_id)
    logo_url = get_correct_logo_url(ad)

    if logo_url is None:
        log.debug(f"Logo url for ad with id {ad_id} not found, sending empty image")
        return file_formatter(get_not_found_logo_file())
    else:
        try:
            r = requests.get(
                logo_url,
                stream=True,
                timeout=settings.COMPANY_LOGO_TIMEOUT,
                proxies={"http": settings.AF_PROXY, "https": settings.AF_PROXY},
                verify=settings.COMPANY_LOGO_CERT,
            )
        except requests.RequestException as e:
            """
            If there is an exception (requests.RequestException is base for several different exceptions),
            log it and raise http 503 "Service Unavailable"
            """
            log.error(f"Error for logo url {logo_url}: {e}")
            raise ServiceUnavailable(f"Error getting logo for id {ad_id}")
        if r.status_code != 200:
            log.error(f"Status code {r.status_code} for {logo_url}")
            r.raise_for_status()
        log.debug(f"Logo found on: {logo_url}")
        return file_formatter(r.raw.read(decode_content=False))


def get_correct_logo_url(ad):
    logo_url = None
    if ad:
        logo_url = ad.get("logo_url", None)
    return logo_url


def file_formatter(file_object):
    return send_file(io.BytesIO(file_object), download_name="logo.png", mimetype="image/png")
