import json
import logging
import re
import time
from operator import itemgetter

from fast_autocomplete import AutoComplete
from flask_restx import abort

from common import constants, settings
from common.main_opensearch_client import opensearch_client
from common.opensearch_connection_with_retries import opensearch_search_with_retry
from search.common_search.ad_search import search_for_ads
from search.common_search.ads_labels import AdsLabels

log = logging.getLogger(__name__)

def suggest(args, querybuilder):
    """
    Returns a list of typeahead suggestions (in result['aggs']).
    In the normal case find_platsannonser() gives the suggestions in the aggs,
    which is then combined with the prefix words. However, <location><space>
    is a special case when instead extra words of type occupation are added.
    Finally, if the list of suggestions is still empty, then
    _complete_suggest(), and possibly _phrase_suggest(), will be called.

    The parameter CONTEXTUAL_TYPEAHEAD changes the behaviour of find_platsannonser()
    The suggestions and their ordering become different.
    True -> aggs based on subset of ads, from query match with prefix words
    False -> aggs based on all ads
    """
    prefix_words = args[constants.FREETEXT_QUERY]
    if (
            len(prefix_words.split()) == 1
            and args[constants.TYPEAHEAD_QUERY][-1:] == ' '
            and (word_type := _check_search_word_type(prefix_words, querybuilder)) == 'location'
    ):
        # FIXME: What is the purpose of this block? Shouldn't we always suggest something when prefix_words ends with a space?
        # FIXME: This block will not run for locations that contains a space, like 'stockholms län'
        # is <location> + <space>: find extra words
        log.debug("<location><space> query pattern. Extra words used as typeahead result.")
        extra_words = suggest_extra_word(prefix_words, querybuilder, word_type)
        result = {'aggs': extra_words}
    else:
        result = search_for_ads(args, querybuilder, start_time=0, x_fields=None)
        if result.get('aggs') and prefix_words:
            final_aggs = []
            for item in result.get('aggs'):
                value = item['value']
                found_phrase = item['found_phrase']
                if is_unwanted_suggested_word(value, args[constants.TYPEAHEAD_QUERY][-1:]):
                    continue
                if len(value) == len(found_phrase):
                    item['value'] = prefix_words + ' ' + value
                else:
                    # item['value'] can have fewer words in some cases.
                    # For example 'län' instead of 'stockholms län' when the search string is 'stockholms lä'.
                    # Make sure that 'län' is replaced with 'stockholms län' with the help of found_phrase.
                    item['value'] = merge_missing_words(prefix_words, found_phrase)
                # TODO: Using value on the line below instead of found_phrase seems to be lazy fix to return a correct value.
                #  If found_phrase is used for search "snickare jönköpings lä" it will have the value "snickare jönköpings jönköpings län"
                item['found_phrase'] = prefix_words + ' ' + value
                if item['found_phrase'].strip() != args[constants.TYPEAHEAD_QUERY].strip():
                    final_aggs.append(item)
            result['aggs'] = final_aggs
        log.debug(f"Typeahead result (normal case): {result.get('aggs')}")
    if not result.get('aggs'):
        result = _complete_suggest(args, querybuilder, start_time=0)
        log.debug(f"No aggs found, result (complete suggest): {result.get('aggs')}")
        if not result.get('aggs') and is_multiple_words(args.get(constants.TYPEAHEAD_QUERY)):
            result = _phrase_suggest(args, querybuilder, start_time=0)
            log.debug(f"No aggs and no complete_suggest, result (phrase suggest): {result.get('aggs')}")

    label_result = _label_suggest(args, querybuilder, start_time=0)
    if label_result.get('aggs'):
        if not result.get('aggs'):
            result = label_result
        else:
            # Add label suggestions to result
            previous_result_values =  set([v['value'] for v in result['aggs']])

            for item in label_result.get('aggs'):
                 if item['value'] not in previous_result_values:
                     result['aggs'].append(item)
    if 'aggs' in result:
        result['aggs'] = clean_unwanted_completions(args[constants.TYPEAHEAD_QUERY], result)
    else:
        result['aggs'] = []

    return result


def merge_missing_words(base_string, compare_string):
    """
    Replaces words from the end of the base_string with words from the compare_string.
    Example: 'stockholms län jönköpings' and 'jönköpings län' -> 'stockholms län jönköpings län'
    """
    base_words = base_string.split()
    compare_words = compare_string.split()

    # Find the position where the compare_words should start replacing in base_words
    for i in range(len(base_words)):
        if base_words[i:] == compare_words[:len(base_words) - i]:
            return ' '.join(base_words[:i] + compare_words)

    # If no match is found, return the base_string concatenated with compare_string
    complete_string = base_string + ' ' + compare_string
    return complete_string.strip()

def clean_unwanted_completions(typeahead_query, result):
    if not result.get('aggs'):
        return []

    typeahead_query_stripped = typeahead_query.strip()
    items_to_remove = []
    for item in result.get('aggs'):
        if item['found_phrase'].strip() == typeahead_query_stripped:
            items_to_remove.append(item)
    return [item for item in result.get('aggs') if item not in items_to_remove]


def is_multiple_words(text):
    return len(remove_multiple_spaces(text).split()) > 1

def remove_multiple_spaces(input_string):
    return re.sub(r'\s+', ' ', input_string).strip()

def is_unwanted_suggested_word(value, last_word='*'):
    if not last_word:
        last_word = '*'
    # FIXME: It's unclear why this function has the last part:
    #  'not any([item.startswith(last_word) for item in constants.UNWANTED_SUGGESTED_WORDS]))'
    #  Should it return True if the last_word starts with 'sven' like in 'svenska'? Right now it returns False.
    return (value in constants.UNWANTED_SUGGESTED_WORDS and
            not any([item.startswith(last_word) for item in constants.UNWANTED_SUGGESTED_WORDS]))


def suggest_extra_word(search_text, querybuilder, search_text_type=None):
    # input one word and suggest extra word
    if not search_text_type:
        search_text_type = _check_search_word_type(search_text, querybuilder)
    new_suggest_list = []
    if search_text_type:
        second_suggest_type = 'occupation' if search_text_type == 'location' else 'location'
        query_dsl = querybuilder.create_suggest_extra_word_query(search_text, search_text_type, second_suggest_type)
        log.debug(f'(suggest_extra_word) query: {query_dsl}')
        query_result = opensearch_search_with_retry(opensearch_client(), query_dsl, settings.ADS_ALIAS)
        results = query_result.get('aggregations').get('first_word').get('second_word').get('buckets')
        for result in results:
            if is_unwanted_suggested_word(result.get('key')):
                continue
            new_suggest_list.append({
                'value': search_text + ' ' + result.get('key'),
                'found_phrase': search_text + ' ' + result.get('key'),
                'type': search_text_type + '_' + second_suggest_type,
                'occurrences': result.get('doc_count')
            })

    log.debug(f'(suggest_extra_word) extra words result: {new_suggest_list}')
    return new_suggest_list


def _check_search_word_type(word, querybuilder):
    '''
    Checks input words type and returns type location/skill/occupation/None
    '''
    word_type = None
    if word:
        word = word.strip()

    if word:
        if (concepts := querybuilder.text_to_concepts(word)):
            for key in concepts.keys():
                value = concepts[key]
                if value:
                    # Get first part of key, for example 'skill_must_not' -> 'skill'
                    word_type = key.split('_')[0]
    return word_type


def _complete_suggest(args, querybuilder, start_time=0):
    if start_time == 0:
        start_time = int(time.time() * 1000)

    input_words = args.get(constants.TYPEAHEAD_QUERY)

    word_list = input_words.split()
    args_middle = args.copy()
    prefix_words = word_list[:-1] if word_list else ''
    if prefix_words:
        args_middle[constants.TYPEAHEAD_QUERY] = ' '.join(prefix_words)
        result = search_for_ads(args_middle, querybuilder, start_time=0, x_fields=None)
        if not result.get('aggs'):
            return result

    word = word_list[-1] if word_list else ''
    if word_list and word_list[:-1]:
        prefix = ' '.join(input_words.split()[:-1])
    else:
        prefix = ''

    query_dsl = querybuilder.create_auto_complete_suggester(word)
    log.debug(f"(complete_suggest) args(complete_suggest): {args}")
    log.debug(f"(complete_suggest) query: {json.dumps(query_dsl)}")
    log.debug(f"(complete_suggest) query constructed after: {int(time.time() * 1000 - start_time)} milliseconds")
    query_result = opensearch_search_with_retry(opensearch_client(), query_dsl, settings.ADS_ALIAS)
    log.debug(f"(complete_suggest) results after: {int(time.time() * 1000 - start_time)} milliseconds")
    if not query_result:
        abort(500, 'Failed to establish connection to database')
        return

    log.debug(f"(complete_suggest) took: {query_result.get('took', 0)}, timed_out: {query_result.get('timed_out', '')}")

    aggs = []
    suggests = query_result.get('suggest', {})
    log.debug(f"(complete_suggest) query result: {suggests}")

    for key in suggests:
        if suggests[key][0].get('options', []):
            for ads in suggests[key][0]['options']:
                value = prefix + ' ' + ads.get('text', '') if prefix else ads.get('text', '')
                aggs.append(
                    {
                        'value': value.strip(),
                        'found_phrase': value.strip(),
                        'type': key.split('-')[0],
                        'occurrences': 0
                    }
                )

    query_result['aggs'] = _suggest_check_occurrences(aggs[:50], args, querybuilder)

    return query_result


def _phrase_suggest(args, querybuilder, start_time=0):
    if start_time == 0:
        start_time = int(time.time() * 1000)

    input_words = args.get(constants.TYPEAHEAD_QUERY)
    query_dsl = querybuilder.create_phrase_suggester(input_words)

    log.debug(f"(phrase_suggest) query constructed after: {int(time.time() * 1000 - start_time)} milliseconds.")
    log.debug(f"(phrase_suggest) args: {args}")
    log.debug(f"(phrase_suggest) query: {json.dumps(query_dsl)}")

    query_result = opensearch_search_with_retry(opensearch_client(), query_dsl, settings.ADS_ALIAS)
    log.debug(f"(phrase_suggest) results after: {int(time.time() * 1000 - start_time)} milliseconds.")
    if not query_result:
        abort(500, 'Failed to establish connection to database')
        return

    log.debug(f"(phrase_suggest) took: {query_result.get('took', 0)}, timed_out: {query_result.get('timed_out', '')}")

    aggs = []
    suggests = query_result.get('suggest', {})
    log.debug(f"(phrase_suggest) query result: {suggests}")

    for key in suggests:
        if suggests[key][0].get('options', []):
            for ads in suggests[key][0]['options']:
                value = ads.get('text', '')
                aggs.append(
                    {
                        'value': value,
                        'found_phrase': value,
                        'type': key.split('.')[-1].split('_')[0],
                        'occurrences': 0
                    }
                )

    query_result['aggs'] = _suggest_check_occurrences(aggs[:50], args, querybuilder)

    return query_result

def _label_suggest(args, querybuilder, start_time=0):
    if start_time == 0:
        start_time = int(time.time() * 1000)

    complete_string = args.get(constants.TYPEAHEAD_QUERY)
    word_list = complete_string.split(' ')
    complete_word = word_list[-1]

    if len(complete_word) < 3 or any(complete_word.endswith(x) for x in ['.', '+', '#', '-']):
        # Bug in fast autocomplete for words that ends with non alpha numeric characters
        return {}

    autocomplete = _prepare_label_autocomplete()
    autocomplete_result = autocomplete.search(word=complete_word, max_cost=3)

    autocomplete_result_flat = _flatten_autocomplete_result(autocomplete_result)
    query_result = {}

    aggs = []
    for term in autocomplete_result_flat:
        full_autocomplete = ' '.join(word_list[0:-1]) + ' ' + term.lower()
        full_autocomplete = full_autocomplete.strip()
        aggs.append(
            {
                'value': full_autocomplete,
                'found_phrase': full_autocomplete,
                'type': 'label',
                'occurrences': 0
            }
        )

    log.debug(f"(_label_suggest) results after: {int(time.time() * 1000 - start_time)} milliseconds.")
    query_result['aggs'] = _suggest_check_occurrences(aggs[:50], args, querybuilder)

    return query_result


def _prepare_label_autocomplete():
    ads_labels = AdsLabels()
    autocomplete_valid_chars = 'abcdefghijklmnopqrstuvwxyzåäö'
    autocomplete_valid_chars += autocomplete_valid_chars.upper()
    autocomplete_valid_chars += '0123456789'
    extracted_ads_labels = ads_labels.get_extracted_ads_labels()
    extracted_ads_labels_dict = {k: {} for k in extracted_ads_labels}
    autocomplete = AutoComplete(words=extracted_ads_labels_dict, valid_chars_for_string=autocomplete_valid_chars)
    return autocomplete


def _flatten_autocomplete_result(xss):
    return [x for xs in xss for x in xs]


def _suggest_check_occurrences(aggs, args, querybuilder):
    saved_value = args.get(constants.FREETEXT_QUERY)

    for agg in aggs:
        args[constants.FREETEXT_QUERY] = agg.get('value')
        # Set CONTEXTUAL_TYPEAHEAD to True to avoid a match_all query in querybuilder.parse_args when counting occurences
        args[constants.CONTEXTUAL_TYPEAHEAD] = True
        query_dsl = querybuilder.build_query(args)
        del query_dsl['aggs']
        del query_dsl['from']
        del query_dsl['size']
        del query_dsl['sort']
        query_result = opensearch_client().count(body=query_dsl, index=settings.ADS_ALIAS, request_timeout=settings.DB_DEFAULT_TIMEOUT)
        occurrences = query_result.get('count')
        agg['occurrences'] = occurrences

    args[constants.FREETEXT_QUERY] = saved_value
    aggs = sort_on_occurences(aggs)
    return aggs


def sort_on_occurences(complete_return_values):
    return sorted(complete_return_values, key=itemgetter('occurrences'), reverse=True)


