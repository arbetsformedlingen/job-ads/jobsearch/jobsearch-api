import logging
import json
from common import settings, fields, taxonomy
from common.main_opensearch_client import opensearch_client
from common.opensearch_connection_with_retries import opensearch_search_with_retry
from common.taxonomy import tax_type, annons_key_to_jobtech_taxonomy_key

log = logging.getLogger(__name__)

taxonomy_cache = {}

reverse_tax_type = {item[1]: item[0] for item in tax_type.items()}


def _build_query(query_string, taxonomy_code, entity_type, offset, limit):
    musts = []
    sort = None
    if query_string:
        musts.append({"bool": {"should": [
            {
                "match_phrase_prefix": {
                    "label": {
                        "query": query_string
                    }
                }
            },
            {
                "term": {
                    "concept_id": {"value": query_string}
                }
            },
            {
                "term": {
                    "legacy_ams_taxonomy_id": {"value": query_string}
                }
            }
        ]}})
    else:
        # Sort numerically for non-query_string-queries
        sort = [
            {
                "legacy_ams_taxonomy_num_id": {"order": "asc"}
            }
        ]
    if taxonomy_code:
        if not isinstance(taxonomy_code, list):
            taxonomy_code = [taxonomy_code]
        terms = [{"term": {"parent.legacy_ams_taxonomy_id": t}} for t in taxonomy_code]
        terms += [{"term": {"parent.concept_id.keyword": t}} for t in taxonomy_code]
        terms += [{"term":
                       {"parent.parent.legacy_ams_taxonomy_id": t}
                   } for t in taxonomy_code]
        terms += [{"term":
                       {"parent.parent.concept_id.keyword": t}
                   } for t in taxonomy_code]
        parent_or_grandparent = {"bool": {"should": terms}}
        # musts.append({"term": {"parent.id": taxonomy_code}})
        musts.append(parent_or_grandparent)
    if entity_type:
        musts.append({"bool": {"should": [{"term": {"type": et}} for et in entity_type]}})
        # musts.append({"term": {"type": entity_type}})

    if not musts:
        query_dsl = {"query": {"match_all": {}}, "from": offset, "size": limit}
    else:
        query_dsl = {
            "query": {
                "bool": {
                    "must": musts
                }
            },
            "from": offset,
            "size": limit
        }
    if sort:
        query_dsl['sort'] = sort

    query_dsl['track_total_hits'] = True
    return query_dsl


def find_concept_by_legacy_ams_taxonomy_id(opensearch_client, taxonomy_type, legacy_ams_taxonomy_id,
                                           not_found_response=None):
    query = {
        "query": {
            "bool": {
                "must": [
                    {"term": {"legacy_ams_taxonomy_id": {
                        "value": legacy_ams_taxonomy_id}}},
                    {"term": {
                        "type": {
                            "value": annons_key_to_jobtech_taxonomy_key.get(taxonomy_type, '')
                        }
                    }}
                ]
            }
        }
    }

    opensearch_response = opensearch_search_with_retry(opensearch_client, query, settings.DB_TAX_INDEX_ALIAS)

    hits = opensearch_response.get('hits', {}).get('hits', [])
    if not hits:
        log.warning(f"No taxonomy entity found for type: {taxonomy_type} and legacy id: {legacy_ams_taxonomy_id}")
        return not_found_response
    return hits[0]['_source']


def find_concepts(opensearch_client, query_string=None, taxonomy_code=[], entity_type=[], offset=0, limit=10):
    query_dsl = _build_query(query_string, taxonomy_code, entity_type, offset, limit)
    log.debug(f"Query: {json.dumps(query_dsl)}")

    opensearch_response = opensearch_search_with_retry(client=opensearch_client, query=query_dsl,
                                                       index=settings.DB_TAX_INDEX_ALIAS)
    log.debug(
        f"(find_concepts) took: {opensearch_response.get('took', '')}, timed_out: {opensearch_response.get('timed_out', '')}")
    if opensearch_response:
        return opensearch_response
    else:
        log.error(f"Failed to query Opensearch, query: {query_dsl} index: {settings.DB_TAX_INDEX_ALIAS}")
        return None


def get_stats_for(taxonomy_type):
    value_path = {
        taxonomy.OCCUPATION: "%s.%s.keyword" %
                             (fields.OCCUPATION, fields.LEGACY_AMS_TAXONOMY_ID),
        taxonomy.GROUP: "%s.%s.keyword" % (
            fields.OCCUPATION_GROUP, fields.LEGACY_AMS_TAXONOMY_ID),
        taxonomy.FIELD: "%s.%s.keyword" % (
            fields.OCCUPATION_FIELD, fields.LEGACY_AMS_TAXONOMY_ID),
        taxonomy.SKILL: "%s.%s.keyword" % (fields.MUST_HAVE_SKILLS,
                                           fields.LEGACY_AMS_TAXONOMY_ID),
        taxonomy.MUNICIPALITY: "%s" % fields.WORKPLACE_ADDRESS_MUNICIPALITY_CODE,
        taxonomy.REGION: "%s" % fields.WORKPLACE_ADDRESS_REGION_CODE
    }
    # Make sure we don't crash if we want to stat on missing type
    for tt in taxonomy_type:
        if tt not in value_path:
            log.warning(f"Taxonomy type: {taxonomy_type} not configured for aggs.")
            return {}

    aggs_query = {
        "from": 0, "size": 0,
        "query": {
            "bool": {
                "must": [{"match_all": {}}],
                'filter': [
                    {
                        'range': {
                            fields.PUBLICATION_DATE: {
                                'lte': 'now/m'
                            }
                        }
                    },
                    {
                        'range': {
                            fields.LAST_PUBLICATION_DATE: {
                                'gte': 'now/m'
                            }
                        }
                    },
                    {
                        'term': {
                            fields.REMOVED: False
                        }
                    },
                ]
            }
        },
        "aggs": {
            "antal_annonser": {
                "terms": {"field": value_path[taxonomy_type[0]], "size": 5000},
            }
        }
    }
    log.debug(f'(get_stats_for) aggs_query: {json.dumps(aggs_query)}')
    aggs_result = opensearch_search_with_retry(opensearch_client(), aggs_query, settings.ADS_ALIAS)

    code_count = {
        item['key']: item['doc_count']
        for item in aggs_result['aggregations']['antal_annonser']['buckets']}
    return code_count
