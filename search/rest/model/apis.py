from flask_restx import Api, Namespace, fields

from common import constants
from common.result_model import root_api, job_ad

# Result models for Jobsearch API
job_ad_searchresult = root_api.inherit('JobAdSearchResult', job_ad, {
    'relevance': fields.Float(),
})

stat_item = root_api.model('StatDetail', {
    'term': fields.String(),
    'concept_id': fields.String(),
    'code': fields.String(),
    'count': fields.Integer()
})

search_stats = root_api.model('Stats', {
    'type': fields.String(),
    'values': fields.List(fields.Nested(stat_item, skip_none=True))
})

freetext_concepts = root_api.model('FreetextConcepts', {
    'skill': fields.List(fields.String()),
    'occupation': fields.List(fields.String()),
    'location': fields.List(fields.String()),
    'skill_must': fields.List(fields.String()),
    'occupation_must': fields.List(fields.String()),
    'location_must': fields.List(fields.String()),
    'skill_must_not': fields.List(fields.String()),
    'occupation_must_not': fields.List(fields.String()),
    'location_must_not': fields.List(fields.String()),
})

number_of_hits = root_api.model('NumberOfHits', {
    'value': fields.Integer()
})

open_results = root_api.model('SearchResults', {
    'total': fields.Nested(number_of_hits),
    'positions': fields.Integer(),
    'query_time_in_millis': fields.Integer(),
    'result_time_in_millis': fields.Integer(),
    'stats': fields.List(fields.Nested(search_stats, skip_none=True)),
    'freetext_concepts': fields.Nested(freetext_concepts, skip_none=True),
    'hits': fields.List(fields.Nested(job_ad_searchresult), attribute='hits', skip_none=True)
})

# Jobsearch

search_api = Api(version=constants.API_VERSION, title='JobSearch',
                 description='## An API for searching current job ads from the Swedish Public Employment Service\n'
                             '**Useful links:**\n'
                             '- [Getting Started](https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-apis/-/blob/main/docs/GettingStartedJobSearchEN.md)\n'
                             '- [Ad fields](https://gitlab.com/arbetsformedlingen/job-ads/jobsearch/jobsearch-api/-/blob/main/docs/AdFields.md)\n'
                             '- [Code examples](https://gitlab.com/arbetsformedlingen/job-ads/getting-started-code-examples/code-examples-start-here)\n'
                             '- [Taxonomy Getting Started](https://arbetsformedlingen.gitlab.io/taxonomy-dev/projects/jobtech-taxonomy/overview.html)\n'
                             '- [Taxonomy Atlas](https://atlas.jobtechdev.se)\n',
                 default_label="An API for retrieving current job ads",
                 contact_url='https://forum.jobtechdev.se/c/vara-api-er-dataset/job-search/28',
                 contact='Contact: JobTech Forum',
                 license='Ads are licensed under CC0',
                 license_url='https://creativecommons.org/publicdomain/zero/1.0/deed.sv')

ns_jobad = Namespace('Open AF-job ads',
                     description='Search and retrieve Arbetsförmedlingens (AF) job ads. Used for online operations.')


search_api.add_namespace(ns_jobad, '/')

for name, definition in root_api.models.items():
    ns_jobad.add_model(name, definition)

typeahead_item = ns_jobad.model('TypeaheadItem', {
    'value': fields.String(),
    'found_phrase': fields.String(),
    'type': fields.String(),
    'occurrences': fields.Integer()
})

typeahead_results = ns_jobad.model('TypeaheadResults', {
    'result_time_in_millis': fields.Integer(),
    'time_in_millis': fields.Integer(),
    'typeahead': fields.List(fields.Nested(typeahead_item))
})

