from datetime import datetime

from flask_restx import inputs, reqparse

from common import constants, fields, taxonomy

# Frågemodeller
QF_CHOICES = ["occupation", "skill", "location", "employer"]
VF_TYPE_CHOICES = [
    taxonomy.OCCUPATION,
    taxonomy.GROUP,
    taxonomy.FIELD,
    taxonomy.SKILL,
    taxonomy.MUNICIPALITY,
    taxonomy.REGION,
    taxonomy.COUNTRY,
    taxonomy.PLACE,
    taxonomy.WAGE_TYPE,
    taxonomy.WORKTIME_EXTENT,
    taxonomy.DRIVING_LICENCE,
    taxonomy.EMPLOYMENT_TYPE,
    taxonomy.LANGUAGE,
]
OPTIONS_BRIEF = "brief"
OPTIONS_FULL = "full"


def lowercase_maxlength(value):
    if value is None:
        raise ValueError("string type must be non-null")
    if len(value) > 255:
        raise ValueError("parameter can not be longer than 255 characters")

    return str(value).lower()


# /ad endpoint query
load_ad_query = reqparse.RequestParser()

# Base query for use with /search and /complete endpoints
base_annons_query = reqparse.RequestParser()
base_annons_query.add_argument(
    constants.X_FEATURE_FREETEXT_BOOL_METHOD,
    choices=["and", "or"],
    default=constants.DEFAULT_FREETEXT_BOOL_METHOD,
    location="headers",
    required=False,
)

base_annons_query.add_argument(
    constants.X_FEATURE_DISABLE_SMART_FREETEXT, type=inputs.boolean, location="headers", required=False
)


base_annons_query.add_argument(
    constants.X_FEATURE_ENABLE_FALSE_NEGATIVE, type=inputs.boolean, location="headers", required=False
)

datetime_or_minutes_regex = (
    r"^(\d\d\d\d-(0?[1-9]|1[0-2])-(0?[1-9]|[12][0-9]|3[01])T(0[0-9]|1[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9]))|(\d+)$"
)

base_annons_query.add_argument(constants.PUBLISHED_BEFORE, type=inputs.regex(datetime_or_minutes_regex))
base_annons_query.add_argument(constants.PUBLISHED_AFTER, type=inputs.regex(datetime_or_minutes_regex))
base_annons_query.add_argument(taxonomy.OCCUPATION, action="append")
base_annons_query.add_argument(taxonomy.GROUP, action="append")
base_annons_query.add_argument(taxonomy.FIELD, action="append")
base_annons_query.add_argument(taxonomy.COLLECTION, action="append")
base_annons_query.add_argument(taxonomy.SKILL, action="append")
base_annons_query.add_argument(taxonomy.LANGUAGE, action="append")
base_annons_query.add_argument(taxonomy.WORKTIME_EXTENT, action="append")
base_annons_query.add_argument(constants.PARTTIME_MIN, type=float)
base_annons_query.add_argument(constants.PARTTIME_MAX, type=float)
base_annons_query.add_argument(taxonomy.DRIVING_LICENCE_REQUIRED, type=inputs.boolean)
base_annons_query.add_argument(taxonomy.DRIVING_LICENCE, action="append")
base_annons_query.add_argument(taxonomy.EMPLOYMENT_TYPE, action="append")
base_annons_query.add_argument(constants.EXPERIENCE_REQUIRED, type=inputs.boolean)
base_annons_query.add_argument(taxonomy.MUNICIPALITY, action="append")
base_annons_query.add_argument(taxonomy.REGION, action="append")
base_annons_query.add_argument(taxonomy.COUNTRY, action="append")
base_annons_query.add_argument(constants.UNSPECIFIED_SWEDEN_WORKPLACE, type=inputs.boolean)
base_annons_query.add_argument(constants.ABROAD, type=inputs.boolean)
base_annons_query.add_argument(constants.REMOTE, type=inputs.boolean)
base_annons_query.add_argument(constants.OPEN_FOR_ALL, type=inputs.boolean)
base_annons_query.add_argument(constants.TRAINEE, type=inputs.boolean)
base_annons_query.add_argument(constants.LARLING, type=inputs.boolean)
base_annons_query.add_argument(constants.FRANCHISE, type=inputs.boolean)
base_annons_query.add_argument(constants.HIRE_WORK_PLACE, type=inputs.boolean)
# Matches(lat,long) +90.0,-127.554334; 45,180; -90,-180; -90.000,-180.0000; +90,+180
# r for raw, PEP8
position_regex = r"^[-+]?([1-8]?\d(\.\d*)?|90(\.0*)?)," r"[-+]?(180(\.0*)?|((1[0-7]\d)|([1-9]?\d))(\.\d*)?)$"
base_annons_query.add_argument(constants.POSITION, type=inputs.regex(position_regex), action="append")
base_annons_query.add_argument(constants.POSITION_RADIUS, type=int, action="append")
base_annons_query.add_argument(constants.EMPLOYER, action="append")
base_annons_query.add_argument(constants.FREETEXT_QUERY, type=lowercase_maxlength)
base_annons_query.add_argument(constants.FREETEXT_FIELDS, action="append", choices=QF_CHOICES)
base_annons_query.add_argument(constants.DURATION, action="append")

# /complete endpoint query
annons_complete_query = base_annons_query.copy()
annons_complete_query.add_argument(constants.LIMIT, type=inputs.int_range(0, constants.MAX_COMPLETE_LIMIT), default=10)
annons_complete_query.add_argument(constants.CONTEXTUAL_TYPEAHEAD, type=inputs.boolean, default=True)
annons_complete_query.add_argument(constants.LABEL, type=str, action="append", required=False)

# /search endpoint query
annons_search_query = base_annons_query.copy()
annons_search_query.add_argument(constants.MIN_RELEVANCE, type=float)
annons_search_query.add_argument(constants.DETAILS, choices=[OPTIONS_FULL, OPTIONS_BRIEF])
annons_search_query.add_argument(constants.OFFSET, type=inputs.int_range(0, constants.MAX_OFFSET), default=0)
annons_search_query.add_argument(constants.LIMIT, type=inputs.int_range(0, constants.MAX_LIMIT), default=10)
# TODO: Remove sort_option 'id' in next major version
annons_search_query.add_argument(constants.SORT, choices=list(fields.sort_options.keys()) + ["id"])
annons_search_query.add_argument(
    constants.STATISTICS,
    action="append",
    choices=[
        taxonomy.OCCUPATION,
        taxonomy.GROUP,
        taxonomy.FIELD,
        taxonomy.COUNTRY,
        taxonomy.MUNICIPALITY,
        taxonomy.REGION,
    ],
)
annons_search_query.add_argument(constants.STAT_LMT, type=inputs.int_range(0, 30), required=False)
annons_search_query.add_argument(constants.LABEL, type=str, action="append", required=False)
