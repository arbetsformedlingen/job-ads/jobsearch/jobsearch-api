import logging

from search.common_search.querybuilder import QueryBuilder, QueryBuilderType
from search.common_search.text_to_concept import TextToConcept
from search.common_search.text_to_label import TextToLabel

log = logging.getLogger(__name__)


# Endpoint functions common between Jobsearch and Historical ads


class QueryBuilderContainer(object):
    """
    Holds a QueryBuilder object for different QueryBuilderType
    Use launch() to create, and get() to get the object
    """

    def __init__(self):
        self.querybuilder = None

    def get(self, qb_type):
        if self.querybuilder is None:
            self.launch()

        if not isinstance(qb_type, QueryBuilderType):
            return None
        self.querybuilder.set_qb_type(qb_type)
        return self.querybuilder

    def launch(self):
        ttc = TextToConcept()
        ttl = TextToLabel()
        self.querybuilder = QueryBuilder(ttc, ttl)
