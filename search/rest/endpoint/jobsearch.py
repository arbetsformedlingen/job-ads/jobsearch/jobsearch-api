import logging
import time

from flask import request
from flask_restx import abort
from flask_restx import Resource

from common import constants
from search.common_search.querybuilder import QueryBuilderType
from search.rest.endpoint.querybuilder_container import QueryBuilderContainer, log
from common.ad_formatter import format_hits_with_only_original_values
from search.rest.model.apis import ns_jobad, open_results, job_ad, typeahead_results
from search.rest.model.queries import annons_complete_query, annons_search_query, load_ad_query
from search.rest.model.swagger import swagger_doc_params, swagger_filter_doc_params
from search.common_search.ad_search import fetch_ad_by_id, search_for_ads
from search.common_search.logo import fetch_ad_logo
from search.common_search.complete import suggest, suggest_extra_word


log = logging.getLogger(__name__)

querybuilder_container = QueryBuilderContainer()


@ns_jobad.route('ad/<id>', endpoint='ad')
class AdById(Resource):
    @ns_jobad.doc(
        description='Load a job ad by ID',
    )
    @ns_jobad.response(404, 'Job ad not found')
    @ns_jobad.expect(load_ad_query)
    @ns_jobad.marshal_with(job_ad)
    def get(self, id, **kwargs):
        result = fetch_ad_by_id(str(id))
        if result.get('removed'):
            abort(404, 'Ad not found')
        else:
            return format_hits_with_only_original_values([result])[0]


@ns_jobad.route('ad/<id>/logo', endpoint='ad_logo')
class AdLogo(Resource):
    @ns_jobad.doc(
        description='Load a logo binary file by ID',
    )
    @ns_jobad.response(404, 'Job ad not found')
    def get(self, id):
        return fetch_ad_logo(str(id))


@ns_jobad.route('search')
class Search(Resource):
    @ns_jobad.doc(
        description='Search using parameters and/or freetext',
        params={**swagger_doc_params, **swagger_filter_doc_params},
    )
    @ns_jobad.expect(annons_search_query)
    @ns_jobad.marshal_with(open_results)
    def get(self, **kwargs):
        start_time = int(time.time() * 1000)
        args = annons_search_query.parse_args()
        return search_endpoint(querybuilder_container.get(QueryBuilderType.JOBSEARCH_SEARCH), args, start_time)



@ns_jobad.route('complete')
class Complete(Resource):
    @ns_jobad.doc(
        description='Typeahead / Suggest next searchword',
        params={
            constants.CONTEXTUAL_TYPEAHEAD: "Set to False to disable contextual typeahead (default: True)",
            **swagger_doc_params
        }
    )
    @ns_jobad.expect(annons_complete_query)
    @ns_jobad.marshal_with(typeahead_results)
    def get(self, **kwargs):
        start_time = int(time.time() * 1000)
        args = annons_complete_query.parse_args()
        limit = args[constants.LIMIT] \
            if args[constants.LIMIT] <= constants.MAX_COMPLETE_LIMIT else constants.MAX_COMPLETE_LIMIT

        word_list, space_last = self.typeahead_query_analyzer(args.get(constants.FREETEXT_QUERY))
        prefix_words = word_list if space_last else word_list[:-1]
        args[constants.TYPEAHEAD_QUERY] = ' '.join(word_list) + (' ' if space_last else '')  # used for aggs queries
        args[constants.FREETEXT_QUERY] = ' '.join(prefix_words)  # used for subset selections (if contextual==True)

        querybuilder_complete = querybuilder_container.get(QueryBuilderType.JOBSEARCH_COMPLETE)
        result = suggest(args, querybuilder_complete)

        # if only one suggestion was given: find extra words
        if len(result.get('aggs')) == 1:
            log.debug(f"Typeahead result with only one suggestion, so adding extra words.")
            extra_words = suggest_extra_word(result.get('aggs')[0]['value'].strip(),
                                             querybuilder_complete)
            result['aggs'] += extra_words

        # TODO: This one can only happen inside above if-clause?
        # if <word> + <space>: make sure <word> is deleted from suggestions
        if len(word_list) == 1 and space_last:
            log.debug("<word><space> query pattern.")
            result['aggs'] = self.find_agg_and_delete(word_list[0], result['aggs'])

        log.debug(f"Typeahead final result: {result['aggs']}")
        log.debug(f"Typeahead query results after: {int(time.time() * 1000 - start_time)} milliseconds.")

        return self.marshal_results(result, limit, start_time)

    @staticmethod
    def typeahead_query_analyzer(original_query):
        # all unnecessary whitespaces are removed
        word_list = original_query.split() if original_query else []
        space_last = (original_query[-1] in ' ') if original_query else False  # TODO: accept also tab last? -> ' \t'
        return word_list, space_last

    @staticmethod
    def marshal_results(esresult, limit, start_time):
        typeahead_result = esresult.get('aggs', [])
        if len(typeahead_result) > limit:
            typeahead_result = typeahead_result[:limit]
        result_time = int(time.time() * 1000 - start_time)
        result = {
            "result_time_in_millis": result_time,
            "time_in_millis": esresult.get('took', 0),
            "typeahead": typeahead_result,
        }
        log.debug(f"Typeahead sending results after: {result_time} milliseconds.")
        return result

    @staticmethod
    def find_agg_and_delete(value, aggs):
        # use to find item from aggs result
        remove_agg = ''
        for agg in aggs:
            if agg['value'] == value:
                remove_agg = agg  # TODO: not executed by tests
                break
        if remove_agg:
            aggs.remove(remove_agg)
            log.debug(f'(find_agg_and_delete) Removed word from aggs: {value}')
        else:
            log.debug(f'(find_agg_and_delete) No removal needed for word: {value}. Aggs untouched.')
        return aggs


def search_endpoint(querybuilder, args, start_time):
    log.debug(f"Query parsed after: {int(time.time() * 1000 - start_time)} milliseconds.")
    result = search_for_ads(args,
                            querybuilder,
                            start_time,
                            request.headers.get('X-Fields'))

    log.debug(f"Query results after: {int(time.time() * 1000 - start_time)} milliseconds.")
    max_score = result.get('max_score', 1.0)
    hits = [dict(hit['_source'],
                 **{'relevance': (hit['_score'] / max_score)
                 if max_score > 0 else 0.0})
            for hit in result.get('hits', [])]
    hits = format_hits_with_only_original_values(hits)
    result['hits'] = hits
    return _format_api_response(result, start_time)


def _format_api_response(esresult, start_time):
    total_results = {'value': esresult.get('total', {}).get('value')}
    result_time = int(time.time() * 1000 - start_time)
    result = {
        "total": total_results,
        "positions": esresult.get('positions', 0),
        "query_time_in_millis": esresult.get('took', 0),
        "result_time_in_millis": result_time,
        "stats": esresult.get('stats', []),
        "freetext_concepts": esresult.get('concepts', {}),
        "hits": esresult.get('hits', {})
    }
    log.debug(f"Sending results after: {result_time} milliseconds.")
    return result
